#include <common/escapes.hpp>

#include <stdexcept>

namespace smile::escapes {
bool isEscaped(char character) {
  for (auto &pair : ESCAPE_TABLE) {
    if (character == pair[1]) {
      return true;
    }
  }

  return false;
}

bool isEscapeFollowing(char character) {
  for (auto &pair : ESCAPE_TABLE) {
    if (character == pair[0]) {
      return true;
    }
  }

  return false;
}

char matchEscapeFollowingFor(char character) {
  for (auto &pair : ESCAPE_TABLE) {
    if (character == pair[1]) {
      return pair[0];
    }
  }

  throw std::invalid_argument("No matching following");
}

char matchEscapedFor(char character) {
  for (auto &pair : ESCAPE_TABLE) {
    if (character == pair[0]) {
      return pair[1];
    }
  }

  throw std::invalid_argument("No matching escape");
}

void escapeString(std::string &string) {
  for (std::string::size_type i = 0; i < string.length(); ++i) {
    if (isEscaped(string[i])) {
      string[i] = matchEscapeFollowingFor(string[i]);
      string.insert(i, 1, '\\');
      ++i;
    }
  }
}

void interpretEscaped(std::string &string) {
  bool escape = false;

  for (std::string::size_type i = 0; i < string.length(); ++i) {
    if (escape) {
      string[i] = matchEscapedFor(string[i]);
      escape = false;
    } else if (string[i] == ESCAPE_SYMBOL) {
      escape = true;
      string.erase(i, 1);
      --i;
    }
  }
}
} // namespace smile::escapes
