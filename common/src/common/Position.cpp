#include <common/Position.hpp>

#include <cassert>

namespace smile {
Position::Position() noexcept
    : m_line(0), m_column(0), m_lineEnd(0), m_columnEnd(0) {}

Position::Position(std::size_t line, std::size_t column, std::size_t length)
    : m_line(line), m_column(column), m_lineEnd(line),
      m_columnEnd(column + length) {
  assert(line != 0 && column != 0 && length != 0);
}

Position::Position(std::size_t line, std::size_t column, std::size_t line_end,
                   std::size_t column_end)
    : m_line(line), m_column(column), m_lineEnd(line_end),
      m_columnEnd(column_end) {
  assert(line != 0 && column != 0 && line <= line_end &&
         (line < line_end || column < column_end));
}

std::size_t Position::line() const noexcept { return m_line; }

std::size_t Position::column() const noexcept { return m_column; }

std::size_t Position::lineEnd() const noexcept { return m_lineEnd; }

std::size_t Position::columnEnd() const noexcept { return m_columnEnd; }

bool Position::isValid() const noexcept { return line() != 0; }

std::size_t Position::length() const noexcept {
  return line() == lineEnd() ? columnEnd() - column() : 0;
}

Position Position::operator-(const Position &other) const {
  return {other.line(), other.column(), line(), column()};
}
} // namespace smile
