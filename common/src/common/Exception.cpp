#include <common/Exception.hpp>

namespace smile {
Exception::Exception(std::string message, Position position)
    : m_message(std::move(message)), m_position(position) {}

const char *Exception::what() const noexcept { return m_message.data(); }

Position Exception::position() const noexcept { return m_position; }
} // namespace smile
