#ifndef SMILECOMPILER_COMMON_INCLUDE_COMMON_EXCEPTION_HPP
#define SMILECOMPILER_COMMON_INCLUDE_COMMON_EXCEPTION_HPP

#include <common/Position.hpp>

#include <exception>
#include <string>

namespace smile {
class Exception : public std::exception {
public:
  Exception(std::string, Position);

  const char *what() const noexcept override;
  Position position() const noexcept;

private:
  const std::string m_message;
  const Position m_position;
};
} // namespace smile

#endif // SMILECOMPILER_COMMON_INCLUDE_COMMON_EXCEPTION_HPP
