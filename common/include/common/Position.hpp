#ifndef SMILECOMPILER_COMMON_INCLUDE_COMMON_POSITION_HPP
#define SMILECOMPILER_COMMON_INCLUDE_COMMON_POSITION_HPP

#include <cstdint>

namespace smile {
class Position {
public:
  Position() noexcept;
  Position(std::size_t, std::size_t, std::size_t);
  Position(std::size_t, std::size_t, std::size_t, std::size_t);

  std::size_t line() const noexcept;
  std::size_t column() const noexcept;
  std::size_t lineEnd() const noexcept;
  std::size_t columnEnd() const noexcept;
  std::size_t length() const noexcept;

  bool isValid() const noexcept;

  Position operator-(const Position &) const;

private:
  std::size_t m_line;
  std::size_t m_column;
  std::size_t m_lineEnd;
  std::size_t m_columnEnd;
};
} // namespace smile

#endif // SMILECOMPILER_COMMON_INCLUDE_COMMON_POSITION_HPP
