#ifndef SMILECOMPILER_COMMON_INCLUDE_COMMON_ESCAPES_HPP
#define SMILECOMPILER_COMMON_INCLUDE_COMMON_ESCAPES_HPP

#include <string>

namespace smile::escapes {
static constexpr const char ESCAPE_SYMBOL = '\\';

static constexpr const char ESCAPE_TABLE[][2]{
    {'a', '\a'},  {'b', '\b'},  {'t', '\t'}, {'n', '\n'},
    {'v', '\v'},  {'f', '\f'},  {'r', '\r'}, {'\"', '\"'},
    {'\'', '\''}, {'\?', '\?'}, {'\\', '\\'}};

bool isEscaped(char);

bool isEscapeFollowing(char);

char matchEscapeFollowingFor(char);

char matchEscapedFor(char);

void escapeString(std::string &);

void interpretEscaped(std::string &string);
} // namespace smile::escapes

#endif // SMILECOMPILER_COMMON_INCLUDE_COMMON_ESCAPES_HPP
