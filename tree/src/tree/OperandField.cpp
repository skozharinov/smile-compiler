#include <tree/OperandField.hpp>

#include <tree/Context.hpp>
#include <tree/operand/Operand.hpp>

namespace smile::tree {
OperandField::OperandField(IdentifierPointer &&name, OperandPointer &&operand,
                           Position position)
    : BaseNode(position), m_name(std::move(name)),
      m_operand(std::move(operand)) {}

const OperandField::IdentifierPointer &OperandField::name() const {
  return m_name;
}

const OperandField::OperandPointer &OperandField::operand() const {
  return m_operand;
}

void OperandField::validate(Context context) const {
  operand()->validate(context);
}
} // namespace smile::tree
