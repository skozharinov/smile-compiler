#include <tree/operand/TupleExpression.hpp>

#include <tree/Context.hpp>
#include <tree/type/TupleType.hpp>

namespace smile::tree {
TupleExpression::TupleExpression(std::list<OperandPointer> &&operands,
                                 Position position)
    : Statement(position), Operand(position), Token(position),
      BaseNode(position), m_operands(std::move(operands)) {}

const std::list<TupleExpression::OperandPointer> &
TupleExpression::operands() const noexcept {
  return m_operands;
}

bool TupleExpression::isConstant(Context context) const noexcept {
  for (auto &operand : operands()) {
    if (!operand->isConstant(context)) {
      return false;
    }
  }

  return true;
}

void TupleExpression::calculateType(Context context) {
  std::list<TypePointer> types;

  for (auto &operand : operands()) {
    operand->calculateType(context);
    types.push_back(operand->type()->clone());
  }

  setType(std::make_unique<TupleType>(std::move(types), true, position()));
}
} // namespace smile::tree
