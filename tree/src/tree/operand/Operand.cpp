#include <tree/operand/Operand.hpp>

#include <tree/Context.hpp>
#include <tree/type/Type.hpp>

namespace smile::tree {
Operand::Operand(Position position)
    : Statement(position), Token(position), BaseNode(position),
      m_calculatedType(nullptr) {}

Operand::~Operand() = default;

void Operand::calculateType(Context) { setType(nullptr); }

const BaseNode::TypePointer &Operand::type() const { return m_calculatedType; }

void Operand::setType(TypePointer &&type) {
  m_calculatedType = std::move(type);
}

void Operand::validate(Context context) const {
  const_cast<Operand *>(this)->calculateType(context);
}
} // namespace smile::tree
