#include <tree/operand/ArrayExpression.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/literal/IntegerLiteral.hpp>
#include <tree/type/ArrayType.hpp>
#include <tree/type/PrimitiveType.hpp>

namespace smile::tree {
ArrayExpression::ArrayExpression(std::list<OperandPointer> &&fields,
                                 Position position)
    : Statement(position), Operand(position), Token(position),
      BaseNode(position), m_operands(std::move(fields)) {}

const std::list<ArrayExpression::OperandPointer> &
ArrayExpression::operands() const noexcept {
  return m_operands;
}

bool ArrayExpression::isConstant(Context context) const noexcept {
  for (auto &operand : operands()) {
    if (!operand->isConstant(context)) {
      return false;
    }
  }

  return true;
}

void ArrayExpression::calculateType(Context context) {
  static TypePointer defaultType = nullptr;
  std::reference_wrapper<const TypePointer> type(defaultType);

  for (auto &operand : operands()) {
    operand->calculateType(context);

    if (type.get() != nullptr && !operand->type()->isSame(type, context)) {
      throw Exception("mismatched types", operand->position());
    }

    if (type.get() == nullptr ||
        dynamic_cast<const PrimitiveType *>(type.get().get())->isLiteral()) {
      type = operand->type();
    }
  }

  setType(std::make_unique<ArrayType>(
      type.get() != nullptr ? type.get()->clone() : nullptr,
      std::make_unique<IntegerLiteral>(operands().size(), position()), true,
      position()));
}
} // namespace smile::tree
