#include <tree/operand/OperationExpression.hpp>

#include <tree/Context.hpp>
#include <tree/literal/Literal.hpp>
#include <tree/operation/Operation.hpp>
#include <tree/token/Token.hpp>
#include <tree/type/Type.hpp>

#include <stack>

namespace smile::tree {
OperationExpression::OperationExpression(std::list<TokenPointer> infix,
                                         Position position)
    : Statement(position), Operand(position), Token(position),
      BaseNode(position), m_infix(std::move(infix)) {}

const std::list<OperationExpression::TokenPointer> &
OperationExpression::infix() const {
  return m_infix;
}

std::list<std::reference_wrapper<const OperationExpression::TokenPointer>>
OperationExpression::toPostfix() const {
  std::stack<std::reference_wrapper<const TokenPointer>> stack;
  std::list<std::reference_wrapper<const TokenPointer>> result;

  for (auto &token : infix()) {
    auto *operand = dynamic_cast<const Operand *>(token.get());
    auto *operation = dynamic_cast<const Operation *>(token.get());

    if (operand != nullptr) {
      result.emplace_back(token);
    } else if (operation != nullptr) {
      if (operation->associativity() == Operation::Associativity::RIGHT) {
        while (!stack.empty() &&
               operation->precedence() <
                   dynamic_cast<const Operation *>(stack.top().get().get())
                       ->precedence()) {
          result.push_back(stack.top());
          stack.pop();
        }

        stack.push(token);
      } else if (operation->associativity() == Operation::Associativity::LEFT) {
        while (!stack.empty() &&
               operation->precedence() <=
                   dynamic_cast<const Operation *>(stack.top().get().get())
                       ->precedence()) {
          result.push_back(stack.top());
          stack.pop();
        }

        stack.push(token);
      }
    }
  }

  while (!stack.empty()) {
    result.push_back(stack.top());
    stack.pop();
  }

  return result;
}

bool OperationExpression::isConstant(Context context) const noexcept {
  for (auto &token : infix()) {
    if (!token->isConstant(context)) {
      return false;
    }
  }

  return true;
}

void OperationExpression::calculateType(Context context) {
  std::stack<TypePointer> types;

  auto postfix = toPostfix();

  for (const TokenPointer &token : postfix) {
    auto *operand = dynamic_cast<Operand *>(token.get());
    auto *operation = dynamic_cast<const Operation *>(token.get());

    if (operand != nullptr) {
      operand->calculateType(context);
      types.push(operand->type()->clone());
    } else if (operation != nullptr) {
      switch (operation->arity()) {
      case Operation::Arity::SINGLE: {
        auto argument = std::move(types.top());
        types.pop();
        types.push(operation->type(argument, context));
        break;
      }

      case Operation::Arity::DOUBLE: {
        auto argumentRhs = std::move(types.top());
        types.pop();
        auto argumentLhs = std::move(types.top());
        types.pop();
        types.push(operation->type(argumentLhs, argumentRhs, context));
        break;
      }
      }
    }
  }

  setType(std::move(types.top()));
}
} // namespace smile::tree
