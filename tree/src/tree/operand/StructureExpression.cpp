#include <tree/operand/StructureExpression.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/OperandField.hpp>
#include <tree/TypeField.hpp>
#include <tree/identifier/Identifier.hpp>
#include <tree/identifier/TypeIdentifier.hpp>
#include <tree/node/Structure.hpp>

namespace smile::tree {
StructureExpression::StructureExpression(
    TypeIdentifierPointer &&name, std::list<OperandFieldPointer> &&fields,
    Position position)
    : Statement(position), Operand(position), Token(position),
      BaseNode(position), m_name(std::move(name)), m_fields(std::move(fields)) {
}

const StructureExpression::TypeIdentifierPointer &
StructureExpression::name() const noexcept {
  return m_name;
}

const std::list<StructureExpression::OperandFieldPointer> &
StructureExpression::fields() const noexcept {
  return m_fields;
}

bool StructureExpression::isConstant(Context context) const noexcept {
  for (auto &field : fields()) {
    if (!field->operand()->isConstant(context)) {
      return false;
    }
  }

  return true;
}

void StructureExpression::calculateType(Context context) {
  if (!context.hasStructure(*name())) {
    throw Exception("structure not found", position());
  }

  const auto &structure = context.structure(*name()).get();

  if (structure.fields().size() != fields().size()) {
    throw Exception("some fields are missing", position());
  }

  for (auto &field : fields()) {
    field->validate(context);
    field->operand()->calculateType(context);

    auto &structureField = structure.field(field->name());

    if (structureField == nullptr) {
      throw Exception("field does not exist", field->position());
    }

    if (!structureField->type()->isSame(field->operand()->type(), context)) {
      throw Exception("mismatched types", field->position());
    }
  }

  setType(name()->clone());
}
} // namespace smile::tree
