#include <tree/token/Token.hpp>

#include <tree/Context.hpp>

namespace smile::tree {
Token::Token(Position position) : BaseNode(position) {}

bool Token::isConstant(Context) const noexcept { return false; }
} // namespace smile::tree
