#include <tree/node/Function.hpp>

#include <tree/Context.hpp>
#include <tree/TypeField.hpp>
#include <tree/identifier/Identifier.hpp>
#include <tree/statement/Compound.hpp>
#include <tree/type/FunctionType.hpp>

namespace smile::tree {
Function::Function(IdentifierPointer &&name,
                   std::list<TypeFieldPointer> &&arguments,
                   TypePointer &&returnType, CompoundPointer &&body,
                   Position position)
    : Node(position), BaseNode(position), m_name(std::move(name)),
      m_arguments(std::move(arguments)), m_returnType(std::move(returnType)),
      m_body(std::move(body)) {}

Function::~Function() = default;

const Function::IdentifierPointer &Function::name() const noexcept {
  return m_name;
}

const std::list<Function::TypeFieldPointer> &Function::arguments() const
    noexcept {
  return m_arguments;
}

const Function::TypePointer &Function::returnType() const noexcept {
  return m_returnType;
}

const Function::CompoundPointer &Function::body() const noexcept {
  return m_body;
}

Function::TypePointer Function::type() const noexcept {
  std::list<TypePointer> newArguments;

  for (auto &field : arguments()) {
    newArguments.push_back(field->type()->clone());
  }

  return std::make_unique<FunctionType>(
      returnType()->clone(), std::move(newArguments), false, position());
}

void Function::validate(Context context) const {
  for (auto &field : arguments()) {
    field->validate(context);
  }

  for (auto &field : arguments()) {
    context += *field;
  }

  if (returnType() != nullptr) {
    returnType()->validate(context);
  }

  context.setExpected(returnType());

  body()->validate(context);
}
} // namespace smile::tree
