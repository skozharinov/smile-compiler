#include <tree/node/Structure.hpp>

#include <tree/Context.hpp>
#include <tree/TypeField.hpp>
#include <tree/identifier/TypeIdentifier.hpp>
#include <tree/node/Function.hpp>
#include <tree/type/ReferenceType.hpp>

namespace smile::tree {
Structure::Structure(TypeIdentifierPointer &&name,
                     std::list<TypeFieldPointer> &&fields, Position position)
    : Node(position), BaseNode(position), m_name(std::move(name)),
      m_fields(std::move(fields)) {}

Structure::~Structure() = default;

const Structure::TypeIdentifierPointer &Structure::name() const noexcept {
  return m_name;
}

const std::list<Structure::TypeFieldPointer> &Structure::fields() const
    noexcept {
  return m_fields;
}

const std::list<Structure::FunctionPointer> &Structure::methods() const
    noexcept {
  return m_methods;
}

static const Structure::TypeFieldPointer DEFAULT_FIELD = nullptr;

const Structure::TypeFieldPointer &
Structure::field(const IdentifierPointer &name) const noexcept {
  for (auto &field : fields()) {
    if (*field->name() == *name) {
      return field;
    }
  }

  return DEFAULT_FIELD;
}

static const Structure::FunctionPointer DEFAULT_METHOD = nullptr;

const Structure::FunctionPointer &
Structure::method(const IdentifierPointer &name) const noexcept {
  for (auto &method : methods()) {
    if (*method->name() == *name) {
      return method;
    }
  }

  return DEFAULT_METHOD;
}

Structure::MethodKind Structure::methodKind(const IdentifierPointer &methodName,
                                            Context context) const noexcept {
  auto &checkedMethod = method(methodName);

  if (checkedMethod->arguments().empty()) {
    return MethodKind::STATIC;
  }

  auto &firstArgument = checkedMethod->arguments().front();

  if (name()->isSame(firstArgument->type(), context)) {
    return MethodKind::VALUE;
  }

  auto *referenceType =
      dynamic_cast<const ReferenceType *>(firstArgument->type().get());

  if (referenceType != nullptr &&
      name()->isSame(referenceType->referenced(), context)) {
    if (referenceType->isMutable()) {
      return MethodKind::MUTABLE_REFERENCE;
    } else {
      return MethodKind::IMMUTABLE_REFERENCE;
    }
  } else {
    return MethodKind::STATIC;
  }
}

void Structure::validate(Context context) const {
  for (auto &field : fields()) {
    field->validate(context);
  }

  for (auto &method : methods()) {
    method->validate(context);
  }
}

Structure &Structure::operator+=(FunctionPointer &&method) noexcept {
  methods().push_back(std::move(method));
  return *this;
}

std::list<Structure::TypeFieldPointer> &Structure::fields() noexcept {
  return m_fields;
}

std::list<Structure::FunctionPointer> &Structure::methods() noexcept {
  return m_methods;
}
} // namespace smile::tree
