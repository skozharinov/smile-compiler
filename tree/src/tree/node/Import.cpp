#include <tree/node/Import.hpp>

#include <tree/Context.hpp>
#include <tree/identifier/Identifier.hpp>

namespace smile::tree {
Import::Import(IdentifierPointer &&path, Position position)
    : Node(position), BaseNode(position), m_path(std::move(path)) {}

const Import::IdentifierPointer &Import::path() const noexcept {
  return m_path;
}
} // namespace smile::tree
