#include <tree/node/Implementation.hpp>

#include <tree/Context.hpp>
#include <tree/identifier/TypeIdentifier.hpp>
#include <tree/node/Function.hpp>

namespace smile::tree {
Implementation::Implementation(TypeIdentifierPointer &&name,
                               std::list<FunctionPointer> &&methods,
                               Position position)
    : Node(position), BaseNode(position), m_name(std::move(name)),
      m_methods(std::move(methods)) {}

Implementation::~Implementation() = default;

const Implementation::TypeIdentifierPointer &Implementation::name() const
    noexcept {
  return m_name;
}

const std::list<Implementation::FunctionPointer> &
Implementation::methods() const noexcept {
  return m_methods;
}

std::list<Implementation::FunctionPointer> &Implementation::methods() noexcept {
  return m_methods;
}

void Implementation::validate(Context context) const {
  for (auto &method : methods()) {
    method->validate(context);
  }
}
} // namespace smile::tree
