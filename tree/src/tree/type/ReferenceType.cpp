#include <tree/type/ReferenceType.hpp>

#include <tree/Context.hpp>
#include <tree/type/PrimitiveType.hpp>

namespace smile::tree {
ReferenceType::ReferenceType(Kind kind, TypePointer &&referenced,
                             bool isMutableReference, bool isMutable,
                             Position position)
    : Type(isMutable, position), BaseNode(position), m_kind(kind),
      m_referenced(std::move(referenced)),
      m_isMutableReference(isMutableReference) {}

ReferenceType::Kind ReferenceType::kind() const noexcept { return m_kind; }

const ReferenceType::TypePointer &ReferenceType::referenced() const noexcept {
  return m_referenced;
}

bool ReferenceType::isMutableReference() const noexcept {
  return m_isMutableReference;
}

bool ReferenceType::isSame(const TypePointer &type, Context context) const
    noexcept {
  auto *referenceType = dynamic_cast<const ReferenceType *>(type.get());
  auto *primitiveType = dynamic_cast<const PrimitiveType *>(type.get());

  if (referenceType != nullptr) {
    return kind() == referenceType->kind() &&
           referenced()->isSame(referenceType->referenced(), context) &&
           (isMutableReference() || !referenceType->isMutableReference());
  } else if (primitiveType != nullptr) {
    return kind() == Kind::POINTER &&
           primitiveType->group() == PrimitiveType::Group::POINTER;
  } else {
    return false;
  }
}

bool ReferenceType::isConvertible(const TypePointer &type,
                                  Context context) const noexcept {
  auto *referenceType = dynamic_cast<const ReferenceType *>(type.get());
  return referenceType != nullptr &&
         referenced()->isSame(referenceType->referenced(), context) &&
         (isMutableReference() || !referenceType->isMutableReference());
}

ReferenceType::TypePointer ReferenceType::clone() const noexcept {
  return std::make_unique<ReferenceType>(kind(), referenced()->clone(),
                                         isMutableReference(), isMutable(),
                                         position());
}

void ReferenceType::validate(Context context) const {
  referenced()->validate(context);
}

int ReferenceType::compareWith(const TypePointer &type, Context context) const
    noexcept {
  auto *referenceType = dynamic_cast<const ReferenceType *>(type.get());

  if (!isSame(type, context)) {
    return 0;
  } else {
    return referenced()->compareWith(referenceType->referenced(), context);
  }
}
} // namespace smile::tree
