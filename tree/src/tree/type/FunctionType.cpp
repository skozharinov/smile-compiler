#include <tree/type/FunctionType.hpp>

#include <tree/Context.hpp>

namespace smile::tree {
FunctionType::FunctionType(TypePointer &&returnType,
                           std::list<TypePointer> &&arguments, bool isMutable,
                           Position position)
    : Type(isMutable, position), BaseNode(position),
      m_returnType(std::move(returnType)), m_arguments(std::move(arguments)) {}

const FunctionType::TypePointer &FunctionType::returnType() const noexcept {
  return m_returnType;
}

const std::list<FunctionType::TypePointer> &FunctionType::arguments() const
    noexcept {
  return m_arguments;
}

bool FunctionType::isSame(const TypePointer &type, Context context) const
    noexcept {
  auto *functionType = dynamic_cast<const FunctionType *>(type.get());

  if (functionType == nullptr ||
      !returnType()->isSame(functionType->returnType(), context) ||
      arguments().size() != functionType->arguments().size()) {
    return false;
  }

  for (auto it1 = arguments().begin(), it2 = functionType->arguments().begin();
       it1 != arguments().end(); ++it1, ++it2) {
    if (!(*it1)->isSame(*it2, context)) {
      return false;
    }
  }

  return true;
}

FunctionType::TypePointer FunctionType::clone() const noexcept {
  auto newReturnType = returnType()->clone();
  std::list<TypePointer> newArguments;

  for (auto &type : arguments()) {
    newArguments.push_back(type->clone());
  }

  return std::make_unique<FunctionType>(std::move(newReturnType),
                                        std::move(newArguments), isMutable(),
                                        position());
}

void FunctionType::validate(Context context) const {
  returnType()->validate(context);

  for (auto &type : arguments()) {
    type->validate(context);
  }
}
} // namespace smile::tree
