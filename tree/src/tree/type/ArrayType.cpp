#include <tree/type/ArrayType.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/operand/Operand.hpp>
#include <tree/type/PrimitiveType.hpp>

namespace smile::tree {
ArrayType::ArrayType(TypePointer &&type, OperandPointer &&size, bool isMutable,
                     Position position)
    : Type(isMutable, position), BaseNode(position), m_type(std::move(type)),
      m_size(std::move(size)) {}

const ArrayType::TypePointer &ArrayType::type() const noexcept {
  return m_type;
}

bool ArrayType::isSame(const TypePointer &, Context) const noexcept {
  // TODO: implement ArrayType comparison
  return false;
}

const ArrayType::OperandPointer &ArrayType::size() const noexcept {
  return m_size;
}

ArrayType::TypePointer ArrayType::iteratingType() const noexcept {
  return type()->clone();
}

ArrayType::TypePointer ArrayType::clone() const noexcept {
  // TODO: implement ArrayType cloning
  return nullptr;
}

void ArrayType::validate(Context context) const {
  type()->validate(context);

  if (!size()->isConstant(context)) {
    throw Exception("array size is not a constant", size()->position());
  }

  size()->calculateType(context);

  if (!size()->type()->isSame(
          std::make_unique<PrimitiveType>(
              PrimitiveType::Kind::UNSIGNED_INT_SIZE, true, size()->position()),
          context)) {
    throw Exception("array size is not of unsigned integer size type",
                    size()->position());
  }
}

int ArrayType::compareWith(const TypePointer &type, Context context) const
    noexcept {
  auto *arrayType = dynamic_cast<const ArrayType *>(type.get());

  if (!isSame(type, context)) {
    return 0;
  } else {
    return this->type()->compareWith(arrayType->type(), context);
  }
}
} // namespace smile::tree
