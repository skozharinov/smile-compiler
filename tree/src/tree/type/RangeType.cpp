#include <tree/type/RangeType.hpp>

#include <tree/Context.hpp>

namespace smile::tree {
RangeType::RangeType(TypePointer &&type, bool isMutable, Position position)
    : Type(isMutable, position), BaseNode(position), m_type(std::move(type)) {}

const RangeType::TypePointer &RangeType::type() const noexcept {
  return m_type;
}

bool RangeType::isSame(const TypePointer &otherType, Context context) const
    noexcept {
  auto *rangeType = dynamic_cast<const RangeType *>(otherType.get());
  return rangeType != nullptr && rangeType->type()->isSame(type(), context);
}

RangeType::TypePointer RangeType::iteratingType() const noexcept {
  return type()->clone();
}

RangeType::TypePointer RangeType::clone() const noexcept {
  return std::make_unique<RangeType>(type()->clone(), isMutable(), position());
}

void RangeType::validate(Context context) const { type()->validate(context); }
} // namespace smile::tree
