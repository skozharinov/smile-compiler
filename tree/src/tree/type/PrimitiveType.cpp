#include <tree/type/PrimitiveType.hpp>

#include <tree/Context.hpp>
#include <tree/type/ReferenceType.hpp>

namespace smile::tree {
PrimitiveType::Kind PrimitiveType::makeSigned(Kind kind) noexcept {
  switch (kind) {
  case Kind::UNSIGNED_INT_8:
    return Kind::SIGNED_INT_8;
  case Kind::UNSIGNED_INT_16:
    return Kind::SIGNED_INT_16;
  case Kind::UNSIGNED_INT_32:
    return Kind::SIGNED_INT_32;
  case Kind::UNSIGNED_INT_64:
    return Kind::SIGNED_INT_64;
  case Kind::UNSIGNED_INT_SIZE:
    return Kind::SIGNED_INT_SIZE;
  case Kind::UNSIGNED_INT_LITERAL:
    return Kind::SIGNED_INT_LITERAL;
  default:
    return kind;
  }
}

PrimitiveType::Kind PrimitiveType::makeUnsigned(Kind kind) noexcept {
  switch (kind) {
  case Kind::SIGNED_INT_8:
    return Kind::UNSIGNED_INT_8;
  case Kind::SIGNED_INT_16:
    return Kind::UNSIGNED_INT_16;
  case Kind::SIGNED_INT_32:
    return Kind::UNSIGNED_INT_32;
  case Kind::SIGNED_INT_64:
    return Kind::UNSIGNED_INT_64;
  case Kind::SIGNED_INT_SIZE:
    return Kind::UNSIGNED_INT_SIZE;
  case Kind::SIGNED_INT_LITERAL:
    return Kind::UNSIGNED_INT_LITERAL;
  default:
    return kind;
  }
}

PrimitiveType::PrimitiveType(Kind kind, bool isMutable, Position position)
    : Type(isMutable, position), BaseNode(position), m_kind(kind) {}

PrimitiveType::Kind PrimitiveType::kind() const noexcept { return m_kind; }

PrimitiveType::Group PrimitiveType::group() const noexcept {
  switch (kind()) {
  case Kind::UNSIGNED_INT_8:
  case Kind::UNSIGNED_INT_16:
  case Kind::UNSIGNED_INT_32:
  case Kind::UNSIGNED_INT_64:
  case Kind::UNSIGNED_INT_SIZE:
  case Kind::UNSIGNED_INT_LITERAL:
    return Group::UNSIGNED_INT;
  case Kind::SIGNED_INT_8:
  case Kind::SIGNED_INT_16:
  case Kind::SIGNED_INT_32:
  case Kind::SIGNED_INT_64:
  case Kind::SIGNED_INT_SIZE:
  case Kind::SIGNED_INT_LITERAL:
    return Group::SIGNED_INT;
  case Kind::FLOAT_32:
  case Kind::FLOAT_64:
  case Kind::FLOAT_LITERAL:
    return Group::FLOAT;
  case Kind::CHARACTER:
    return Group::CHARACTER;
  case Kind::STRING:
    return Group::STRING;
  case Kind::BOOLEAN:
    return Group::BOOLEAN;
  case Kind::NULL_POINTER:
    return Group::POINTER;
  default:
    return static_cast<Group>(-1);
  }
}

bool PrimitiveType::isSame(const TypePointer &type, Context) const noexcept {
  auto *primitiveType = dynamic_cast<const PrimitiveType *>(type.get());
  auto *referenceType = dynamic_cast<const ReferenceType *>(type.get());

  if (referenceType != nullptr) {
    return referenceType->kind() == ReferenceType::Kind::POINTER &&
           group() == Group::POINTER;
  } else if (primitiveType != nullptr) {
    return (kind() == primitiveType->kind()) ||
           ((isLiteral() || primitiveType->isLiteral()) &&
            group() == primitiveType->group()) ||
           (kind() == Kind::UNSIGNED_INT_LITERAL &&
            primitiveType->group() == Group::SIGNED_INT) ||
           (primitiveType->kind() == Kind::UNSIGNED_INT_LITERAL &&
            group() == Group::SIGNED_INT);
  } else {
    return false;
  }
}

bool PrimitiveType::isConvertible(const TypePointer &type, Context) const
    noexcept {
  auto *primitiveType = dynamic_cast<const PrimitiveType *>(type.get());
  auto *referenceType = dynamic_cast<const ReferenceType *>(type.get());

  if (referenceType != nullptr) {
    return referenceType->kind() == ReferenceType::Kind::POINTER &&
           group() == Group::POINTER;
  } else if (primitiveType != nullptr) {
    switch (group()) {
    case Group::SIGNED_INT:
    case Group::UNSIGNED_INT:
    case Group::FLOAT:
      switch (primitiveType->group()) {
      case Group::SIGNED_INT:
      case Group::UNSIGNED_INT:
      case Group::FLOAT:
        return true;
      default:
        return false;
      }
    case Group::STRING:
      return primitiveType->group() == Group::STRING;
    case Group::CHARACTER:
      return primitiveType->group() == Group::CHARACTER;
    case Group::BOOLEAN:
      switch (primitiveType->group()) {
      case Group::SIGNED_INT:
      case Group::UNSIGNED_INT:
      case Group::BOOLEAN:
        return true;
      default:
        return false;
      }
    default:
      return false;
    }
  } else {
    return false;
  }
}

bool PrimitiveType::isLiteral() const noexcept {
  switch (kind()) {
  case Kind::SIGNED_INT_LITERAL:
  case Kind::UNSIGNED_INT_LITERAL:
  case Kind::FLOAT_LITERAL:
  case Kind::NULL_POINTER:
    return true;
  default:
    return false;
  }
}

Type::TypePointer PrimitiveType::clone() const noexcept {
  return std::make_unique<PrimitiveType>(kind(), isMutable(), position());
}

int PrimitiveType::compareWith(const TypePointer &type, Context context) const
    noexcept {
  auto *primitiveType = dynamic_cast<const PrimitiveType *>(type.get());

  if (!isSame(type, context) || (isLiteral() && primitiveType->isLiteral())) {
    return 0;
  } else if (isLiteral()) {
    return 1;
  } else {
    return -1;
  }
}
} // namespace smile::tree
