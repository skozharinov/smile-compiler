#include <tree/type/Type.hpp>

#include <tree/Context.hpp>

namespace smile::tree {
Type::Type(bool isMutable, Position position)
    : BaseNode(position), m_isMutable(isMutable) {}

bool Type::isMutable() const noexcept { return m_isMutable; }

void Type::setMutable(bool isMutable) noexcept { m_isMutable = isMutable; }

bool Type::isSame(const TypePointer &, Context) const noexcept { return false; }

bool Type::isConvertible(const TypePointer &type, Context context) const
    noexcept {
  return isSame(type, context);
}

int Type::compareWith(const TypePointer &, Context) const noexcept { return 0; }

Type::TypePointer Type::iteratingType() const noexcept { return nullptr; }

Type::TypePointer Type::clone() const noexcept { return nullptr; }
} // namespace smile::tree
