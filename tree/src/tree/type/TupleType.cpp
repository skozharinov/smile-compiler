#include <tree/type/TupleType.hpp>

#include <tree/Context.hpp>

#include <utility>

namespace smile::tree {
TupleType::TupleType(std::list<TypePointer> &&types, bool isMutable,
                     Position position)
    : Type(isMutable, position), BaseNode(position), m_types(std::move(types)) {
}

const std::list<TupleType::TypePointer> &TupleType::types() const noexcept {
  return m_types;
}

bool TupleType::isSame(const TypePointer &type, Context context) const
    noexcept {
  auto *tupleType = dynamic_cast<const TupleType *>(type.get());

  if (tupleType != nullptr && types().size() == tupleType->types().size()) {
    for (auto it1 = types().begin(), it2 = tupleType->types().begin();
         it1 != types().end(); ++it1, ++it2) {
      if (!(*it1)->isSame(*it2, context)) {
        return false;
      }
    }

    return true;
  }

  return false;
}

void TupleType::validate(Context context) const {
  for (auto &type : types()) {
    type->validate(context);
  }
}

TupleType::TypePointer TupleType::clone() const noexcept {
  std::list<TypePointer> newTypes;

  for (auto &type : types()) {
    newTypes.push_back(type->clone());
  }

  return std::make_unique<TupleType>(std::move(newTypes), isMutable(),
                                     position());
}
} // namespace smile::tree
