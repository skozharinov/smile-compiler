#include <tree/identifier/Identifier.hpp>

namespace smile::tree {
Identifier::Identifier(std::string &&contents, Position position)
    : BaseNode(position), m_contents(std::move(contents)) {}

std::string Identifier::contents() const noexcept { return m_contents; }

bool Identifier::operator==(const Identifier &other) const {
  return m_contents == other.m_contents;
}

std::size_t IdentifierHash::operator()(const Identifier &identifier) const {
  return m_hashFunctor(identifier.contents());
}
} // namespace smile::tree
