#include <tree/identifier/TypeIdentifier.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>

namespace smile::tree {
TypeIdentifier::TypeIdentifier(std::string &&contents, bool isMutable,
                               Position position)
    : Identifier(std::move(contents), position), Type(isMutable, position),
      BaseNode(position) {}

bool TypeIdentifier::isSame(const TypePointer &type, Context) const noexcept {
  auto *identifier = dynamic_cast<const Identifier *>(type.get());
  return identifier != nullptr && *identifier == *this;
}

TypeIdentifier::TypePointer TypeIdentifier::clone() const noexcept {
  return std::make_unique<TypeIdentifier>(std::string(contents()), isMutable(),
                                          position());
}

void TypeIdentifier::validate(Context context) const {
  if (!context.hasStructure(*this)) {
    throw Exception("structure does not exist", position());
  }
}
} // namespace smile::tree
