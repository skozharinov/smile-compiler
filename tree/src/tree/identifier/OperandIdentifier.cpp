#include <tree/identifier/OperandIdentifier.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/TypeField.hpp>
#include <tree/node/Function.hpp>
#include <tree/statement/Variable.hpp>
#include <tree/type/Type.hpp>

namespace smile::tree {
OperandIdentifier::OperandIdentifier(std::string &&contents, Position position)
    : Identifier(std::move(contents), position), Operand(position),
      Statement(position), Token(position), BaseNode(position) {}

bool OperandIdentifier::isConstant(Context context) const noexcept {
  return context.hasVariable(*this) &&
         context.variable(*this).get().isConstant();
}

void OperandIdentifier::calculateType(Context context) {
  if (context.hasVariable(*this)) {
    setType(context.variable(*this).get().field()->type()->clone());
  } else if (context.hasField(*this)) {
    setType(context.field(*this).get().type()->clone());
  } else if (context.hasFunction(*this)) {
    setType(context.function(*this).get().type()->clone());
  } else {
    throw Exception("undeclared variable", position());
  }
}
} // namespace smile::tree
