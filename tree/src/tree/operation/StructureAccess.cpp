#include <tree/operation/StructureAccess.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/TypeField.hpp>
#include <tree/identifier/TypeIdentifier.hpp>
#include <tree/node/Function.hpp>
#include <tree/node/Structure.hpp>
#include <tree/type/FunctionType.hpp>

namespace smile::tree {
StructureAccess::StructureAccess(IdentifierPointer &&field, Position position)
    : Operation(Arity::SINGLE, Associativity::LEFT, 140, position),
      Token(position), BaseNode(position), m_field(std::move(field)) {}

const StructureAccess::IdentifierPointer &StructureAccess::field() const
    noexcept {
  return m_field;
}

bool StructureAccess::isConstant(Context) const noexcept { return true; }

StructureAccess::TypePointer StructureAccess::type(const TypePointer &type,
                                                   Context context) const {
  auto *typeIdentifier = dynamic_cast<const TypeIdentifier *>(type.get());

  if (typeIdentifier == nullptr) {
    throw Exception("mismatched types", position());
  }

  if (!context.hasStructure(*typeIdentifier)) {
    throw Exception("no such structure", position());
  }

  auto &structure = context.structure(*typeIdentifier).get();

  auto &structureField = structure.field(field());
  auto &method = structure.method(field());

  if (structureField != nullptr) {
    auto result = structureField->type()->clone();
    result->setMutable(type->isMutable());
    return result;
  } else if (method != nullptr) {
    switch (structure.methodKind(field(), context)) {
    case Structure::MethodKind::MUTABLE_REFERENCE:
      if (!type->isMutable()) {
        throw Exception("mutable reference method call on immutable type",
                        position());
      }

    case Structure::MethodKind::IMMUTABLE_REFERENCE:
    case Structure::MethodKind::VALUE: {
      std::list<TypePointer> types;

      for (auto &argumentType : method->arguments()) {
        types.push_back(argumentType->type()->clone());
      }

      types.pop_front();
      return std::make_unique<FunctionType>(
          method->returnType()->clone(), std::move(types), false, position());
    }

    case Structure::MethodKind::STATIC:
      return method->type()->clone();
    }
  } else {
    throw Exception("no such field", position());
  }
}
} // namespace smile::tree
