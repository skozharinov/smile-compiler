#include <tree/operation/TupleAccess.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/literal/IntegerLiteral.hpp>
#include <tree/type/TupleType.hpp>

namespace smile::tree {
TupleAccess::TupleAccess(IntegerLiteralPointer &&field, Position position)
    : Operation(Arity::SINGLE, Associativity::LEFT, 140, position),
      Token(position), BaseNode(position), m_field(std::move(field)) {}

const TupleAccess::IntegerLiteralPointer &TupleAccess::field() const noexcept {
  return m_field;
}

bool TupleAccess::isConstant(Context context) const noexcept { return true; }

TupleAccess::TypePointer TupleAccess::type(const TypePointer &type,
                                           Context context) const {
  auto *tupleType = dynamic_cast<const TupleType *>(type.get());

  if (tupleType == nullptr) {
    throw Exception("mismatched types", position());
  } else if (field()->value() >= tupleType->types().size()) {
    throw Exception("tuple index error", position());
  }

  auto result =
      (*std::next(tupleType->types().begin(), field()->value()))->clone();
  result->setMutable(type->isMutable());
  return std::move(result);
}
} // namespace smile::tree
