#include <tree/operation/UnaryOperation.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/type/PrimitiveType.hpp>
#include <tree/type/ReferenceType.hpp>

namespace smile::tree {
UnaryOperation::UnaryOperation(Kind kind, Position position)
    : Operation(Arity::SINGLE, Associativity::RIGHT, 130, position),
      Token(position), BaseNode(position), m_kind(kind) {}

UnaryOperation::Kind UnaryOperation::kind() const noexcept { return m_kind; }

bool UnaryOperation::isConstant(Context) const noexcept {
  return kind() != Kind::MUTABLE_REFERENCE;
}

UnaryOperation::TypePointer UnaryOperation::type(const TypePointer &type,
                                                 Context) const {
  switch (kind()) {
  case Kind::REFERENCE:
    return std::make_unique<ReferenceType>(
        ReferenceType::Kind::REFERENCE, type->clone(), false, true, position());
  case Kind::MUTABLE_REFERENCE:
    if (!type->isMutable()) {
      throw Exception("mutable reference to an immutable type", position());
    }

    return std::make_unique<ReferenceType>(
        ReferenceType::Kind::REFERENCE, type->clone(), true, true, position());
  case Kind::DEREFERENCE: {
    auto *referenceType = dynamic_cast<const ReferenceType *>(type.get());

    if (referenceType == nullptr) {
      throw Exception("mismatched types", position());
    }

    auto result = referenceType->referenced()->clone();
    result->setMutable(referenceType->isMutableReference());
    return std::move(result);
  }

  case Kind::NEGATIVE: {
    auto *primitiveType = dynamic_cast<const PrimitiveType *>(type.get());

    if (primitiveType == nullptr) {
      throw Exception("mismatched types", position());
    }

    switch (primitiveType->group()) {
    case PrimitiveType::Group::UNSIGNED_INT:
    case PrimitiveType::Group::SIGNED_INT:
    case PrimitiveType::Group::FLOAT:
      return std::make_unique<PrimitiveType>(
          PrimitiveType::makeSigned(primitiveType->kind()), true, position());
    default:
      throw Exception("mismatched types", position());
    }
  }
  case Kind::POSITIVE: {
    auto *primitiveType = dynamic_cast<const PrimitiveType *>(type.get());

    if (primitiveType == nullptr) {
      throw Exception("mismatched types", position());
    }

    switch (primitiveType->group()) {
    case PrimitiveType::Group::UNSIGNED_INT:
    case PrimitiveType::Group::SIGNED_INT:
    case PrimitiveType::Group::FLOAT:
      return std::make_unique<PrimitiveType>(primitiveType->kind(), true,
                                             position());
    default:
      throw Exception("mismatched types", position());
    }
  }
  case Kind::NEGATION: {
    auto *primitiveType = dynamic_cast<const PrimitiveType *>(type.get());

    if (primitiveType == nullptr) {
      throw Exception("mismatched types", position());
    }

    switch (primitiveType->group()) {
    case PrimitiveType::Group::UNSIGNED_INT:
    case PrimitiveType::Group::SIGNED_INT:
    case PrimitiveType::Group::BOOLEAN:
      return std::make_unique<PrimitiveType>(primitiveType->kind(), true,
                                             position());
    default:
      throw Exception("mismatched types", position());
    }
  }
  default:
    return nullptr;
  }
}
} // namespace smile::tree
