#include <tree/operation/Operation.hpp>

#include <tree/Context.hpp>

namespace smile::tree {
Operation::Operation(Arity arity, Associativity associativity, int precedence,
                     Position position)
    : Token(position), BaseNode(position), m_arity(arity),
      m_associativity(associativity), m_precedence(precedence) {}

Operation::Arity Operation::arity() const noexcept { return m_arity; }

Operation::Associativity Operation::associativity() const noexcept {
  return m_associativity;
}

int Operation::precedence() const noexcept { return m_precedence; }

Operation::TypePointer Operation::type(const TypePointer &, Context) const {
  return nullptr;
}

Operation::TypePointer Operation::type(const TypePointer &, const TypePointer &,
                                       Context) const {
  return nullptr;
}
} // namespace smile::tree
