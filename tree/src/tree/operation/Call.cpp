#include <tree/operation/Call.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/operand/Operand.hpp>
#include <tree/type/FunctionType.hpp>
#include <utility>

namespace smile::tree {
Call::Call(std::list<OperandPointer> &&arguments, Position position)
    : Operation(Arity::SINGLE, Associativity::LEFT, 140, position),
      Token(position), BaseNode(position), m_arguments(std::move(arguments)) {}

const std::list<Call::OperandPointer> &Call::arguments() const noexcept {
  return m_arguments;
}

void Call::validate(Context context) const {
  for (auto &argument : arguments()) {
    argument->validate(context);
  }
}

Call::TypePointer Call::type(const TypePointer &type, Context context) const {
  auto *functionType = dynamic_cast<const FunctionType *>(type.get());

  if (functionType == nullptr) {
    throw Exception("call on non-function type", position());
  }

  if (m_arguments.size() != functionType->arguments().size()) {
    throw Exception("arguments number mismatch", position());
  }

  auto it1 = arguments().begin();
  auto it2 = functionType->arguments().begin();

  for (; it1 != m_arguments.end(); ++it1, ++it2) {
    auto &callArgument = *it1;
    auto &functionArgument = *it2;

    callArgument->calculateType(context);

    if (!callArgument->type()->isSame(functionArgument, context)) {
      throw Exception("arguments type mismatch", callArgument->position());
    }
  }

  return functionType->returnType()->clone();
}
} // namespace smile::tree
