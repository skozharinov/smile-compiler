#include <tree/operation/Cast.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/type/Type.hpp>

namespace smile::tree {
Cast::Cast(TypePointer &&newType, Position position)
    : Operation(Arity::SINGLE, Associativity::LEFT, 120, position),
      Token(position), BaseNode(position), m_newType(std::move(newType)) {}

const Cast::TypePointer &Cast::newType() const noexcept { return m_newType; }

bool Cast::isConstant(Context) const noexcept { return true; }

void Cast::validate(Context context) const { newType()->validate(context); }

Cast::TypePointer Cast::type(const TypePointer &type, Context context) const {
  if (!type->isConvertible(newType(), context)) {
    throw Exception("mismatched types", position());
  }

  auto result = newType()->clone();
  result->setMutable(type->isMutable());
  return result;
}
} // namespace smile::tree
