#include <tree/operation/BinaryOperation.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/type/ArrayType.hpp>
#include <tree/type/PrimitiveType.hpp>
#include <tree/type/RangeType.hpp>
#include <tree/type/ReferenceType.hpp>

namespace smile::tree {
BinaryOperation::BinaryOperation(Kind kind, bool isAssignment,
                                 Position position)
    : Operation(Arity::DOUBLE, Associativity::LEFT, 0, position),
      Token(position), BaseNode(position), m_kind(kind),
      m_isAssignment(isAssignment) {
  if (isAssignment) {
    switch (kind) {
    case Kind::NOOP:
    case Kind::ADDITION:
    case Kind::SUBTRACTION:
    case Kind::MULTIPLICATION:
    case Kind::DIVISION:
    case Kind::REMAINDER:
    case Kind::DISJUNCTION:
    case Kind::CONJUNCTION:
    case Kind::XOR:
    case Kind::LEFT_SHIFT:
    case Kind::RIGHT_SHIFT:
      break;
    default:
      throw std::invalid_argument("unsupported assignment operation");
    }
  } else {
    if (kind == Kind::NOOP) {
      throw std::invalid_argument("unsupported non-assignment operation");
    }
  }
}

BinaryOperation::Kind BinaryOperation::kind() const noexcept { return m_kind; }

bool BinaryOperation::isAssignment() const noexcept { return m_isAssignment; }

Operation::Associativity BinaryOperation::associativity() const noexcept {
  return isAssignment() ? Associativity::RIGHT : Associativity ::LEFT;
}

int BinaryOperation::precedence() const noexcept {
  if (isAssignment()) {
    return 0;
  } else {
    switch (kind()) {
    case Kind::ARRAY_ACCESS:
      return 140;
    case Kind::MULTIPLICATION:
    case Kind::DIVISION:
    case Kind::REMAINDER:
      return 110;
    case Kind::ADDITION:
    case Kind::SUBTRACTION:
      return 100;
    case Kind::LEFT_SHIFT:
    case Kind::RIGHT_SHIFT:
      return 90;
    case Kind::CONJUNCTION:
      return 80;
    case Kind::XOR:
      return 70;
    case Kind::DISJUNCTION:
      return 60;
    case Kind::EQUALITY:
    case Kind::INEQUALITY:
    case Kind::LESS:
    case Kind::LESS_OR_EQUALS:
    case Kind::GREATER:
    case Kind::GREATER_OR_EQUALS:
      return 50;
    case Kind::LAZY_CONJUNCTION:
      return 40;
    case Kind::LAZY_DISJUNCTION:
      return 30;
    case Kind::RANGE:
      return 20;
    default:
      return 0;
    }
  }
}

bool BinaryOperation::isConstant(Context) const noexcept { return true; }

BinaryOperation::TypePointer BinaryOperation::type(const TypePointer &lhs,
                                                   const TypePointer &rhs,
                                                   Context context) const {
  if (!lhs->isSame(rhs, context)) {
    throw Exception("mismatched types", position());
  } else if (isAssignment() && !lhs->isMutable()) {
    throw Exception("cannot assign to an immutable type", position());
  }

  TypePointer resultType;

  if (lhs->compareWith(rhs, context) > 0) {
    resultType = rhs->clone();
  } else {
    resultType = lhs->clone();
  }

  resultType->setMutable(true);

  auto *primitiveType = dynamic_cast<const PrimitiveType *>(resultType.get());
  auto *referenceType = dynamic_cast<const ReferenceType *>(resultType.get());

  switch (kind()) {
  case Kind::NOOP:
    return nullptr;

  case Kind::ADDITION:
  case Kind::SUBTRACTION:
  case Kind::MULTIPLICATION:
  case Kind::DIVISION:
  case Kind::REMAINDER:
    if (primitiveType != nullptr) {
      switch (primitiveType->group()) {
      case PrimitiveType::Group::UNSIGNED_INT:
      case PrimitiveType::Group::SIGNED_INT:
      case PrimitiveType::Group::FLOAT:
        return isAssignment() ? nullptr : std::move(resultType);
      default:
        throw Exception("mismatched types", position());
      }
    }

    throw Exception("mismatched types", position());

  case Kind::DISJUNCTION:
  case Kind::CONJUNCTION:
  case Kind::XOR:
    if (primitiveType != nullptr) {
      switch (primitiveType->group()) {
      case PrimitiveType::Group::UNSIGNED_INT:
      case PrimitiveType::Group::SIGNED_INT:
      case PrimitiveType::Group::BOOLEAN:
        return isAssignment() ? nullptr : std::move(resultType);
      default:
        throw Exception("mismatched types", position());
      }
    }

    throw Exception("mismatched types", position());

  case Kind::LEFT_SHIFT:
  case Kind::RIGHT_SHIFT:
    if (primitiveType != nullptr) {
      switch (primitiveType->group()) {
      case PrimitiveType::Group::UNSIGNED_INT:
      case PrimitiveType::Group::SIGNED_INT:
        return isAssignment() ? nullptr : std::move(resultType);
      default:
        throw Exception("mismatched types", position());
      }
    }

    throw Exception("mismatched types", position());

  case Kind::EQUALITY:
  case Kind::INEQUALITY:
    if (primitiveType != nullptr) {
      switch (primitiveType->group()) {
      case PrimitiveType::Group::UNSIGNED_INT:
      case PrimitiveType::Group::SIGNED_INT:
      case PrimitiveType::Group::FLOAT:
      case PrimitiveType::Group::CHARACTER:
      case PrimitiveType::Group::STRING:
      case PrimitiveType::Group::BOOLEAN:
      case PrimitiveType::Group::POINTER:
        return std::make_unique<PrimitiveType>(PrimitiveType::Kind::BOOLEAN,
                                               true, position());
      default:
        throw Exception("mismatched types", position());
      }
    }

    if (referenceType != nullptr) {
      return std::make_unique<PrimitiveType>(PrimitiveType::Kind::BOOLEAN, true,
                                             position());
    }

    throw Exception("mismatched types", position());

  case Kind::GREATER:
  case Kind::GREATER_OR_EQUALS:
  case Kind::LESS:
  case Kind::LESS_OR_EQUALS:
    if (primitiveType != nullptr) {
      switch (primitiveType->group()) {
      case PrimitiveType::Group::UNSIGNED_INT:
      case PrimitiveType::Group::SIGNED_INT:
      case PrimitiveType::Group::FLOAT:
      case PrimitiveType::Group::CHARACTER:
      case PrimitiveType::Group::STRING:
      case PrimitiveType::Group::BOOLEAN:
        return std::make_unique<PrimitiveType>(PrimitiveType::Kind::BOOLEAN,
                                               true, position());
      default:
        throw Exception("mismatched types", position());
      }
    }

    throw Exception("mismatched types", position());

  case Kind::LAZY_CONJUNCTION:
  case Kind::LAZY_DISJUNCTION:
    if (primitiveType != nullptr) {
      switch (primitiveType->group()) {
      case PrimitiveType::Group::BOOLEAN:
        return std::move(resultType);
      default:
        throw Exception("mismatched types", position());
      }
    }

    throw Exception("mismatched types", position());

  case Kind::RANGE:
    if (primitiveType != nullptr) {
      switch (primitiveType->group()) {
      case PrimitiveType::Group::UNSIGNED_INT:
      case PrimitiveType::Group::SIGNED_INT:
        return std::make_unique<RangeType>(std::move(resultType), true,
                                           position());
      default:
        throw Exception("mismatched types", position());
      }
    }

    throw Exception("mismatched types", position());

  case Kind::ARRAY_ACCESS: {
    auto *arrayType = dynamic_cast<const ArrayType *>(lhs.get());

    if (arrayType == nullptr) {
      throw Exception("accessing non-array type by index", position());
    }

    if (!rhs->isSame(
            std::make_unique<PrimitiveType>(
                PrimitiveType::Kind::UNSIGNED_INT_SIZE, true, rhs->position()),
            context)) {
      throw Exception("array index type mismatch", rhs->position());
    }

    return arrayType->clone();
  }

  default:
    return nullptr;
  }
}
} // namespace smile::tree
