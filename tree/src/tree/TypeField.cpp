#include <tree/TypeField.hpp>

#include <tree/Context.hpp>
#include <tree/type/Type.hpp>

namespace smile::tree {
TypeField::TypeField(IdentifierPointer &&name, TypePointer &&type,
                     Position position)
    : BaseNode(position), m_name(std::move(name)), m_type(std::move(type)) {}

const TypeField::IdentifierPointer &TypeField::name() const { return m_name; }

const TypeField::TypePointer &TypeField::type() const { return m_type; }

void TypeField::validate(Context context) const { type()->validate(context); }
} // namespace smile::tree
