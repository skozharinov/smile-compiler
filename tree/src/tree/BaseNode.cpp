#include <tree/BaseNode.hpp>

#include <tree/Context.hpp>

namespace smile::tree {
BaseNode::BaseNode(Position position) : m_position(position) {}

Position BaseNode::position() const { return m_position; }

void BaseNode::validate(Context) const {}
} // namespace smile::tree
