#include <tree/statement/Conditional.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/operand/Operand.hpp>
#include <tree/statement/Compound.hpp>
#include <tree/type/PrimitiveType.hpp>

namespace smile::tree {
Conditional::Conditional(OperandPointer &&condition, CompoundPointer &&body,
                         ConditionalPointer &&alternative, Position position)
    : Statement(position), BaseNode(position),
      m_condition(std::move(condition)), m_body(std::move(body)),
      m_alternative(std::move(alternative)) {}

const Conditional::OperandPointer &Conditional::condition() const noexcept {
  return m_condition;
}

const Conditional::CompoundPointer &Conditional::body() const noexcept {
  return m_body;
}

const Conditional::ConditionalPointer &Conditional::alternative() const
    noexcept {
  return m_alternative;
}

void Conditional::validate(Context context) const {
  if (condition() == nullptr) {
    if (alternative() != nullptr) {
      throw Exception("unexpected alternative branch",
                      alternative()->position());
    }
  } else {
    condition()->calculateType(context);

    if (!condition()->type()->isSame(
            std::make_unique<PrimitiveType>(PrimitiveType::Kind::BOOLEAN, true,
                                            condition()->position()),
            context)) {
      throw Exception("condition is not a boolean", condition()->position());
    }

    if (alternative() != nullptr) {
      alternative()->validate(context);
    }
  }

  body()->validate(context);
}
} // namespace smile::tree
