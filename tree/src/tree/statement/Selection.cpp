#include <tree/statement/Selection.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/literal/Literal.hpp>
#include <tree/type/Type.hpp>

namespace smile::tree {
Selection::Selection(OperandPointer &&operand,
                     std::list<SelectionBranchPointer> &&branches,
                     Position position)
    : Statement(position), BaseNode(position), m_operand(std::move(operand)),
      m_branches(std::move(branches)) {}

const Selection::OperandPointer &Selection::operand() const noexcept {
  return m_operand;
}

const std::list<Selection::SelectionBranchPointer> &Selection::branches() const
    noexcept {
  return m_branches;
}

void Selection::validate(Context context) const {
  operand()->calculateType(context);

  for (auto &branch : branches()) {
    branch->validate(context);

    if (branch->operand() != nullptr &&
        !branch->operand()->type()->isSame(operand()->type(), context)) {
      throw Exception("mismatched types", branch->operand()->position());
    }
  }
}

SelectionBranch::SelectionBranch(OperandPointer &&operand,
                                 StatementPointer &&statement,
                                 Position position)
    : BaseNode(position), m_operand(std::move(operand)),
      m_statement(std::move(statement)) {}

const SelectionBranch::OperandPointer &SelectionBranch::operand() const
    noexcept {
  return m_operand;
}

const SelectionBranch::StatementPointer &SelectionBranch::statement() const
    noexcept {
  return m_statement;
}

void SelectionBranch::validate(Context context) const {
  if (operand() != nullptr) {
    operand()->calculateType(context);
  }

  statement()->validate(context);
}
} // namespace smile::tree
