#include <tree/statement/Compound.hpp>

#include <tree/Context.hpp>
#include <tree/statement/Statement.hpp>
#include <tree/statement/Variable.hpp>

namespace smile::tree {
Compound::Compound(std::list<StatementPointer> &&statements, Position position)
    : Statement(position), BaseNode(position),
      m_statements(std::move(statements)) {}

const std::list<Compound::StatementPointer> &Compound::statements() const
    noexcept {
  return m_statements;
}

void Compound::validate(Context context) const {
  for (auto &statement : statements()) {
    statement->validate(context);

    if (dynamic_cast<const Variable *>(statement.get()) != nullptr) {
      context += *dynamic_cast<const Variable *>(statement.get());
    }
  }
}
} // namespace smile::tree
