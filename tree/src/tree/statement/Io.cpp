#include <tree/statement/Io.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/operand/Operand.hpp>
#include <tree/type/ReferenceType.hpp>

namespace smile::tree {
Io::Io(Kind kind, OperandPointer &&operand, Position position)
    : Statement(position), BaseNode(position), m_kind(kind),
      m_operand(std::move(operand)) {}

Io::Kind Io::kind() const noexcept { return m_kind; }

const Io::OperandPointer &Io::operand() const noexcept { return m_operand; }

void Io::validate(Context context) const {
  operand()->calculateType(context);

  auto referenceType =
      dynamic_cast<const ReferenceType *>(operand()->type().get());

  if (referenceType == nullptr) {
    throw Exception("reading to non-reference type", position());
  } else if (referenceType->kind() != ReferenceType::Kind::REFERENCE) {
    throw Exception("cannot use pointer in io statement", position());
  }

  switch (kind()) {
  case Kind::READ:
    if (!referenceType->isMutable()) {
      throw Exception("reading to immutable reference", position());
    }

    break;
  case Kind::WRITE:
    break;
  }
}
} // namespace smile::tree
