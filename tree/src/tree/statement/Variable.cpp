#include <tree/statement/Variable.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/TypeField.hpp>
#include <tree/identifier/Identifier.hpp>
#include <tree/operand/Operand.hpp>
#include <tree/type/Type.hpp>

namespace smile::tree {
Variable::Variable(TypeFieldPointer &&field, bool isConstant,
                   OperandPointer &&firstValue, Position position)
    : Statement(position), BaseNode(position), m_field(std::move(field)),
      m_isConstant(isConstant), m_operand(std::move(firstValue)) {}

const Variable::TypeFieldPointer &Variable::field() const noexcept {
  return m_field;
}

bool Variable::isConstant() const noexcept { return m_isConstant; }

const Variable::OperandPointer &Variable::operand() const noexcept {
  return m_operand;
}

void Variable::validate(Context context) const {
  field()->validate(context);
  operand()->calculateType(context);

  if (!field()->type()->isSame(operand()->type(), context)) {
    throw Exception("mismatched types", operand()->position());
  }
}
} // namespace smile::tree
