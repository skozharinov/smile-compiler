#include <tree/statement/LoopBreaker.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>

namespace smile::tree {
LoopBreaker::LoopBreaker(Kind kind, Position position)
    : Statement(position), BaseNode(position), m_kind(kind) {}

LoopBreaker::Kind LoopBreaker::kind() const noexcept { return m_kind; }

void LoopBreaker::validate(Context context) const {
  if (!context.isInLoop()) {
    throw Exception("unexpected loop breaker statement", position());
  }
}
} // namespace smile::tree
