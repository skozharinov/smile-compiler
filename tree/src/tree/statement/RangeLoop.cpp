#include <tree/statement/RangeLoop.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/TypeField.hpp>
#include <tree/operand/Operand.hpp>
#include <tree/statement/Compound.hpp>
#include <tree/type/Type.hpp>

namespace smile::tree {
RangeLoop::RangeLoop(TypeFieldPointer &&field, OperandPointer &&range,
                     CompoundPointer &&body, Position position)
    : Statement(position), BaseNode(position), m_field(std::move(field)),
      m_range(std::move(range)), m_body(std::move(body)) {}

const RangeLoop::TypeFieldPointer &RangeLoop::field() const noexcept {
  return m_field;
}

const RangeLoop::OperandPointer &RangeLoop::range() const noexcept {
  return m_range;
}

const RangeLoop::CompoundPointer &RangeLoop::body() const noexcept {
  return m_body;
}

void RangeLoop::validate(Context context) const {
  field()->validate(context);
  range()->calculateType(context);

  auto &type = range()->type();

  if (type->iteratingType() == nullptr) {
    throw Exception("not an iterable", range()->position());
  }

  if (!range()->type()->iteratingType()->isSame(field()->type(), context)) {
    throw Exception("type mismatch", range()->position());
  }

  context += *field();
  context.setInLoop();

  body()->validate(context);
}
} // namespace smile::tree
