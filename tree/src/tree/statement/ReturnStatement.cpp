#include <tree/statement/ReturnStatement.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/operand/Operand.hpp>
#include <tree/type/Type.hpp>

namespace smile::tree {
tree::ReturnStatement::ReturnStatement(OperandPointer &&operand,
                                       Position position)
    : Statement(position), BaseNode(position), m_operand(std::move(operand)) {}

const ReturnStatement::OperandPointer &ReturnStatement::operand() const
    noexcept {
  return m_operand;
}

void ReturnStatement::validate(Context context) const {
  if (context.expected().get() == nullptr) {
    if (operand() != nullptr) {
      throw Exception("unexpected returned expression", operand()->position());
    }
  } else {
    operand()->calculateType(context);

    if (operand()->type() == nullptr) {
      throw Exception("expression return nothing", operand()->position());
    } else if (!operand()->type()->isSame(context.expected(), context)) {
      throw Exception("mismatched types", position());
    }
  }
}
} // namespace smile::tree
