#include <tree/statement/UnconditionalLoop.hpp>

#include <tree/Context.hpp>
#include <tree/statement/Compound.hpp>

namespace smile::tree {
UnconditionalLoop::UnconditionalLoop(CompoundPointer &&body, Position position)
    : Statement(position), BaseNode(position), m_body(std::move(body)) {}

const UnconditionalLoop::CompoundPointer &UnconditionalLoop::body() const
    noexcept {
  return m_body;
}

void UnconditionalLoop::validate(Context context) const {
  context.setInLoop();
  body()->validate(context);
}
} // namespace smile::tree
