#include <tree/statement/ConditionalLoop.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/operand/Operand.hpp>
#include <tree/statement/Compound.hpp>
#include <tree/type/PrimitiveType.hpp>

namespace smile::tree {
ConditionalLoop::ConditionalLoop(OperandPointer &&condition,
                                 CompoundPointer &&body, Position position)
    : Statement(position), BaseNode(position),
      m_condition(std::move(condition)), m_body(std::move(body)) {}

const ConditionalLoop::OperandPointer &ConditionalLoop::condition() const
    noexcept {
  return m_condition;
}

const ConditionalLoop::CompoundPointer &ConditionalLoop::body() const noexcept {
  return m_body;
}

void ConditionalLoop::validate(Context context) const {
  condition()->calculateType(context);

  if (!condition()->type()->isSame(
          std::make_unique<PrimitiveType>(PrimitiveType::Kind::BOOLEAN, true,
                                          condition()->position()),
          context)) {
    throw Exception("condition is not a boolean", condition()->position());
  }

  context.setInLoop();
  body()->validate(context);
}
} // namespace smile::tree
