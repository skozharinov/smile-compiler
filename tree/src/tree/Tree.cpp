#include <tree/Tree.hpp>

#include <common/Exception.hpp>
#include <tree/Context.hpp>
#include <tree/identifier/TypeIdentifier.hpp>
#include <tree/node/Function.hpp>
#include <tree/node/Implementation.hpp>
#include <tree/node/Import.hpp>
#include <tree/node/Node.hpp>
#include <tree/node/Structure.hpp>

#include <queue>
#include <utility>

using namespace std::literals;

namespace smile::tree {
Tree::Tree() = default;
Tree::Tree(Tree &&) noexcept = default;
Tree::~Tree() = default;

Tree &Tree::operator+=(Tree::NodePointer &&node) {
  m_nodes.push_back(std::move(node));
  return *this;
}

std::list<Tree::ImportPointer> Tree::extractImports() noexcept {
  std::list<ImportPointer> imports;

  for (auto &node : nodes()) {
    auto *import = dynamic_cast<Import *>(node.get());

    if (import != nullptr) {
      static_cast<void>(node.release());
      imports.push_back(std::unique_ptr<Import>(import));
    }
  }

  nodes().remove_if([](auto &node) { return !static_cast<bool>(node); });

  return std::move(imports);
}

void Tree::addToBeginning(Tree &&tree) noexcept {
  nodes().splice(nodes().begin(), std::move(tree.nodes()));
}

void Tree::rebuild() {
  std::queue<std::unique_ptr<Implementation>> implementations;

  for (auto &node : nodes()) {
    auto *structure = dynamic_cast<Structure *>(node.get());
    auto *function = dynamic_cast<Function *>(node.get());
    auto *implementation = dynamic_cast<Implementation *>(node.get());

    if (structure != nullptr) {
      if (m_structures.count(*structure->name()) > 0) {
        throw Exception("redeclaration of structure '"s +
                            structure->name()->contents() + "'"s,
                        structure->position());
      } else {
        m_structures.emplace(*structure->name(), *structure);
      }
    } else if (function != nullptr) {
      if (m_functions.count(*function->name()) > 0) {
        throw Exception("redeclaration of function '"s +
                            function->name()->contents() + "'"s,
                        function->position());
      } else {
        m_functions.emplace(*function->name(), *function);
      }
    } else if (implementation != nullptr) {
      static_cast<void>(node.release());
      implementations.push(std::unique_ptr<Implementation>(implementation));
    }
  }

  nodes().remove_if([](auto &node) { return !static_cast<bool>(node); });

  while (!implementations.empty()) {
    auto &implementation = implementations.front();

    if (m_structures.count(*implementation->name()) == 0) {
      throw Exception("structure '"s + implementation->name()->contents() +
                          "' is not defined"s,
                      implementation->name()->position());
    }

    auto &structure = m_structures.at(*implementation->name()).get();

    for (auto &method : implementation->methods()) {
      if (structure.method(method->name()) != nullptr) {
        throw Exception("redeclaration of method '"s +
                            method->name()->contents() + "' of structure '"s +
                            structure.name()->contents() + "'"s,
                        method->position());
      } else {
        structure += std::move(method);
      }
    }

    implementations.pop();
  }
}

void Tree::validate() const {
  tree::Context baseContext;

  for (auto &function : m_functions) {
    baseContext += function.second;
  }

  for (auto &structure : m_structures) {
    baseContext += structure.second;
  }

  for (auto &node : nodes()) {
    node->validate(baseContext);
  }
}

Tree &Tree::operator=(Tree &&) noexcept = default;

std::list<Tree::NodePointer> &Tree::nodes() noexcept { return m_nodes; }

const std::list<Tree::NodePointer> &Tree::nodes() const noexcept {
  return m_nodes;
}
} // namespace smile::tree
