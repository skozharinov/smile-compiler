#include <tree/literal/StringLiteral.hpp>

#include <tree/Context.hpp>
#include <tree/type/PrimitiveType.hpp>

namespace smile::tree {
StringLiteral::StringLiteral(std::string &&value, Position position)
    : Literal(position), Operand(position), Statement(position),
      Token(position), BaseNode(position), m_value(std::move(value)) {}

const std::string &StringLiteral::value() const noexcept { return m_value; }

void StringLiteral::calculateType(Context) {
  setType(std::make_unique<PrimitiveType>(PrimitiveType::Kind::STRING, true,
                                          position()));
}
} // namespace smile::tree
