#include <tree/literal/NullPointer.hpp>

#include <tree/Context.hpp>
#include <tree/type/PrimitiveType.hpp>

namespace smile::tree {
NullPointer::NullPointer(Position position)
    : Literal(position), Operand(position), Statement(position),
      Token(position), BaseNode(position) {}

void NullPointer::calculateType(Context) {
  setType(std::make_unique<PrimitiveType>(PrimitiveType::Kind::NULL_POINTER,
                                          true, position()));
}
} // namespace smile::tree
