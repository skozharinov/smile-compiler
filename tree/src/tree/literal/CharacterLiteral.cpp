#include <tree/literal/CharacterLiteral.hpp>

#include <tree/Context.hpp>
#include <tree/type/PrimitiveType.hpp>

namespace smile::tree {
CharacterLiteral::CharacterLiteral(char value, Position position)
    : Literal(position), Operand(position), Statement(position),
      Token(position), BaseNode(position), m_value(value) {}

char CharacterLiteral::value() const noexcept { return m_value; }

void CharacterLiteral::calculateType(Context) {
  setType(std::make_unique<PrimitiveType>(PrimitiveType::Kind::CHARACTER, true,
                                          position()));
}
} // namespace smile::tree
