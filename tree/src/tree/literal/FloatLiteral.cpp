#include <tree/literal/FloatLiteral.hpp>

#include <tree/Context.hpp>
#include <tree/type/PrimitiveType.hpp>

namespace smile::tree {
FloatLiteral::FloatLiteral(double value, Position position)
    : Literal(position), Operand(position), Statement(position),
      Token(position), BaseNode(position), m_value(value) {}

double FloatLiteral::value() const noexcept { return m_value; }

void FloatLiteral::calculateType(Context) {
  setType(std::make_unique<PrimitiveType>(PrimitiveType::Kind::FLOAT_LITERAL,
                                          true, position()));
}
} // namespace smile::tree
