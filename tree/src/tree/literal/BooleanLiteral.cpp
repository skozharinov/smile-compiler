#include <tree/literal/BooleanLiteral.hpp>

#include <tree/Context.hpp>
#include <tree/type/PrimitiveType.hpp>

namespace smile::tree {
BooleanLiteral::BooleanLiteral(bool value, Position position)
    : Literal(position), Operand(position), Statement(position),
      Token(position), BaseNode(position), m_value(value) {}

bool BooleanLiteral::value() const noexcept { return false; }

void BooleanLiteral::calculateType(Context) {
  setType(std::make_unique<PrimitiveType>(PrimitiveType::Kind::BOOLEAN, true,
                                          position()));
}
} // namespace smile::tree
