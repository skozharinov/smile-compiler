#include <tree/literal/IntegerLiteral.hpp>

#include <tree/Context.hpp>
#include <tree/type/PrimitiveType.hpp>

namespace smile::tree {
IntegerLiteral::IntegerLiteral(std::uint64_t value, Position position)
    : Literal(position), Operand(position), Token(position),
      Statement(position), BaseNode(position), m_value(value) {}

std::uint64_t IntegerLiteral::value() const noexcept { return m_value; }

void IntegerLiteral::calculateType(Context) {
  setType(std::make_unique<PrimitiveType>(
      PrimitiveType::Kind::UNSIGNED_INT_LITERAL, true, position()));
}
} // namespace smile::tree
