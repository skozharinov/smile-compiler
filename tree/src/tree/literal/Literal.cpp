#include <tree/literal/Literal.hpp>

#include <tree/Context.hpp>

namespace smile::tree {
Literal::Literal(Position position)
    : Operand(position), Statement(position), Token(position),
      BaseNode(position) {}

bool Literal::isConstant(Context) const noexcept { return true; }
} // namespace smile::tree
