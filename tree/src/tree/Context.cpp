#include <tree/Context.hpp>

#include <tree/TypeField.hpp>
#include <tree/identifier/TypeIdentifier.hpp>
#include <tree/node/Function.hpp>
#include <tree/node/Structure.hpp>
#include <tree/statement/Variable.hpp>

namespace smile::tree {
static const Context::TypePointer DEFAULT_TYPE = nullptr;

Context::Context() : m_expectedType(DEFAULT_TYPE), m_inLoop(false) {}

bool Context::hasStructure(IdentifierReference identifier) const {
  return structures().count(identifier) > 0;
}

bool Context::hasFunction(IdentifierReference identifier) const {
  return functions().count(identifier) > 0;
}

bool Context::hasVariable(IdentifierReference identifier) const {
  return variables().count(identifier) > 0;
}

bool Context::hasField(IdentifierReference identifier) const {
  return fields().count(identifier) > 0;
}

Context::StructureReference
Context::structure(IdentifierReference identifier) const {
  return structures().at(identifier);
}

Context::FunctionReference
Context::function(IdentifierReference identifier) const {
  return functions().at(identifier);
}

Context::VariableReference
Context::variable(IdentifierReference identifier) const {
  return variables().at(identifier);
}

Context::TypeFieldReference
Context::field(IdentifierReference identifier) const {
  return fields().at(identifier);
}

bool Context::isInLoop() const { return m_inLoop; }

void Context::setInLoop() { m_inLoop = true; }

Context::TypePointerReference Context::expected() const {
  return m_expectedType;
}

void Context::setExpected(TypePointerReference type) { m_expectedType = type; }

Context Context::operator+(StructureReference structure) const {
  return Context(*this) += structure;
}

Context Context::operator+(FunctionReference function) const {
  return Context(*this) += function;
}

Context Context::operator+(VariableReference variable) const {
  return Context(*this) += variable;
}

Context Context::operator+(TypeFieldReference field) const {
  return Context(*this) += field;
}

Context &Context::operator+=(StructureReference structure) {
  structures().emplace(*structure.get().name(), structure);
  return *this;
}

Context &Context::operator+=(FunctionReference function) {
  functions().emplace(*function.get().name(), function);
  return *this;
}

Context &Context::operator+=(VariableReference variable) {
  fields().erase(*variable.get().field()->name());
  variables().erase(*variable.get().field()->name());
  variables().emplace(*variable.get().field()->name(), variable);
  return *this;
}

Context &Context::operator+=(TypeFieldReference field) {
  fields().erase(*field.get().name());
  variables().erase(*field.get().name());
  fields().emplace(*field.get().name(), field);
  return *this;
}

Context::IdentifierReferenceMap<Context::StructureReference> &
Context::structures() noexcept {
  return m_structures;
}

const Context::IdentifierReferenceMap<Context::StructureReference> &
Context::structures() const noexcept {
  return m_structures;
}

Context::IdentifierReferenceMap<Context::FunctionReference> &
Context::functions() noexcept {
  return m_functions;
}

const Context::IdentifierReferenceMap<Context::FunctionReference> &
Context::functions() const noexcept {
  return m_functions;
}

Context::IdentifierReferenceMap<Context::VariableReference> &
Context::variables() noexcept {
  return m_variables;
}

const Context::IdentifierReferenceMap<Context::VariableReference> &
Context::variables() const noexcept {
  return m_variables;
}

Context::IdentifierReferenceMap<Context::TypeFieldReference> &
Context::fields() noexcept {
  return m_fields;
}

const Context::IdentifierReferenceMap<Context::TypeFieldReference> &
Context::fields() const noexcept {
  return m_fields;
}
} // namespace smile::tree
