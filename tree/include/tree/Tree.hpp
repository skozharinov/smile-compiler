#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_TREE_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_TREE_HPP

#include <tree/identifier/Identifier.hpp>

#include <functional>
#include <list>
#include <memory>
#include <queue>

namespace smile::tree {
class Node;

class Import;

class Structure;

class Function;

class Tree {
public:
  using NodePointer = std::unique_ptr<Node>;
  using ImportPointer = std::unique_ptr<Import>;
  using IdentifierReference = std::reference_wrapper<const Identifier>;
  using StructureReference = std::reference_wrapper<Structure>;
  using FunctionReference = std::reference_wrapper<Function>;
  template <class T>
  using IdentifierReferenceMap =
      std::unordered_map<IdentifierReference, T, IdentifierHash,
                         std::equal_to<Identifier>>;

  Tree();
  Tree(const Tree &) = delete;
  Tree(Tree &&) noexcept;
  ~Tree();

  std::list<ImportPointer> extractImports() noexcept;
  void addToBeginning(Tree &&) noexcept;
  void rebuild();
  void validate() const;

  Tree &operator=(const Tree &) = delete;
  Tree &operator=(Tree &&) noexcept;

  Tree &operator+=(NodePointer &&);

protected:
  std::list<NodePointer> &nodes() noexcept;
  const std::list<NodePointer> &nodes() const noexcept;

private:
  std::list<NodePointer> m_nodes;
  IdentifierReferenceMap<StructureReference> m_structures;
  IdentifierReferenceMap<FunctionReference> m_functions;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_TREE_HPP
