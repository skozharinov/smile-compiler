#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_SELECTION_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_SELECTION_HPP

#include <tree/statement/Statement.hpp>

#include <list>
#include <memory>

namespace smile::tree {
class Operand;
class SelectionBranch;

class Selection : public Statement {
public:
  using SelectionBranchPointer = std::unique_ptr<SelectionBranch>;

  Selection(OperandPointer &&, std::list<SelectionBranchPointer> &&, Position);

  const OperandPointer &operand() const noexcept;
  const std::list<SelectionBranchPointer> &branches() const noexcept;

  void validate(Context) const override;

private:
  OperandPointer m_operand;
  std::list<SelectionBranchPointer> m_branches;
};

class SelectionBranch : public BaseNode {
public:
  using StatementPointer = std::unique_ptr<Statement>;

  SelectionBranch(OperandPointer &&, StatementPointer &&, Position);

  const OperandPointer &operand() const noexcept;
  const StatementPointer &statement() const noexcept;

  void validate(Context) const override;

private:
  OperandPointer m_operand;
  StatementPointer m_statement;
};
} // namespace smile::tree

#endif
