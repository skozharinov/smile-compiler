#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_LOOPBREAKER_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_LOOPBREAKER_HPP

#include <tree/statement/Statement.hpp>

namespace smile::tree {
class LoopBreaker : public Statement {
public:
  enum class Kind { BREAK, CONTINUE };

  LoopBreaker(Kind, Position);

  Kind kind() const noexcept;

  void validate(Context) const override;

private:
  Kind m_kind;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_LOOPBREAKER_HPP
