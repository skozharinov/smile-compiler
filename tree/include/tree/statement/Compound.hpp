#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_COMPOUND_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_COMPOUND_HPP

#include <tree/statement/Statement.hpp>

#include <list>
#include <memory>

namespace smile::tree {
class Context;

class Compound : public Statement {
public:
  using StatementPointer = std::unique_ptr<Statement>;

  Compound(std::list<StatementPointer> &&, Position);

  const std::list<StatementPointer> &statements() const noexcept;

  void validate(Context) const override;

private:
  std::list<StatementPointer> m_statements;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_COMPOUND_HPP
