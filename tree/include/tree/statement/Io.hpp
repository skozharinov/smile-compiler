#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_IO_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_IO_HPP

#include <tree/statement/Statement.hpp>

#include <memory>

namespace smile::tree {
class Operand;

class Io : public Statement {
public:
  enum class Kind { READ, WRITE };

  Io(Kind, OperandPointer &&, Position);

  Kind kind() const noexcept;
  const OperandPointer &operand() const noexcept;

  void validate(Context) const override;

private:
  Kind m_kind;
  OperandPointer m_operand;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_IO_HPP
