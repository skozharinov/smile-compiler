#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_CONDITIONAL_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_CONDITIONAL_HPP

#include <tree/statement/Statement.hpp>

#include <memory>

namespace smile::tree {
class Operand;

class Compound;

class Conditional : public Statement {
public:
  using CompoundPointer = std::unique_ptr<Compound>;
  using ConditionalPointer = std::unique_ptr<Conditional>;

  Conditional(OperandPointer &&, CompoundPointer &&, ConditionalPointer &&,
              Position);

  const OperandPointer &condition() const noexcept;
  const CompoundPointer &body() const noexcept;
  const ConditionalPointer &alternative() const noexcept;

  void validate(Context) const override;

private:
  OperandPointer m_condition;
  CompoundPointer m_body;
  ConditionalPointer m_alternative;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_CONDITIONAL_HPP
