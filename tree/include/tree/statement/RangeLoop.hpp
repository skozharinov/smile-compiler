#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_RANGELOOP_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_RANGELOOP_HPP

#include <tree/statement/Statement.hpp>

#include <memory>

namespace smile::tree {
class TypeField;

class Operand;

class Compound;

class RangeLoop : public Statement {
public:
  using TypeFieldPointer = std::unique_ptr<TypeField>;
  using CompoundPointer = std::unique_ptr<Compound>;

  RangeLoop(TypeFieldPointer &&, OperandPointer &&, CompoundPointer &&,
            Position);

  const TypeFieldPointer &field() const noexcept;
  const OperandPointer &range() const noexcept;
  const CompoundPointer &body() const noexcept;

  void validate(Context) const override;

private:
  TypeFieldPointer m_field;
  OperandPointer m_range;
  CompoundPointer m_body;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_RANGELOOP_HPP
