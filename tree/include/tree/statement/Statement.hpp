#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_STATEMENT_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_STATEMENT_HPP

#include <tree/BaseNode.hpp>

namespace smile::tree {
class Statement : virtual public BaseNode {
public:
  explicit Statement(Position);
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_STATEMENT_HPP
