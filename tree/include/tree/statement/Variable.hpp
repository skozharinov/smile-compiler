#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_VARIABLE_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_VARIABLE_HPP

#include <tree/statement/Statement.hpp>

#include <memory>

namespace smile::tree {
class TypeField;

class Operand;

class Variable : public Statement {
public:
  using TypeFieldPointer = std::unique_ptr<TypeField>;

  Variable(TypeFieldPointer &&, bool, OperandPointer &&, Position);

  const TypeFieldPointer &field() const noexcept;
  bool isConstant() const noexcept;
  const OperandPointer &operand() const noexcept;

  void validate(Context) const override;

private:
  TypeFieldPointer m_field;
  bool m_isConstant;
  OperandPointer m_operand;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_VARIABLE_HPP
