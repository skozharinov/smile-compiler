#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_UNCONDITIONALLOOP_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_UNCONDITIONALLOOP_HPP

#include <tree/statement/Statement.hpp>

#include <memory>

namespace smile::tree {
class Compound;

class UnconditionalLoop : public Statement {
public:
  using CompoundPointer = std::unique_ptr<Compound>;

  UnconditionalLoop(CompoundPointer &&, Position);

  const CompoundPointer &body() const noexcept;

  void validate(Context) const override;

private:
  CompoundPointer m_body;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_UNCONDITIONALLOOP_HPP
