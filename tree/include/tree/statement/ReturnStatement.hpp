#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_RETURNSTATEMENT_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_RETURNSTATEMENT_HPP

#include <tree/statement/Statement.hpp>

#include <memory>

namespace smile::tree {
class Operand;

class ReturnStatement : public Statement {
public:
  ReturnStatement(OperandPointer &&, Position);

  const OperandPointer &operand() const noexcept;

  void validate(Context) const override;

private:
  OperandPointer m_operand;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_RETURNSTATEMENT_HPP
