#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_CONDITIONALLOOP_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_CONDITIONALLOOP_HPP

#include <tree/statement/Statement.hpp>

#include <memory>

namespace smile::tree {
class Operand;

class Compound;

class ConditionalLoop : public Statement {
public:
  using CompoundPointer = std::unique_ptr<Compound>;

  ConditionalLoop(OperandPointer &&, CompoundPointer &&, Position);

  const OperandPointer &condition() const noexcept;
  const CompoundPointer &body() const noexcept;

  void validate(Context) const override;

private:
  OperandPointer m_condition;
  CompoundPointer m_body;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_STATEMENT_CONDITIONALLOOP_HPP
