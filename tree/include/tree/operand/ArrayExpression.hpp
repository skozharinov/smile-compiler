#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_OPERAND_ARRAYEXPRESSION_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_OPERAND_ARRAYEXPRESSION_HPP

#include <tree/operand/Operand.hpp>

#include <list>

namespace smile::tree {
class ArrayExpression : public Operand {
public:
  ArrayExpression(std::list<OperandPointer> &&, Position);

  const std::list<OperandPointer> &operands() const noexcept;

  bool isConstant(Context) const noexcept override;
  void calculateType(Context) override;

private:
  std::list<OperandPointer> m_operands;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_OPERAND_ARRAYEXPRESSION_HPP
