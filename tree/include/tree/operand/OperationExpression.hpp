#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_OPERAND_OPERATIONEXPRESSION_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_OPERAND_OPERATIONEXPRESSION_HPP

#include <tree/operand/Operand.hpp>

#include <list>

namespace smile::tree {
class OperationExpression : public Operand {
public:
  using TokenPointer = std::unique_ptr<Token>;

  OperationExpression(std::list<TokenPointer>, Position);

  const std::list<TokenPointer> &infix() const;
  std::list<std::reference_wrapper<const TokenPointer>> toPostfix() const;

  bool isConstant(Context) const noexcept override;
  void calculateType(Context) override;

private:
  std::list<TokenPointer> m_infix;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_OPERAND_OPERATIONEXPRESSION_HPP
