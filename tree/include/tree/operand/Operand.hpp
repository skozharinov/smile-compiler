#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_OPERAND_OPERAND_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_OPERAND_OPERAND_HPP

#include <tree/statement/Statement.hpp>
#include <tree/token/Token.hpp>

#include <memory>

namespace smile::tree {
class Operand : virtual public Token, virtual public Statement {
public:
  explicit Operand(Position);
  ~Operand() override;

  virtual void calculateType(Context);

  const TypePointer &type() const;
  void setType(TypePointer &&);

  void validate(Context) const override final;

private:
  TypePointer m_calculatedType;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_OPERAND_OPERAND_HPP
