#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_OPERAND_STRUCTUREEXPRESSION_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_OPERAND_STRUCTUREEXPRESSION_HPP

#include <tree/operand/Operand.hpp>

#include <list>

namespace smile::tree {
class TypeIdentifier;

class OperandField;

class Context;

class StructureExpression : public Operand {
public:
  using TypeIdentifierPointer = std::unique_ptr<TypeIdentifier>;
  using OperandFieldPointer = std::unique_ptr<OperandField>;

  StructureExpression(TypeIdentifierPointer &&,
                      std::list<OperandFieldPointer> &&, Position);

  const TypeIdentifierPointer &name() const noexcept;
  const std::list<OperandFieldPointer> &fields() const noexcept;

  bool isConstant(Context) const noexcept override;

  void calculateType(Context) override;

private:
  TypeIdentifierPointer m_name;
  std::list<OperandFieldPointer> m_fields;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_OPERAND_STRUCTUREEXPRESSION_HPP
