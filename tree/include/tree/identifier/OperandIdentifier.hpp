#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_IDENTIFIER_OPERANDIDENTIFIER_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_IDENTIFIER_OPERANDIDENTIFIER_HPP

#include <tree/identifier/Identifier.hpp>
#include <tree/operand/Operand.hpp>

namespace smile::tree {
class OperandIdentifier : public Identifier, public Operand {
public:
  OperandIdentifier(std::string &&, Position);

  bool isConstant(Context) const noexcept override;

  void calculateType(Context) override;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_IDENTIFIER_OPERANDIDENTIFIER_HPP
