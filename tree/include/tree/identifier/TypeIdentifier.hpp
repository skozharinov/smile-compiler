#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_IDENTIFIER_TYPEIDENTIFIER_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_IDENTIFIER_TYPEIDENTIFIER_HPP

#include <tree/identifier/Identifier.hpp>
#include <tree/type/Type.hpp>

namespace smile::tree {
class TypeIdentifier : public Identifier, public Type {
public:
  TypeIdentifier(std::string &&, bool, Position);

  bool isSame(const TypePointer &, Context) const noexcept override;
  TypePointer clone() const noexcept override;

  void validate(Context) const override;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_IDENTIFIER_TYPEIDENTIFIER_HPP
