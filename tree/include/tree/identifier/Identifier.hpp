#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_IDENTIFIER_IDENTIFIER_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_IDENTIFIER_IDENTIFIER_HPP

#include <tree/BaseNode.hpp>

#include <string>

namespace smile::tree {
class Identifier : virtual public BaseNode {
public:
  Identifier(std::string &&, Position);

  std::string contents() const noexcept;

  bool operator==(const Identifier &) const;

private:
  std::string m_contents;
};

struct IdentifierHash {
  std::size_t operator()(const Identifier &) const;
  std::hash<std::string> m_hashFunctor;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_IDENTIFIER_IDENTIFIER_HPP
