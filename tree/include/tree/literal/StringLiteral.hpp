#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_STRINGLITERAL_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_STRINGLITERAL_HPP

#include <tree/literal/Literal.hpp>

#include <string>

namespace smile::tree {
class StringLiteral : public Literal {
public:
  StringLiteral(std::string &&, Position);

  const std::string &value() const noexcept;

  void calculateType(Context) override;

private:
  std::string m_value;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_STRINGLITERAL_HPP
