#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_NULLPOINTER_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_NULLPOINTER_HPP

#include <tree/literal/Literal.hpp>

namespace smile::tree {
class NullPointer : public Literal {
public:
  explicit NullPointer(Position);

  void calculateType(Context) override;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_NULLPOINTER_HPP
