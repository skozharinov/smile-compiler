#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_CHARACTERLITERAL_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_CHARACTERLITERAL_HPP

#include <tree/literal/Literal.hpp>

#include <string>

namespace smile::tree {
class CharacterLiteral : public Literal {
public:
  CharacterLiteral(char, Position);

  char value() const noexcept;

  void calculateType(Context) override;

private:
  char m_value;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_CHARACTERLITERAL_HPP
