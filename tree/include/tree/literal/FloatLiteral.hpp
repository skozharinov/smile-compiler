#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_FLOATLITERAL_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_FLOATLITERAL_HPP

#include <tree/literal/Literal.hpp>

namespace smile::tree {
class FloatLiteral : public Literal {
public:
  FloatLiteral(double, Position);

  double value() const noexcept;

  void calculateType(Context) override;

private:
  double m_value;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_FLOATLITERAL_HPP
