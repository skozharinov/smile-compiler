#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_INTEGERLITERAL_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_INTEGERLITERAL_HPP

#include <tree/literal/Literal.hpp>

namespace smile::tree {
class IntegerLiteral : public Literal {
public:
  IntegerLiteral(std::uint64_t, Position);

  std::uint64_t value() const noexcept;

  void calculateType(Context) override;

private:
  std::uint64_t m_value;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_INTEGERLITERAL_HPP
