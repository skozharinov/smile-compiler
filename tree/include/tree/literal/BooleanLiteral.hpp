#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_BOOLEANLITERAL_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_BOOLEANLITERAL_HPP

#include <tree/literal/Literal.hpp>

namespace smile::tree {
class BooleanLiteral : public Literal {
public:
  BooleanLiteral(bool, Position);

  bool value() const noexcept;

  void calculateType(Context) override;

private:
  bool m_value;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_BOOLEANLITERAL_HPP
