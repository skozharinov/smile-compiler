#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_LITERAL_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_LITERAL_HPP

#include <tree/operand/Operand.hpp>

namespace smile::tree {
class Literal : virtual public Operand {
public:
  explicit Literal(Position);

  bool isConstant(Context) const noexcept override;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_LITERAL_LITERAL_HPP
