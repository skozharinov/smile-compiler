#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_BINARYOPERATION_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_BINARYOPERATION_HPP

#include <tree/operation/Operation.hpp>

namespace smile::tree {
class BinaryOperation : public Operation {
public:
  enum class Kind {
    NOOP,
    ADDITION,
    SUBTRACTION,
    MULTIPLICATION,
    DIVISION,
    REMAINDER,
    DISJUNCTION,
    CONJUNCTION,
    XOR,
    LEFT_SHIFT,
    RIGHT_SHIFT,
    EQUALITY,
    INEQUALITY,
    GREATER,
    GREATER_OR_EQUALS,
    LESS,
    LESS_OR_EQUALS,
    LAZY_CONJUNCTION,
    LAZY_DISJUNCTION,
    RANGE,
    ARRAY_ACCESS
  };

  BinaryOperation(Kind, bool, Position);

  Kind kind() const noexcept;

  bool isAssignment() const noexcept;

  Associativity associativity() const noexcept override;

  int precedence() const noexcept override;

  TypePointer type(const TypePointer &, const TypePointer &,
                   Context) const override;

  bool isConstant(Context) const noexcept override;

private:
  Kind m_kind;
  bool m_isAssignment;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_BINARYOPERATION_HPP
