#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_STRUCTUREACCESS_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_STRUCTUREACCESS_HPP

#include <tree/operation/Operation.hpp>

#include <memory>

namespace smile::tree {
class Identifier;

class StructureAccess : public Operation {
public:
  StructureAccess(IdentifierPointer &&, Position);

  const IdentifierPointer &field() const noexcept;

  TypePointer type(const TypePointer &, Context) const override;

  bool isConstant(Context) const noexcept override;

private:
  IdentifierPointer m_field;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_STRUCTUREACCESS_HPP
