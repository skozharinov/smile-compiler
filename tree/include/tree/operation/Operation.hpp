#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_OPERATION_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_OPERATION_HPP

#include <tree/token/Token.hpp>

#include <memory>

namespace smile::tree {
class Type;

class Operation : virtual public Token {
public:
  enum class Arity { SINGLE, DOUBLE };

  enum class Associativity { LEFT, RIGHT };

  Operation(Arity, Associativity, int, Position);

  virtual Arity arity() const noexcept;
  virtual Associativity associativity() const noexcept;
  virtual int precedence() const noexcept;

  virtual TypePointer type(const TypePointer &, Context) const;
  virtual TypePointer type(const TypePointer &, const TypePointer &,
                           Context) const;

private:
  const Arity m_arity;
  const Associativity m_associativity;
  const int m_precedence;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_OPERATION_HPP
