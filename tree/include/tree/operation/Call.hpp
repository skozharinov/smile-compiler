#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_CALL_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_CALL_HPP

#include <tree/operation/Operation.hpp>

#include <list>

namespace smile::tree {
class Operand;

class Call : public Operation {
public:
  Call(std::list<OperandPointer> &&, Position);

  const std::list<OperandPointer> &arguments() const noexcept;

  TypePointer type(const TypePointer &, Context) const override;

  void validate(Context) const override;

private:
  std::list<OperandPointer> m_arguments;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_CALL_HPP
