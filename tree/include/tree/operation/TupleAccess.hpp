#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_TUPLEACCESS_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_TUPLEACCESS_HPP

#include <tree/operation/Operation.hpp>

#include <memory>

namespace smile::tree {
class IntegerLiteral;

class TupleAccess : public Operation {
public:
  using IntegerLiteralPointer = std::unique_ptr<IntegerLiteral>;

  TupleAccess(IntegerLiteralPointer &&, Position);

  const IntegerLiteralPointer &field() const noexcept;

  TypePointer type(const TypePointer &, Context) const override;

  bool isConstant(Context) const noexcept override;

private:
  IntegerLiteralPointer m_field;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_TUPLEACCESS_HPP
