#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_UNARYOPERATION_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_UNARYOPERATION_HPP

#include <tree/operation/Operation.hpp>

namespace smile::tree {
class UnaryOperation : public Operation {
public:
  enum class Kind {
    REFERENCE,
    MUTABLE_REFERENCE,
    DEREFERENCE,
    NEGATIVE,
    POSITIVE,
    NEGATION,
  };

  UnaryOperation(Kind, Position);

  Kind kind() const noexcept;

  TypePointer type(const TypePointer &, Context) const override;

  bool isConstant(Context) const noexcept override;

private:
  Kind m_kind;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_UNARYOPERATION_HPP
