#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_CAST_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_CAST_HPP

#include <tree/operation/Operation.hpp>

namespace smile::tree {
class Type;

class Cast : public Operation {
public:
  Cast(TypePointer &&, Position);

  const TypePointer &newType() const noexcept;

  TypePointer type(const TypePointer &, Context) const override;

  bool isConstant(Context) const noexcept override;

  void validate(Context) const override;

private:
  TypePointer m_newType;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_OPERATION_CAST_HPP
