#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_TYPEFIELD_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_TYPEFIELD_HPP

#include <tree/BaseNode.hpp>

#include <memory>

namespace smile::tree {
class Identifier;

class Type;

class TypeField : public BaseNode {
public:
  using TypePointer = TypePointer;

  TypeField(IdentifierPointer &&, TypePointer &&, Position);

  const IdentifierPointer &name() const;
  const TypePointer &type() const;

  void validate(Context) const override;

private:
  IdentifierPointer m_name;
  TypePointer m_type;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_TYPEFIELD_HPP
