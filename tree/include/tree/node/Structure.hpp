#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_NODE_STRUCTURE_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_NODE_STRUCTURE_HPP

#include <tree/node/Node.hpp>

#include <list>
#include <memory>

namespace smile::tree {
class TypeIdentifier;

class TypeField;

class Function;

class Identifier;

class Structure : public Node {
public:
  using TypeIdentifierPointer = std::unique_ptr<TypeIdentifier>;
  using TypeFieldPointer = std::unique_ptr<TypeField>;
  using FunctionPointer = std::unique_ptr<Function>;

  enum class MethodKind {
    IMMUTABLE_REFERENCE,
    MUTABLE_REFERENCE,
    VALUE,
    STATIC
  };

  Structure(TypeIdentifierPointer &&, std::list<TypeFieldPointer> &&, Position);
  ~Structure() override;

  const TypeIdentifierPointer &name() const noexcept;
  const std::list<TypeFieldPointer> &fields() const noexcept;
  const std::list<FunctionPointer> &methods() const noexcept;

  const TypeFieldPointer &field(const IdentifierPointer &) const noexcept;
  const FunctionPointer &method(const IdentifierPointer &) const noexcept;

  MethodKind methodKind(const IdentifierPointer &, Context) const noexcept;

  void validate(Context) const override;

  Structure &operator+=(FunctionPointer &&) noexcept;

protected:
  std::list<TypeFieldPointer> &fields() noexcept;
  std::list<FunctionPointer> &methods() noexcept;

private:
  TypeIdentifierPointer m_name;
  std::list<TypeFieldPointer> m_fields;
  std::list<FunctionPointer> m_methods;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_NODE_STRUCTURE_HPP
