#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_NODE_IMPORT_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_NODE_IMPORT_HPP

#include <tree/node/Node.hpp>

#include <memory>

namespace smile::tree {
class Identifier;

class Import : public Node {
public:
  Import(IdentifierPointer &&, Position);
  const IdentifierPointer &path() const noexcept;

private:
  IdentifierPointer m_path;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_NODE_IMPORT_HPP
