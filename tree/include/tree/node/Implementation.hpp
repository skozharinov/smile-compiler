#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_NODE_IMPLEMENTATION_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_NODE_IMPLEMENTATION_HPP

#include <tree/node/Node.hpp>

#include <list>
#include <memory>

namespace smile::tree {
class TypeIdentifier;

class Function;

class Implementation : public Node {
public:
  using TypeIdentifierPointer = std::unique_ptr<TypeIdentifier>;
  using FunctionPointer = std::unique_ptr<Function>;

  Implementation(TypeIdentifierPointer &&, std::list<FunctionPointer> &&,
                 Position);
  ~Implementation() override;

  const TypeIdentifierPointer &name() const noexcept;
  const std::list<FunctionPointer> &methods() const noexcept;
  std::list<FunctionPointer> &methods() noexcept;

  void validate(Context) const override;

private:
  TypeIdentifierPointer m_name;
  std::list<FunctionPointer> m_methods;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_NODE_IMPLEMENTATION_HPP
