#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_NODE_NODE_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_NODE_NODE_HPP

#include <tree/BaseNode.hpp>

namespace smile::tree {
class Node : virtual public BaseNode {
public:
  explicit Node(Position);
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_NODE_NODE_HPP
