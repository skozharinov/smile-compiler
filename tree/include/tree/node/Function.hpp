#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_NODE_FUNCTION_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_NODE_FUNCTION_HPP

#include <tree/node/Node.hpp>

#include <list>
#include <memory>

namespace smile::tree {
class Identifier;

class TypeField;

class Type;

class Compound;

class Context;

class Function : public Node {
public:
  using TypeFieldPointer = std::unique_ptr<tree::TypeField>;
  using CompoundPointer = std::unique_ptr<Compound>;

  Function(IdentifierPointer &&, std::list<TypeFieldPointer> &&, TypePointer &&,
           CompoundPointer &&, Position);
  ~Function() override;

  const IdentifierPointer &name() const noexcept;
  const std::list<TypeFieldPointer> &arguments() const noexcept;
  const TypePointer &returnType() const noexcept;
  const CompoundPointer &body() const noexcept;

  TypePointer type() const noexcept;

  void validate(Context) const override;

private:
  IdentifierPointer m_name;
  std::list<TypeFieldPointer> m_arguments;
  TypePointer m_returnType;
  CompoundPointer m_body;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_NODE_FUNCTION_HPP
