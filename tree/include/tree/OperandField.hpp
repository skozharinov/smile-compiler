#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_OPERANDFIELD_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_OPERANDFIELD_HPP

#include <tree/BaseNode.hpp>

#include <memory>

namespace smile::tree {
class Identifier;

class Operand;

class OperandField : public BaseNode {
public:
  OperandField(IdentifierPointer &&, OperandPointer &&, Position);

  const IdentifierPointer &name() const;
  const OperandPointer &operand() const;

  void validate(Context) const override;

private:
  IdentifierPointer m_name;
  OperandPointer m_operand;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_OPERANDFIELD_HPP
