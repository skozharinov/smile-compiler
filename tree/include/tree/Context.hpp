#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_CONTEXT_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_CONTEXT_HPP

#include <tree/identifier/Identifier.hpp>

#include <functional>
#include <memory>
#include <unordered_map>

namespace smile::tree {
class Structure;
class Function;
class Variable;
class Type;
class TypeField;

class Context {
public:
  using TypePointer = std::unique_ptr<Type>;
  using TypePointerReference = std::reference_wrapper<const TypePointer>;
  using TypeFieldReference = std::reference_wrapper<const TypeField>;
  using IdentifierReference = std::reference_wrapper<const Identifier>;
  using StructureReference = std::reference_wrapper<Structure>;
  using FunctionReference = std::reference_wrapper<Function>;
  using VariableReference = std::reference_wrapper<const Variable>;
  template <class T>
  using IdentifierReferenceMap =
      std::unordered_map<IdentifierReference, T, IdentifierHash,
                         std::equal_to<Identifier>>;

  Context();

  bool hasStructure(IdentifierReference) const;
  bool hasFunction(IdentifierReference) const;
  bool hasVariable(IdentifierReference) const;
  bool hasField(IdentifierReference) const;

  StructureReference structure(IdentifierReference) const;
  FunctionReference function(IdentifierReference) const;
  VariableReference variable(IdentifierReference) const;
  TypeFieldReference field(IdentifierReference) const;

  TypePointerReference expected() const;
  void setExpected(TypePointerReference);

  bool isInLoop() const;
  void setInLoop();

  Context operator+(StructureReference) const;
  Context operator+(FunctionReference) const;
  Context operator+(VariableReference) const;
  Context operator+(TypeFieldReference) const;

  Context &operator+=(StructureReference);
  Context &operator+=(FunctionReference);
  Context &operator+=(VariableReference);
  Context &operator+=(TypeFieldReference);

protected:
  IdentifierReferenceMap<StructureReference> &structures() noexcept;
  const IdentifierReferenceMap<StructureReference> &structures() const noexcept;
  IdentifierReferenceMap<FunctionReference> &functions() noexcept;
  const IdentifierReferenceMap<FunctionReference> &functions() const noexcept;
  IdentifierReferenceMap<VariableReference> &variables() noexcept;
  const IdentifierReferenceMap<VariableReference> &variables() const noexcept;
  IdentifierReferenceMap<TypeFieldReference> &fields() noexcept;
  const IdentifierReferenceMap<TypeFieldReference> &fields() const noexcept;

private:
  IdentifierReferenceMap<StructureReference> m_structures;
  IdentifierReferenceMap<FunctionReference> m_functions;
  IdentifierReferenceMap<VariableReference> m_variables;
  IdentifierReferenceMap<TypeFieldReference> m_fields;

  TypePointerReference m_expectedType;
  bool m_inLoop;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_CONTEXT_HPP
