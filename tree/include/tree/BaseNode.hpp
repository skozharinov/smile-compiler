#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_BASENODE_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_BASENODE_HPP

#include <common/Position.hpp>

#include <memory>

namespace smile::tree {
class Context;

class Type;

class Operand;

class Identifier;

class BaseNode {
public:
  using TypePointer = std::unique_ptr<Type>;
  using OperandPointer = std::unique_ptr<Operand>;
  using IdentifierPointer = std::unique_ptr<Identifier>;

  Position position() const;

  virtual void validate(Context) const;

protected:
  explicit BaseNode(Position);
  virtual ~BaseNode() = default;

private:
  const Position m_position;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_BASENODE_HPP
