#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_FUNCTIONTYPE_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_FUNCTIONTYPE_HPP

#include <tree/type/Type.hpp>

#include <list>
#include <memory>

namespace smile::tree {
class FunctionType : public Type {
public:
  FunctionType(TypePointer &&, std::list<TypePointer> &&, bool, Position);

  const TypePointer &returnType() const noexcept;
  const std::list<TypePointer> &arguments() const noexcept;

  bool isSame(const TypePointer &, Context) const noexcept override;

  TypePointer clone() const noexcept override;

  void validate(Context) const override;

private:
  TypePointer m_returnType;
  std::list<TypePointer> m_arguments;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_FUNCTIONTYPE_HPP
