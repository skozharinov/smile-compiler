#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_PRIMITIVETYPE_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_PRIMITIVETYPE_HPP

#include <tree/type/Type.hpp>

namespace smile::tree {
class PrimitiveType : public Type {
public:
  enum class Kind {
    SIGNED_INT_8,
    UNSIGNED_INT_8,
    SIGNED_INT_16,
    UNSIGNED_INT_16,
    SIGNED_INT_32,
    UNSIGNED_INT_32,
    SIGNED_INT_64,
    UNSIGNED_INT_64,
    SIGNED_INT_SIZE,
    UNSIGNED_INT_SIZE,
    SIGNED_INT_LITERAL,
    UNSIGNED_INT_LITERAL,
    FLOAT_32,
    FLOAT_64,
    FLOAT_LITERAL,
    CHARACTER,
    STRING,
    BOOLEAN,
    NULL_POINTER
  };

  enum class Group {
    SIGNED_INT,
    UNSIGNED_INT,
    FLOAT,
    STRING,
    CHARACTER,
    BOOLEAN,
    POINTER
  };

  static Kind makeSigned(Kind) noexcept;
  static Kind makeUnsigned(Kind) noexcept;

  PrimitiveType(Kind, bool, Position);

  Kind kind() const noexcept;
  Group group() const noexcept;
  bool isLiteral() const noexcept;

  bool isSame(const TypePointer &, Context) const noexcept override;
  bool isConvertible(const TypePointer &, Context) const noexcept override;
  int compareWith(const TypePointer &, Context) const noexcept override;
  TypePointer clone() const noexcept override;

private:
  Kind m_kind;
};
} // namespace smile::tree

#endif
