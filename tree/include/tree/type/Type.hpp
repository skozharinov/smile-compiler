#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_TYPE_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_TYPE_HPP

#include <tree/BaseNode.hpp>

#include <memory>

namespace smile::tree {
class Context;

class Type : virtual public BaseNode {
public:
  Type(bool, Position);

  bool isMutable() const noexcept;
  void setMutable(bool) noexcept;

  virtual bool isSame(const TypePointer &, Context) const noexcept;
  virtual bool isConvertible(const TypePointer &, Context) const noexcept;
  virtual int compareWith(const TypePointer &, Context) const noexcept;
  virtual TypePointer iteratingType() const noexcept;
  virtual TypePointer clone() const noexcept;

private:
  bool m_isMutable;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_TYPE_HPP
