#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_RANGETYPE_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_RANGETYPE_HPP

#include <tree/type/Type.hpp>

namespace smile::tree {
class RangeType : public Type {
public:
  RangeType(TypePointer &&, bool, Position);

  const TypePointer &type() const noexcept;

  bool isSame(const TypePointer &, Context) const noexcept override;
  TypePointer iteratingType() const noexcept override;
  TypePointer clone() const noexcept override;

  void validate(Context) const override;

private:
  TypePointer m_type;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_RANGETYPE_HPP
