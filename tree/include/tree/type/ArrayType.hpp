#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_ARRAYTYPE_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_ARRAYTYPE_HPP

#include <tree/type/Type.hpp>

namespace smile::tree {
class Operand;

class ArrayType : public Type {
public:
  ArrayType(TypePointer &&, OperandPointer &&, bool, Position);

  const TypePointer &type() const noexcept;
  const OperandPointer &size() const noexcept;

  bool isSame(const TypePointer &, Context) const noexcept override;
  int compareWith(const TypePointer &, Context) const noexcept override;
  TypePointer iteratingType() const noexcept override;
  TypePointer clone() const noexcept override;

  void validate(Context) const override;

public:
  TypePointer m_type;
  OperandPointer m_size;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_ARRAYTYPE_HPP
