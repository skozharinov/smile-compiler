#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_REFERENCETYPE_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_REFERENCETYPE_HPP

#include <tree/type/Type.hpp>

namespace smile::tree {
class ReferenceType : public Type {
public:
  enum class Kind { REFERENCE, POINTER };

  ReferenceType(Kind, TypePointer &&, bool, bool, Position);

  Kind kind() const noexcept;
  const TypePointer &referenced() const noexcept;
  bool isMutableReference() const noexcept;

  bool isSame(const TypePointer &, Context) const noexcept override;
  bool isConvertible(const TypePointer &, Context) const noexcept override;
  int compareWith(const TypePointer &, Context) const noexcept override;
  TypePointer clone() const noexcept override;

  void validate(Context) const override;

private:
  Kind m_kind;
  TypePointer m_referenced;
  bool m_isMutableReference;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_REFERENCETYPE_HPP
