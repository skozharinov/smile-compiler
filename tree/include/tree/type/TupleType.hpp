#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_TUPLETYPE_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_TUPLETYPE_HPP

#include <tree/type/Type.hpp>

#include <list>

namespace smile::tree {
class TupleType : public Type {
public:
  TupleType(std::list<TypePointer> &&, bool, Position);

  const std::list<TypePointer> &types() const noexcept;

  bool isSame(const TypePointer &, Context) const noexcept override;
  TypePointer clone() const noexcept override;

  void validate(Context) const override;

private:
  std::list<TypePointer> m_types;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_TYPE_TUPLETYPE_HPP
