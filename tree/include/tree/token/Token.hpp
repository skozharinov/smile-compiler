#ifndef SMILECOMPILER_TREE_INCLUDE_TREE_TOKEN_TOKEN_HPP
#define SMILECOMPILER_TREE_INCLUDE_TREE_TOKEN_TOKEN_HPP

#include <tree/BaseNode.hpp>

namespace smile::tree {
class Context;

class Token : virtual public BaseNode {
public:
  explicit Token(Position);

  virtual bool isConstant(Context) const noexcept;
};
} // namespace smile::tree

#endif // SMILECOMPILER_TREE_INCLUDE_TREE_TOKEN_TOKEN_HPP
