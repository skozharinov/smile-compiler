#include <cli/App.hpp>

#include <common/Exception.hpp>
#include <lexis/Analyzer.hpp>
#include <syntax/Analyzer.hpp>
#include <tree/node/Import.hpp>

#include <boost/program_options.hpp>

#include <chrono>
#include <fstream>
#include <iostream>
#include <utility>

using namespace std::literals;

namespace smile::cli {
namespace {
using namespace std::chrono;

milliseconds::rep ticksBetween(steady_clock::time_point firstPoint,
                               steady_clock::time_point secondPoint) {
  return duration_cast<milliseconds>(secondPoint - firstPoint).count();
}
} // namespace

App::App()
    : m_optionsDescription("Allowed options"), m_produceHelpOption(false),
      m_reproduceTokensOption(false), m_dumpLexisOption(false),
      m_printElapsedTimeOption(true) {
  m_optionsDescription.add_options()(PRODUCE_HELP, "produce help message")(
      REPRODUCE_TOKENS,
      "stop after lexical analysis and print all the token to output")(
      DUMP_LEXIS, "stop after lexical analysis and print output of "
                  "lexical analyzer to output")(NO_PRINT_ELAPSED_TIME,
                                                "do not print elapsed time")(
      INPUT_FILE, boost::program_options::value<std::string>(),
      "specify input file (instead of standard input used by default)");
}

int App::operator()(int argc, const char *argv[]) {
  auto programStartPoint = std::chrono::steady_clock::now();

  try {
    parseOptions(argc, argv);
  } catch (boost::program_options::error &e) {
    std::cerr << "smile: "
              << "error: " << e.what() << std::endl;

    return 1;
  }

  if (produceHelpOption()) {
    produceHelp();
    return 0;
  }

  try {
    readInputData();
  } catch (std::exception &e) {
    std::cerr << "smile: "
              << "error: " << e.what() << std::endl;

    return 2;
  }

  auto readInputDataPoint = std::chrono::steady_clock::now();

  if (printElapsedTimeOption()) {
    std::cout << "Finished reading input data in "
              << ticksBetween(programStartPoint, readInputDataPoint) << " ms"
              << std::endl;
  }

  try {
    parseTokens();
  } catch (Exception &e) {
    static constexpr const char *SEP = ":";
    std::cerr << inputFile() << SEP << e.position().line() << SEP
              << e.position().column() << ": "
              << "lexical error: " << e.what() << std::endl;

    return 3;
  }

  auto finishedLexicalAnalysisPoint = std::chrono::steady_clock::now();

  if (printElapsedTimeOption()) {
    std::cout << "Finished lexical analysis in "
              << ticksBetween(readInputDataPoint, finishedLexicalAnalysisPoint)
              << " ms" << std::endl;
  }

  if (reproduceTokensOption()) {
    reproduceTokens();
    return 0;
  } else if (dumpLexisOption()) {
    dumpLexis();
    return 0;
  }

  try {
    analyzeSyntax();
  } catch (Exception &e) {
    static constexpr const char *SEP = ":";
    std::cerr << inputFile() << SEP << e.position().line() << SEP
              << e.position().column() << ": "
              << "syntax error: " << e.what() << std::endl;

    return 4;
  }

  auto finishedSyntaxAnalysisPoint = std::chrono::steady_clock::now();

  if (printElapsedTimeOption()) {
    std::cout << "Finished syntax analysis in "
              << ticksBetween(finishedLexicalAnalysisPoint,
                              finishedSyntaxAnalysisPoint)
              << " ms" << std::endl;
  }

  try {
    analyzeSemantics();
  } catch (Exception &e) {
    static constexpr const char *SEP = ":";
    std::cerr << inputFile() << SEP << e.position().line() << SEP
              << e.position().column() << ": "
              << "semantic error: " << e.what() << std::endl;

    return 5;
  }

  auto finishedSemanticAnalysisPoint = std::chrono::steady_clock::now();

  if (printElapsedTimeOption()) {
    std::cout << "Finished semantic analysis in "
              << ticksBetween(finishedSyntaxAnalysisPoint,
                              finishedSemanticAnalysisPoint)
              << " ms" << std::endl;
  }

  if (printElapsedTimeOption()) {
    std::cout << "Finished compilation in "
              << ticksBetween(programStartPoint, finishedSemanticAnalysisPoint)
              << " ms" << std::endl;
  }

  return 0;
}

bool App::produceHelpOption() const noexcept { return m_produceHelpOption; }

bool App::reproduceTokensOption() const noexcept {
  return m_reproduceTokensOption;
}

bool App::dumpLexisOption() const noexcept { return m_dumpLexisOption; }

bool App::printElapsedTimeOption() const noexcept {
  return m_printElapsedTimeOption;
}

std::string App::inputFile() const noexcept {
  return m_inputFile.empty() ? STDIN_FILENAME : m_inputFile;
}

void App::produceHelp() const {
  std::cout << optionsDescription() << std::endl;
}

void App::parseOptions(int argc, const char *argv[]) {
  using namespace boost::program_options;

  variables_map variables;
  store(parse_command_line(argc, argv, optionsDescription()), variables);

  setProduceHelpOption(variables.count(PRODUCE_HELP) > 0);
  setReproduceTokensOption(variables.count(REPRODUCE_TOKENS) > 0);
  setDumpLexisOption(variables.count(DUMP_LEXIS) > 0);
  setPrintElapsedTimeOption(variables.count(NO_PRINT_ELAPSED_TIME) == 0);

  if (variables.count(INPUT_FILE) > 0) {
    setInputFile(std::move(variables.at(INPUT_FILE).as<std::string>()));
  }
}

void App::readInputData() {
  std::ostreambuf_iterator<char> inputIterator(inputData());

  if (inputFile() == STDIN_FILENAME) {
    std::istreambuf_iterator<char> begin(std::cin), end;
    std::copy(begin, end, inputIterator);
  } else {
    std::ifstream fileInput(inputFile());

    if (!fileInput.is_open()) {
      throw std::invalid_argument("file '"s + inputFile() +
                                  "' does not exist"s);
    }

    std::istreambuf_iterator<char> begin(fileInput), end;
    std::copy(begin, end, inputIterator);
  }
}

void App::parseTokens() {
  lexis::Analyzer analyzer(inputData());

  while (analyzer.hasNextToken()) {
    addToken(analyzer.nextToken());
  }

  tokens().reverse();
}

void App::reproduceTokens() {
  for (auto &token : tokens()) {
    std::cout << token->codeString() << std::endl;
  }
}

void App::dumpLexis() {
  static constexpr const char *SEP = ":";

  for (auto &token : tokens()) {
    std::cout << token->position().line() << SEP << token->position().column()
              << SEP << token->position().length() << SEP
              << token->classString() << SEP << token->typeString() << SEP
              << token->codeString() << std::endl;
  }
}

void App::analyzeSyntax() {
  syntax::Analyzer analyzer(std::move(tokens()));
  setTree(analyzer.process());
}

void App::analyzeSemantics() {
  tree().extractImports();
  tree().rebuild();
  tree().validate();
}

const boost::program_options::options_description &
App::optionsDescription() const noexcept {
  return m_optionsDescription;
}

std::stringstream &App::inputData() noexcept { return m_inputData; }

std::forward_list<std::unique_ptr<token::Token>> &App::tokens() noexcept {
  return m_tokens;
}

void App::setProduceHelpOption(bool produceHelpOption) noexcept {
  m_produceHelpOption = produceHelpOption;
}

void App::setReproduceTokensOption(bool reproduceTokensOption) noexcept {
  m_reproduceTokensOption = reproduceTokensOption;
}

void App::setDumpLexisOption(bool dumpLexisOption) noexcept {
  m_dumpLexisOption = dumpLexisOption;
}

void App::setPrintElapsedTimeOption(bool printElapsedTimeOption) noexcept {
  m_printElapsedTimeOption = printElapsedTimeOption;
}

void App::setInputFile(std::string &&inputFile) noexcept {
  m_inputFile = inputFile;
}

void App::addToken(std::unique_ptr<token::Token> &&token) noexcept {
  tokens().push_front(std::move(token));
}

const tree::Tree &App::tree() const { return m_tree; }

tree::Tree &&App::tree() { return std::move(m_tree); }

void App::setTree(tree::Tree &&tree) { m_tree = std::move(tree); }
} // namespace smile::cli
