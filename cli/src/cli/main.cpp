#include <cli/App.hpp>

int main(int argc, const char *argv[]) {
  smile::cli::App app;
  return app(argc, argv);
}
