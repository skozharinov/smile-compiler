#ifndef SMILECOMPILER_CLI_INCLUDE_CLI_APP_HPP
#define SMILECOMPILER_CLI_INCLUDE_CLI_APP_HPP

#include <token/Token.hpp>
#include <tree/Tree.hpp>

#include <boost/program_options.hpp>

#include <forward_list>
#include <memory>
#include <sstream>
#include <string>

namespace smile::cli {
class App {
public:
  static constexpr const char *STDIN_FILENAME = "<stdin>";
  static constexpr const char *PRODUCE_HELP = "help";
  static constexpr const char *REPRODUCE_TOKENS = "reproduce-tokens";
  static constexpr const char *DUMP_LEXIS = "dump-lexis";
  static constexpr const char *NO_PRINT_ELAPSED_TIME = "no-print-elapsed-time";
  static constexpr const char *INPUT_FILE = "input-file";

  App();
  int operator()(int, const char *[]);

  bool produceHelpOption() const noexcept;
  bool reproduceTokensOption() const noexcept;
  bool dumpLexisOption() const noexcept;
  bool printElapsedTimeOption() const noexcept;

  std::string inputFile() const noexcept;

protected:
  void produceHelp() const;
  void parseOptions(int, const char *[]);
  void readInputData();
  void parseTokens();
  void reproduceTokens();
  void dumpLexis();
  void analyzeSyntax();
  void analyzeSemantics();

  const boost::program_options::options_description &optionsDescription() const
      noexcept;
  std::stringstream &inputData() noexcept;
  std::forward_list<std::unique_ptr<token::Token>> &tokens() noexcept;

  void setProduceHelpOption(bool) noexcept;
  void setReproduceTokensOption(bool) noexcept;
  void setDumpLexisOption(bool) noexcept;
  void setPrintElapsedTimeOption(bool) noexcept;
  void setInputFile(std::string &&) noexcept;

  void addToken(std::unique_ptr<token::Token> &&) noexcept;

  const tree::Tree &tree() const;
  tree::Tree &&tree();
  void setTree(tree::Tree &&);

private:
  boost::program_options::options_description m_optionsDescription;

  bool m_produceHelpOption;
  bool m_reproduceTokensOption;
  bool m_dumpLexisOption;
  bool m_printElapsedTimeOption;

  std::string m_inputFile;
  std::stringstream m_inputData;

  std::forward_list<std::unique_ptr<token::Token>> m_tokens;
  tree::Tree m_tree;
};
} // namespace smile::cli

#endif // SMILECOMPILER_CLI_INCLUDE_CLI_APP_HPP
