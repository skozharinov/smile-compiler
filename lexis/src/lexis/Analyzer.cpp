#include <lexis/Analyzer.hpp>

#include <common/Exception.hpp>
#include <common/Position.hpp>
#include <common/escapes.hpp>
#include <token/Identifier.hpp>
#include <token/Keyword.hpp>
#include <token/Operation.hpp>
#include <token/Punctuation.hpp>
#include <token/literal/CharacterLiteral.hpp>
#include <token/literal/FloatLiteral.hpp>
#include <token/literal/IntegerLiteral.hpp>
#include <token/literal/StringLiteral.hpp>

#include <cstring>
#include <string>

using namespace std::literals;

namespace smile::lexis {
Analyzer::Analyzer(std::istream &input)
    : m_input(input), m_lineContents(), m_line(0), m_column(0) {}

bool Analyzer::hasNextToken() {
  if (m_line == 0) {
    advanceLine();
  }

  while (skipSpaces() || skipComment())
    ;

  return m_lineContents.length() > 0;
}

Analyzer::TokenPointer Analyzer::nextToken() {
  if (!hasNextToken()) {
    return nullptr;
  }

  TokenPointer token;

  if (nextNumericLiteral(token) || nextCharacterLiteral(token) ||
      nextStringLiteral(token) || nextAlphabeticToken(token) ||
      nextSymbolicToken(token)) {
    return token;
  }

  if (std::isprint(currentCharacter())) {
    throw Exception("unexpected character '"s + currentCharacter() + "'"s,
                    Position(m_line, m_column, 1));
  } else {
    throw Exception("unexpected unprintable character",
                    Position(m_line, m_column, 1));
  }
}

char Analyzer::nextCharacter(std::ptrdiff_t offset) const {
  return m_lineContents.at(m_column - 1 + offset);
}

char Analyzer::currentCharacter() const {
  return m_lineContents.at(m_column - 1);
}

std::string Analyzer::substring(std::size_t length) const {
  return m_lineContents.substr(m_column - 1, length);
}

bool Analyzer::canAdvance(std::ptrdiff_t offset) const {
  return m_column - 1 + offset < m_lineContents.length();
}

void Analyzer::advance(std::ptrdiff_t offset) {
  m_column += offset;

  if (m_column - 1 >= m_lineContents.length()) {
    advanceLine();
  }
}

void Analyzer::advanceLine() {
  m_column = 1;
  char buffer[MAX_LINE_LENGTH];

  while (m_input.getline(buffer, MAX_LINE_LENGTH)) {
    ++m_line;

    if (std::strlen(buffer) > 0) {
      break;
    }
  }

  if (m_input.fail() && !m_input.eof()) {
    throw Exception("failed to read line", Position(m_line + 1, 1, 1));
  }

  m_lineContents = buffer;
}

bool Analyzer::skipSpaces() {
  std::size_t length = 0;

  while (m_lineContents.length() > 0 && std::isspace(nextCharacter(length))) {
    ++length;
  }

  if (length > 0) {
    advance(length);
    return true;
  } else {
    return false;
  }
}

bool Analyzer::skipComment() {
  if (canAdvance(1) && currentCharacter() == '/' && nextCharacter(1) == '/') {
    advanceLine();
    return true;
  } else {
    return false;
  }
}

bool Analyzer::nextNumericLiteral(TokenPointer &token) {
  std::size_t length = 0;
  std::ptrdiff_t dot = -1;
  std::ptrdiff_t exponent = -1;
  std::ptrdiff_t sign = -1;

  while (canAdvance(length)) {
    char character = nextCharacter(length);

    if (std::isdigit(character)) {
    } else if (character == '.' && dot == -1 && length > 0 && exponent == -1) {
      dot = length;
    } else if ((character == 'e' || character == 'E') && exponent == -1 &&
               length > 0 && length != dot + 1) {
      exponent = length;
    } else if ((character == '+' || character == '-') && exponent != -1 &&
               length == exponent + 1) {
      sign = length;
    } else {
      break;
    }

    ++length;
  }

  if (length == 0) {
    return false;
  } else if (sign == length - 1) {
    length -= 2;
  } else if (exponent == length - 1) {
    length -= 1;
  }

  std::string string = substring(length);

  if (dot >= 0 || exponent > 0) {
    token = extractToken<token::FloatLiteral>(length, string);
  } else {
    token = extractToken<token::IntegerLiteral>(length, string);
  }

  return true;
}

bool Analyzer::nextCharacterLiteral(TokenPointer &token) {
  if (currentCharacter() != CHARACTER_QUOTATION || !canAdvance(1)) {
    return false;
  }

  std::size_t length = 1;
  bool escape = false;

  while (true) {
    if (escape) {
      escape = false;
    } else {
      if (nextCharacter(length) == escapes::ESCAPE_SYMBOL) {
        escape = true;
      } else if (nextCharacter(length) == CHARACTER_QUOTATION) {
        ++length;
        break;
      }
    }

    ++length;

    if (!canAdvance(length)) {
      throw Exception("unclosed character literal",
                      Position(m_line, m_column, length));
    }
  }

  token = extractToken<token::CharacterLiteral>(length, substring(length));
  return true;
}

bool Analyzer::nextStringLiteral(TokenPointer &token) {
  if (currentCharacter() != STRING_QUOTATION || !canAdvance(1)) {
    return false;
  }

  std::size_t length = 1;
  bool escape = false;

  while (true) {
    if (escape) {
      escape = false;
    } else {
      if (nextCharacter(length) == escapes::ESCAPE_SYMBOL) {
        escape = true;
      } else if (nextCharacter(length) == STRING_QUOTATION) {
        ++length;
        break;
      }
    }

    ++length;

    if (!canAdvance(length)) {
      throw Exception("unclosed string literal",
                      Position(m_line, m_column, length));
    }
  }

  token = extractToken<token::StringLiteral>(length, substring(length));
  return true;
}

bool Analyzer::nextAlphabeticToken(TokenPointer &token) {
  if (!std::isalpha(currentCharacter()) && currentCharacter() != '_') {
    return false;
  }

  std::size_t length = 1;

  while (canAdvance(length)) {
    if (!std::isalnum(nextCharacter(length)) && nextCharacter(length) != '_') {
      break;
    }

    ++length;
  }

  std::string result = substring(length);

  for (auto &pair : token::Keyword::DATA) {
    auto &keyword = pair.first;
    auto &data = pair.second;

    if (result == std::get<1>(data)) {
      token = extractToken<token::Keyword>(length, keyword);
      return true;
    }
  }

  token = extractToken<token::Identifier>(length, result);
  return true;
}

bool Analyzer::nextSymbolicToken(TokenPointer &token) {
  bool matchedOperation = false;
  bool matchedPunctuation = false;
  std::size_t maxMatched = 0;
  auto maxOperationKind = token::Operation::Kind::ASSIGNMENT;
  auto maxPunctuationKind = token::Punctuation::Kind::SEMICOLON;

  for (auto &pair : token::Operation::DATA) {
    auto &operation = pair.first;
    auto &data = pair.second;

    if (std::get<1>(data).length() > maxMatched &&
        canAdvance(std::get<1>(data).length() - 1) &&
        substring(std::get<1>(data).length()) == std::get<1>(data)) {
      matchedOperation = true;
      matchedPunctuation = false;
      maxMatched = std::get<1>(data).length();
      maxOperationKind = operation;
    }
  }

  for (auto &pair : token::Punctuation::DATA) {
    auto &punctuation = pair.first;
    auto &data = pair.second;

    if (std::get<1>(data).length() > maxMatched &&
        canAdvance(std::get<1>(data).length() - 1) &&
        substring(std::get<1>(data).length()) == std::get<1>(data)) {
      matchedOperation = false;
      matchedPunctuation = true;
      maxMatched = std::get<1>(data).length();
      maxPunctuationKind = punctuation;
    }
  }

  if (matchedOperation) {
    token = extractToken<token::Operation>(maxMatched, maxOperationKind);
  } else if (matchedPunctuation) {
    token = extractToken<token::Punctuation>(maxMatched, maxPunctuationKind);
  }

  return matchedOperation || matchedPunctuation;
}

template <class T, class... Args>
Analyzer::TokenPointer Analyzer::extractToken(std::size_t length,
                                              Args... arguments) {
  TokenPointer result(new T(arguments..., Position(m_line, m_column, length)));
  advance(length);
  return result;
}
} // namespace smile::lexis
