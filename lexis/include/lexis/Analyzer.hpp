#ifndef SMILECOMPILER_LEXIS_INCLUDE_LEXIS_ANALYZER_HPP
#define SMILECOMPILER_LEXIS_INCLUDE_LEXIS_ANALYZER_HPP

#include <token/Token.hpp>

#include <istream>
#include <memory>

namespace smile::lexis {
class Analyzer {
public:
  using TokenPointer = std::unique_ptr<token::Token>;

  static constexpr const std::size_t MAX_LINE_LENGTH = 16384;
  static constexpr const char CHARACTER_QUOTATION = '\'';
  static constexpr const char STRING_QUOTATION = '\"';

  explicit Analyzer(std::istream &);

  bool hasNextToken();
  TokenPointer nextToken();

protected:
  char nextCharacter(std::ptrdiff_t = 1) const;
  char currentCharacter() const;

  std::string substring(std::size_t) const;

  bool canAdvance(std::ptrdiff_t = 1) const;
  void advance(std::ptrdiff_t = 1);
  void advanceLine();

  bool skipSpaces();
  bool skipComment();

  bool nextNumericLiteral(TokenPointer &);
  bool nextCharacterLiteral(TokenPointer &);
  bool nextStringLiteral(TokenPointer &);
  bool nextAlphabeticToken(TokenPointer &);
  bool nextSymbolicToken(TokenPointer &);

  template <class T, class... Args>
  TokenPointer extractToken(std::size_t, Args...);

private:
  std::istream &m_input;
  std::string m_lineContents;
  std::size_t m_line;
  std::size_t m_column;
};
} // namespace smile::lexis

#endif // SMILECOMPILER_LEXIS_INCLUDE_LEXIS_ANALYZER_HPP
