#include <syntax/Analyzer.hpp>

#include <common/Exception.hpp>
#include <token/Identifier.hpp>
#include <token/Keyword.hpp>
#include <token/Operation.hpp>
#include <token/Punctuation.hpp>
#include <token/literal/CharacterLiteral.hpp>
#include <token/literal/FloatLiteral.hpp>
#include <token/literal/IntegerLiteral.hpp>
#include <token/literal/StringLiteral.hpp>
#include <tree/OperandField.hpp>
#include <tree/Tree.hpp>
#include <tree/TypeField.hpp>
#include <tree/identifier/OperandIdentifier.hpp>
#include <tree/identifier/TypeIdentifier.hpp>
#include <tree/literal/BooleanLiteral.hpp>
#include <tree/literal/CharacterLiteral.hpp>
#include <tree/literal/FloatLiteral.hpp>
#include <tree/literal/IntegerLiteral.hpp>
#include <tree/literal/Literal.hpp>
#include <tree/literal/NullPointer.hpp>
#include <tree/literal/StringLiteral.hpp>
#include <tree/node/Function.hpp>
#include <tree/node/Implementation.hpp>
#include <tree/node/Import.hpp>
#include <tree/node/Structure.hpp>
#include <tree/operand/ArrayExpression.hpp>
#include <tree/operand/OperationExpression.hpp>
#include <tree/operand/StructureExpression.hpp>
#include <tree/operand/TupleExpression.hpp>
#include <tree/operation/BinaryOperation.hpp>
#include <tree/operation/Call.hpp>
#include <tree/operation/Cast.hpp>
#include <tree/operation/Operation.hpp>
#include <tree/operation/StructureAccess.hpp>
#include <tree/operation/TupleAccess.hpp>
#include <tree/operation/UnaryOperation.hpp>
#include <tree/statement/Compound.hpp>
#include <tree/statement/Conditional.hpp>
#include <tree/statement/ConditionalLoop.hpp>
#include <tree/statement/Io.hpp>
#include <tree/statement/LoopBreaker.hpp>
#include <tree/statement/RangeLoop.hpp>
#include <tree/statement/ReturnStatement.hpp>
#include <tree/statement/Selection.hpp>
#include <tree/statement/Statement.hpp>
#include <tree/statement/UnconditionalLoop.hpp>
#include <tree/statement/Variable.hpp>
#include <tree/type/ArrayType.hpp>
#include <tree/type/PrimitiveType.hpp>
#include <tree/type/ReferenceType.hpp>
#include <tree/type/TupleType.hpp>

using namespace std::literals;
using namespace smile::token;

namespace smile::syntax {
Analyzer::Analyzer(std::forward_list<TokenPointer> &&tokens)
    : m_tokens(std::move(tokens)) {
  m_tokensIterator = m_tokens.begin();
}

tree::Tree Analyzer::process() {
  nextToken();
  return program();
}

const Analyzer::TokenPointer &Analyzer::token() const { return m_currentToken; }

void Analyzer::nextToken() {
  if (reachedEnd()) {
    m_currentPosition = Position(currentPosition().lineEnd(),
                                 currentPosition().columnEnd() + 1, 1);
  } else {
    m_currentToken = std::move(*(m_tokensIterator++));
    m_currentPosition = m_currentToken->position();
  }
}

bool Analyzer::reachedEnd() const { return m_tokensIterator == m_tokens.end(); }

void Analyzer::saveStartPosition() { m_savedPosition.push(currentPosition()); }

Position Analyzer::currentPosition() const { return m_currentPosition; }

Position Analyzer::position() {
  auto result = currentPosition() - m_savedPosition.top();
  m_savedPosition.pop();
  return result;
}

Exception Analyzer::fail(const std::string &failToken) const {
  return Exception("expected '"s + failToken + "', got '"s +
                       token()->codeString() + "'"s,
                   token()->position());
}

tree::Tree Analyzer::program() {
  tree::Tree tree;

  while (!reachedEnd()) {
    if (test<Keyword>(Keyword::FUNCTION)) {
      tree += function();
    } else if (test<Keyword>(Keyword::STRUCTURE)) {
      tree += structure();
    } else if (test<Keyword>(Keyword::IMPLEMENTATION)) {
      tree += implementation();
    } else if (test<Keyword>(Keyword::IMPORT)) {
      tree += import();
    } else {
      throw fail("top-level statement");
    }
  }

  return std::move(tree);
}

std::unique_ptr<tree::Identifier> Analyzer::extractIdentifier() {
  saveStartPosition();

  auto *identifier = dynamic_cast<Identifier *>(token().get());

  if (identifier != nullptr) {
    auto result = identifier->codeString();
    nextToken();
    return std::make_unique<tree::Identifier>(std::move(result), position());
  } else {
    throw fail("identifier");
  }
}

std::unique_ptr<tree::TypeIdentifier>
Analyzer::toTypeIdentifier(std::unique_ptr<tree::Identifier> &&identifier) {
  return std::make_unique<tree::TypeIdentifier>(
      std::move(identifier->contents()), true, identifier->position());
}

std::unique_ptr<tree::OperandIdentifier>
Analyzer::toOperandIdentifier(std::unique_ptr<tree::Identifier> &&identifier) {
  return std::make_unique<tree::OperandIdentifier>(
      std::move(identifier->contents()), identifier->position());
}

std::unique_ptr<tree::IntegerLiteral> Analyzer::extractIntLiteral() {
  saveStartPosition();

  auto *integer = dynamic_cast<IntegerLiteral *>(token().get());

  if (integer != nullptr) {
    auto result = integer->toUnsigned64();
    nextToken();
    return std::make_unique<tree::IntegerLiteral>(result, position());
  } else {
    throw fail("integer literal");
  }
}

std::unique_ptr<tree::FloatLiteral> Analyzer::extractFloatLiteral() {
  saveStartPosition();

  auto *floatLiteral = dynamic_cast<FloatLiteral *>(token().get());

  if (floatLiteral != nullptr) {
    auto result = floatLiteral->toFloat64();
    nextToken();
    return std::make_unique<tree::FloatLiteral>(result, position());
  } else if (testAndGo<Keyword>(Keyword::FLOAT_INF)) {
    return std::make_unique<tree::FloatLiteral>(
        std::numeric_limits<double>::infinity(), position());
  } else if (testAndGo<Keyword>(Keyword::FLOAT_NAN)) {
    return std::make_unique<tree::FloatLiteral>(
        std::numeric_limits<double>::quiet_NaN(), position());
  } else {
    throw fail("float literal");
  }
}

std::unique_ptr<tree::BooleanLiteral> Analyzer::extractBooleanLiteral() {
  saveStartPosition();

  if (testAndGo<Keyword>(Keyword::BOOLEAN_TRUE)) {
    return std::make_unique<tree::BooleanLiteral>(true, position());
  } else if (testAndGo<Keyword>(Keyword::BOOLEAN_FALSE)) {
    return std::make_unique<tree::BooleanLiteral>(false, position());
  } else {
    throw fail("boolean literal");
  }
}

std::unique_ptr<tree::CharacterLiteral> Analyzer::extractCharacterLiteral() {
  saveStartPosition();

  auto *character = dynamic_cast<CharacterLiteral *>(token().get());

  if (character != nullptr) {
    auto result = character->toCharacter();
    nextToken();
    return std::make_unique<tree::CharacterLiteral>(result, position());
  } else {
    throw fail("character literal");
  }
}

std::unique_ptr<tree::StringLiteral> Analyzer::extractStringLiteral() {
  saveStartPosition();

  auto *string = dynamic_cast<StringLiteral *>(token().get());

  if (string != nullptr) {
    auto result = string->toString();
    nextToken();
    return std::make_unique<tree::StringLiteral>(std::move(result), position());
  } else {
    throw fail("string literal");
  }
}

std::unique_ptr<tree::Operand> Analyzer::arrayExpression() {
  saveStartPosition();
  match<Punctuation>(Punctuation::LEFT_SQUARE_BRACKET);
  auto list = expressionList();
  match<Punctuation>(Punctuation::RIGHT_SQUARE_BRACKET);
  return std::make_unique<tree::ArrayExpression>(std::move(list), position());
}

std::unique_ptr<tree::ArrayType> Analyzer::arrayType() {
  saveStartPosition();
  match<Punctuation>(Punctuation::LEFT_SQUARE_BRACKET);
  auto arrayType = type();
  match<Punctuation>(Punctuation::SEMICOLON);
  auto size = expression();
  match<Punctuation>(Punctuation::RIGHT_SQUARE_BRACKET);
  return std::make_unique<tree::ArrayType>(std::move(arrayType),
                                           std::move(size), true, position());
}

std::unique_ptr<tree::BinaryOperation> Analyzer::binaryOperation() {
  saveStartPosition();

  auto kind = tree::BinaryOperation::Kind::ADDITION;
  bool assignment = false;

  if (testAndGo<Operation>(Operation::ADDITION)) {
    kind = tree::BinaryOperation::Kind::ADDITION;
  } else if (testAndGo<Operation>(Operation::SUBTRACTION)) {
    kind = tree::BinaryOperation::Kind::SUBTRACTION;
  } else if (testAndGo<Operation>(Operation::MULTIPLICATION)) {
    kind = tree::BinaryOperation::Kind::MULTIPLICATION;
  } else if (testAndGo<Operation>(Operation::DIVISION)) {
    kind = tree::BinaryOperation::Kind::DIVISION;
  } else if (testAndGo<Operation>(Operation::REMAINDER)) {
    kind = tree::BinaryOperation::Kind::REMAINDER;
  } else if (testAndGo<Operation>(Operation::CONJUNCTION)) {
    kind = tree::BinaryOperation::Kind::CONJUNCTION;
  } else if (testAndGo<Operation>(Operation::DISJUNCTION)) {
    kind = tree::BinaryOperation::Kind::DISJUNCTION;
  } else if (testAndGo<Operation>(Operation::XOR)) {
    kind = tree::BinaryOperation::Kind::XOR;
  } else if (testAndGo<Operation>(Operation::LEFT_SHIFT)) {
    kind = tree::BinaryOperation::Kind::LEFT_SHIFT;
  } else if (testAndGo<Operation>(Operation::RIGHT_SHIFT)) {
    kind = tree::BinaryOperation::Kind::RIGHT_SHIFT;
  } else if (testAndGo<Operation>(Operation::EQUALITY)) {
    kind = tree::BinaryOperation::Kind::EQUALITY;
  } else if (testAndGo<Operation>(Operation::INEQUALITY)) {
    kind = tree::BinaryOperation::Kind::INEQUALITY;
  } else if (testAndGo<Operation>(Operation::GREATER)) {
    kind = tree::BinaryOperation::Kind::GREATER;
  } else if (testAndGo<Operation>(Operation::GREATER_OR_EQUALS)) {
    kind = tree::BinaryOperation::Kind::GREATER_OR_EQUALS;
  } else if (testAndGo<Operation>(Operation::LESS)) {
    kind = tree::BinaryOperation::Kind::LESS;
  } else if (testAndGo<Operation>(Operation::LESS_OR_EQUALS)) {
    kind = tree::BinaryOperation::Kind::LESS_OR_EQUALS;
  } else if (testAndGo<Operation>(Operation::LAZY_CONJUNCTION)) {
    kind = tree::BinaryOperation::Kind::LAZY_CONJUNCTION;
  } else if (testAndGo<Operation>(Operation::LAZY_DISJUNCTION)) {
    kind = tree::BinaryOperation::Kind::LAZY_DISJUNCTION;
  } else if (testAndGo<Keyword>(Keyword::RANGE_OPERATOR)) {
    kind = tree::BinaryOperation::Kind::RANGE;
  } else if (testAndGo<Operation>(Operation::ASSIGNMENT)) {
    kind = tree::BinaryOperation::Kind::NOOP;
    assignment = true;
  } else if (testAndGo<Operation>(Operation::ADDITION_ASSIGNMENT)) {
    kind = tree::BinaryOperation::Kind::ADDITION;
    assignment = true;
  } else if (testAndGo<Operation>(Operation::SUBTRACTION_ASSIGNMENT)) {
    kind = tree::BinaryOperation::Kind::SUBTRACTION;
    assignment = true;
  } else if (testAndGo<Operation>(Operation::MULTIPLICATION_ASSIGNMENT)) {
    kind = tree::BinaryOperation::Kind::MULTIPLICATION;
    assignment = true;
  } else if (testAndGo<Operation>(Operation::DIVISION_ASSIGNMENT)) {
    kind = tree::BinaryOperation::Kind::DIVISION;
    assignment = true;
  } else if (testAndGo<Operation>(Operation::REMAINDER_ASSIGNMENT)) {
    kind = tree::BinaryOperation::Kind::REMAINDER;
    assignment = true;
  } else if (testAndGo<Operation>(Operation::CONJUNCTION_ASSIGNMENT)) {
    kind = tree::BinaryOperation::Kind::CONJUNCTION;
    assignment = true;
  } else if (testAndGo<Operation>(Operation::DISJUNCTION_ASSIGNMENT)) {
    kind = tree::BinaryOperation::Kind::DISJUNCTION;
    assignment = true;
  } else if (testAndGo<Operation>(Operation::XOR_ASSIGNMENT)) {
    kind = tree::BinaryOperation::Kind::XOR;
    assignment = true;
  } else if (testAndGo<Operation>(Operation::LEFT_SHIFT_ASSIGNMENT)) {
    kind = tree::BinaryOperation::Kind::LEFT_SHIFT;
    assignment = true;
  } else if (testAndGo<Operation>(Operation::RIGHT_SHIFT_ASSIGNMENT)) {
    kind = tree::BinaryOperation::Kind::RIGHT_SHIFT;
    assignment = true;
  } else {
    throw fail("assignment operation");
  }

  return std::make_unique<tree::BinaryOperation>(kind, assignment, position());
}

std::unique_ptr<tree::Call> Analyzer::call() {
  saveStartPosition();
  match<Punctuation>(Punctuation::LEFT_ROUND_BRACKET);
  auto list = expressionList();
  match<Punctuation>(Punctuation::RIGHT_ROUND_BRACKET);
  return std::make_unique<tree::Call>(std::move(list), position());
}

std::unique_ptr<tree::Cast> Analyzer::cast() {
  saveStartPosition();
  match<Keyword>(Keyword::CAST_OPERATOR);
  auto newType = type();
  return std::make_unique<tree::Cast>(std::move(newType), position());
}

std::unique_ptr<tree::Compound> Analyzer::compound() {
  saveStartPosition();
  match<Punctuation>(Punctuation::LEFT_CURLY_BRACKET);
  auto list = statementList();
  match<Punctuation>(Punctuation::RIGHT_CURLY_BRACKET);
  return std::make_unique<tree::Compound>(std::move(list), position());
}

std::unique_ptr<tree::Conditional> Analyzer::conditional() {
  saveStartPosition();
  match<Keyword>(Keyword::CONDITIONAL_MAIN);
  auto condition = expression(false);
  auto body = compound();
  std::unique_ptr<tree::Conditional> alternative = nullptr;

  if (testAndGo<Keyword>(Keyword::CONDITIONAL_ALTERNATIVE)) {

    if (test<Keyword>(Keyword::CONDITIONAL_MAIN)) {
      alternative = conditional();
    } else if (test<Punctuation>(Punctuation::LEFT_CURLY_BRACKET)) {
      auto alternativeBody = compound();
      alternative = std::make_unique<tree::Conditional>(
          nullptr, std::move(alternativeBody), nullptr,
          alternativeBody->position());
    } else {
      throw fail("alternative");
    }
  }

  return std::make_unique<tree::Conditional>(
      std::move(condition), std::move(body), std::move(alternative),
      position());
}

std::unique_ptr<tree::ConditionalLoop> Analyzer::conditionalLoop() {
  saveStartPosition();
  match<Keyword>(Keyword::CONDITIONAL_LOOP);
  auto condition = expression(false);
  auto body = compound();
  return std::make_unique<tree::ConditionalLoop>(std::move(condition),
                                                 std::move(body), position());
}

std::list<std::unique_ptr<tree::Token>>
Analyzer::expressionHelper(bool allowStructureExpressions) {
  std::list<std::unique_ptr<tree::Token>> result;

  if (test<Identifier>()) {
    if (allowStructureExpressions) {
      result.push_back(structureExpression());
    } else {
      result.push_back(toOperandIdentifier(extractIdentifier()));
    }
  } else if (test<Keyword>(Keyword::BOOLEAN_TRUE) ||
             test<Keyword>(Keyword::BOOLEAN_FALSE) ||
             test<Keyword>(Keyword::FLOAT_NAN) ||
             test<Keyword>(Keyword::FLOAT_INF) ||
             test<Keyword>(Keyword::NULL_POINTER) || test<FloatLiteral>() ||
             test<IntegerLiteral>() || test<CharacterLiteral>() ||
             test<StringLiteral>()) {
    result.push_back(literal());
  } else if (test<Punctuation>(Punctuation::LEFT_ROUND_BRACKET)) {
    result.push_back(tupleExpression());
  } else if (test<Punctuation>(Punctuation::LEFT_SQUARE_BRACKET)) {
    result.push_back(arrayExpression());
  } else if (test<Operation>(Operation::MULTIPLICATION) ||
             test<Operation>(Operation::CONJUNCTION) ||
             test<Operation>(Operation::ADDITION) ||
             test<Operation>(Operation::NEGATION) ||
             test<Operation>(Operation::SUBTRACTION)) {
    result.push_back(unaryOperation());
    result.splice(result.end(), expressionHelper(allowStructureExpressions));
  } else {
    throw fail("expression");
  }

  auto list = expressionPrime(allowStructureExpressions);
  result.splice(result.end(), list);

  return std::move(result);
}

std::unique_ptr<tree::Operand>
Analyzer::expression(bool allowStructureExpressions) {
  saveStartPosition();
  auto result = expressionHelper(allowStructureExpressions);

  if (result.size() == 1) {
    return std::unique_ptr<tree::Operand>(
        dynamic_cast<tree::Operand *>(result.front().release()));
  } else {
    return std::make_unique<tree::OperationExpression>(std::move(result),
                                                       position());
  }
}

std::list<std::unique_ptr<tree::Operand>> Analyzer::expressionList() {
  std::list<std::unique_ptr<tree::Operand>> result;

  while (test<Operation>(Operation::MULTIPLICATION) ||
         test<Operation>(Operation::CONJUNCTION) ||
         test<Operation>(Operation::ADDITION) ||
         test<Operation>(Operation::NEGATION) ||
         test<Keyword>(Keyword::BOOLEAN_FALSE) ||
         test<Keyword>(Keyword::BOOLEAN_TRUE) || test<CharacterLiteral>() ||
         test<Keyword>(Keyword::FLOAT_INF) ||
         test<Keyword>(Keyword::NULL_POINTER) || test<FloatLiteral>() ||
         test<Keyword>(Keyword::FLOAT_NAN) || test<Identifier>() ||
         test<IntegerLiteral>() ||
         test<Punctuation>(Punctuation::LEFT_ROUND_BRACKET) ||
         test<Punctuation>(Punctuation::LEFT_SQUARE_BRACKET) ||
         test<StringLiteral>() || test<Operation>(Operation::SUBTRACTION)) {
    result.push_back(expression());

    if (testAndGo<Punctuation>(Punctuation::COMMA)) {
    } else {
      break;
    }
  }

  return result;
}

std::list<std::unique_ptr<tree::Token>>
Analyzer::expressionPrime(bool allowStructureExpressions) {
  std::list<std::unique_ptr<tree::Token>> result;

  bool needRecursion = false;

  if (test<Operation>(Operation::ADDITION) ||
      test<Operation>(Operation::SUBTRACTION) ||
      test<Operation>(Operation::MULTIPLICATION) ||
      test<Operation>(Operation::DIVISION) ||
      test<Operation>(Operation::REMAINDER) ||
      test<Operation>(Operation::DISJUNCTION) ||
      test<Operation>(Operation::DISJUNCTION) ||
      test<Operation>(Operation::XOR) ||
      test<Operation>(Operation::LEFT_SHIFT) ||
      test<Operation>(Operation::RIGHT_SHIFT) ||
      test<Operation>(Operation::EQUALITY) ||
      test<Operation>(Operation::INEQUALITY) ||
      test<Operation>(Operation::GREATER) ||
      test<Operation>(Operation::GREATER_OR_EQUALS) ||
      test<Operation>(Operation::LESS) ||
      test<Operation>(Operation::LESS_OR_EQUALS) ||
      test<Operation>(Operation::LAZY_CONJUNCTION) ||
      test<Operation>(Operation::LAZY_DISJUNCTION) ||
      test<Keyword>(Keyword::RANGE_OPERATOR) ||
      test<Operation>(Operation::ASSIGNMENT) ||
      test<Operation>(Operation::ADDITION_ASSIGNMENT) ||
      test<Operation>(Operation::SUBTRACTION_ASSIGNMENT) ||
      test<Operation>(Operation::MULTIPLICATION_ASSIGNMENT) ||
      test<Operation>(Operation::DIVISION_ASSIGNMENT) ||
      test<Operation>(Operation::REMAINDER_ASSIGNMENT) ||
      test<Operation>(Operation::CONJUNCTION_ASSIGNMENT) ||
      test<Operation>(Operation::DISJUNCTION_ASSIGNMENT) ||
      test<Operation>(Operation::XOR_ASSIGNMENT) ||
      test<Operation>(Operation::LEFT_SHIFT_ASSIGNMENT) ||
      test<Operation>(Operation::RIGHT_SHIFT_ASSIGNMENT)) {
    result.push_back(binaryOperation());
    result.splice(result.end(), expressionHelper(allowStructureExpressions));
    needRecursion = true;
  } else if (test<Keyword>(Keyword::CAST_OPERATOR)) {
    result.push_back(cast());
    needRecursion = true;
  } else if (test<Punctuation>(Punctuation::LEFT_ROUND_BRACKET)) {
    result.push_back(call());
    needRecursion = true;
  } else if (testAndGo<Punctuation>(Punctuation::LEFT_SQUARE_BRACKET)) {
    saveStartPosition();
    auto index = expression();
    match<Punctuation>(Punctuation::RIGHT_SQUARE_BRACKET);
    auto access = std::make_unique<tree::BinaryOperation>(
        tree::BinaryOperation::Kind::ARRAY_ACCESS, false, position());
    result.push_back(std::move(access));
    result.push_back(std::move(index));
    needRecursion = true;
  } else if (test<Operation>(Operation::MEMBER_ACCESS)) {
    saveStartPosition();
    nextToken();

    if (test<IntegerLiteral>()) {
      auto field = extractIntLiteral();
      result.push_back(
          std::make_unique<tree::TupleAccess>(std::move(field), position()));
    } else if (test<Identifier>()) {
      auto field = extractIdentifier();
      result.push_back(std::make_unique<tree::StructureAccess>(std::move(field),
                                                               position()));
    } else {
      throw fail("member id");
    }

    needRecursion = true;
  }

  if (needRecursion) {
    result.splice(result.end(), expressionPrime(allowStructureExpressions));
  }

  return result;
}

std::unique_ptr<tree::Function> Analyzer::function() {
  saveStartPosition();
  match<Keyword>(Keyword::FUNCTION);
  auto identifier = extractIdentifier();
  match<Punctuation>(Punctuation::LEFT_ROUND_BRACKET);
  auto fields = typeFieldList();
  match<Punctuation>(Punctuation::RIGHT_ROUND_BRACKET);
  std::unique_ptr<tree::Type> returnType = nullptr;

  if (testAndGo<Punctuation>(Punctuation::SELECTION)) {
    returnType = type();
  }

  auto body = compound();
  return std::make_unique<tree::Function>(
      std::move(identifier), std::move(fields), std::move(returnType),
      std::move(body), position());
}

std::list<std::unique_ptr<tree::Function>> Analyzer::functionList() {
  std::list<std::unique_ptr<tree::Function>> result;

  while (test<Keyword>(Keyword::FUNCTION)) {
    result.push_back(function());
  }

  return result;
}

std::unique_ptr<tree::Implementation> Analyzer::implementation() {
  saveStartPosition();
  match<Keyword>(Keyword::IMPLEMENTATION);
  auto identifier = extractIdentifier();
  match<Punctuation>(Punctuation::LEFT_CURLY_BRACKET);
  auto functions = functionList();
  match<Punctuation>(Punctuation::RIGHT_CURLY_BRACKET);
  return std::make_unique<tree::Implementation>(
      toTypeIdentifier(std::move(identifier)), std::move(functions),
      position());
}

std::unique_ptr<tree::Import> Analyzer::import() {
  saveStartPosition();
  match<Keyword>(Keyword::IMPORT);
  auto path = extractIdentifier();
  match<Punctuation>(Punctuation::SEMICOLON);
  return std::make_unique<tree::Import>(std::move(path), position());
}

std::unique_ptr<tree::Statement> Analyzer::incompleteStatement() {
  if (test<Operation>(Operation::MULTIPLICATION) ||
      test<Operation>(Operation::CONJUNCTION) ||
      test<Operation>(Operation::ADDITION) ||
      test<Operation>(Operation::NEGATION) ||
      test<Keyword>(Keyword::BOOLEAN_FALSE) ||
      test<Keyword>(Keyword::BOOLEAN_TRUE) || test<CharacterLiteral>() ||
      test<Keyword>(Keyword::FLOAT_INF) ||
      test<Keyword>(Keyword::NULL_POINTER) || test<FloatLiteral>() ||
      test<Keyword>(Keyword::FLOAT_NAN) || test<Identifier>() ||
      test<IntegerLiteral>() ||
      test<Punctuation>(Punctuation::LEFT_ROUND_BRACKET) ||
      test<Punctuation>(Punctuation::LEFT_SQUARE_BRACKET) ||
      test<StringLiteral>() || test<Operation>(Operation::SUBTRACTION)) {
    return expression();
  } else if (test<Keyword>(Keyword::RETURN)) {
    return returnStatement();
  } else if (test<Keyword>(Keyword::CONTINUE) ||
             test<Keyword>(Keyword::BREAK)) {
    return loopBreaker();
  } else if (test<Keyword>(Keyword::MUTABLE_VARIABLE) ||
             test<Keyword>(Keyword::IMMUTABLE_VARIABLE)) {
    return variable();
  } else if (test<Keyword>(Keyword::READ) || test<Keyword>(Keyword::WRITE)) {
    return io();
  } else {
    throw fail("statement");
  }
}

std::unique_ptr<tree::Io> Analyzer::io() {
  saveStartPosition();
  auto kind = tree::Io::Kind::READ;

  if (testAndGo<Keyword>(Keyword::READ)) {
  } else if (testAndGo<Keyword>(Keyword::WRITE)) {
    kind = tree::Io::Kind::WRITE;
  } else {
    throw fail("IO statement");
  }

  auto operand = expression();
  return std::make_unique<tree::Io>(kind, std::move(operand), position());
}

std::unique_ptr<tree::Literal> Analyzer::literal() {
  if (test<IntegerLiteral>()) {
    return extractIntLiteral();
  } else if (test<FloatLiteral>() || test<Keyword>(Keyword::FLOAT_NAN) ||
             test<Keyword>(Keyword::FLOAT_INF)) {
    return extractFloatLiteral();
  } else if (test<CharacterLiteral>()) {
    return extractCharacterLiteral();
  } else if (test<StringLiteral>()) {
    return extractStringLiteral();
  } else if (test<Keyword>(Keyword::BOOLEAN_TRUE) ||
             test<Keyword>(Keyword::BOOLEAN_FALSE)) {
    return extractBooleanLiteral();
  } else if (test<Keyword>(Keyword::NULL_POINTER)) {
    saveStartPosition();
    nextToken();
    return std::make_unique<tree::NullPointer>(position());
  } else {
    throw fail("literal");
  }
}

std::unique_ptr<tree::LoopBreaker> Analyzer::loopBreaker() {
  saveStartPosition();
  auto kind = tree::LoopBreaker::Kind::CONTINUE;

  if (testAndGo<Keyword>(Keyword::BREAK)) {
    kind = tree::LoopBreaker::Kind::BREAK;
  } else if (testAndGo<Keyword>(Keyword::CONTINUE)) {
    kind = tree::LoopBreaker::Kind::CONTINUE;
  } else {
    throw fail("loop breaker");
  }

  return std::make_unique<tree::LoopBreaker>(kind, position());
}

std::unique_ptr<tree::OperandField> Analyzer::operandField() {
  saveStartPosition();
  auto name = extractIdentifier();
  match<Punctuation>(Punctuation::COLON);
  auto value = expression();
  return std::make_unique<tree::OperandField>(std::move(name), std::move(value),
                                              position());
}

std::list<std::unique_ptr<tree::OperandField>> Analyzer::operandFieldList() {
  std::list<std::unique_ptr<tree::OperandField>> result;

  while (test<Identifier>()) {
    result.push_back(operandField());

    if (testAndGo<Punctuation>(Punctuation::COMMA)) {
    } else {
      break;
    }
  }

  return result;
}

std::unique_ptr<tree::PrimitiveType> Analyzer::primitiveType() {
  saveStartPosition();

  auto kind = tree::PrimitiveType::Kind::SIGNED_INT_8;

  if (testAndGo<Keyword>(Keyword::SIGNED_INT_8)) {
    kind = tree::PrimitiveType::Kind::SIGNED_INT_8;
  } else if (testAndGo<Keyword>(Keyword::UNSIGNED_INT_8)) {
    kind = tree::PrimitiveType::Kind::UNSIGNED_INT_8;
  } else if (testAndGo<Keyword>(Keyword::SIGNED_INT_16)) {
    kind = tree::PrimitiveType::Kind::SIGNED_INT_16;
  } else if (testAndGo<Keyword>(Keyword::UNSIGNED_INT_16)) {
    kind = tree::PrimitiveType::Kind::UNSIGNED_INT_16;
  } else if (testAndGo<Keyword>(Keyword::SIGNED_INT_32)) {
    kind = tree::PrimitiveType::Kind::SIGNED_INT_32;
  } else if (testAndGo<Keyword>(Keyword::UNSIGNED_INT_32)) {
    kind = tree::PrimitiveType::Kind::UNSIGNED_INT_32;
  } else if (testAndGo<Keyword>(Keyword::SIGNED_INT_64)) {
    kind = tree::PrimitiveType::Kind::SIGNED_INT_64;
  } else if (testAndGo<Keyword>(Keyword::UNSIGNED_INT_64)) {
    kind = tree::PrimitiveType::Kind::UNSIGNED_INT_64;
  } else if (testAndGo<Keyword>(Keyword::SIGNED_INT_SIZE)) {
    kind = tree::PrimitiveType::Kind::SIGNED_INT_SIZE;
  } else if (testAndGo<Keyword>(Keyword::UNSIGNED_INT_SIZE)) {
    kind = tree::PrimitiveType::Kind::UNSIGNED_INT_SIZE;
  } else if (testAndGo<Keyword>(Keyword::FLOAT_32)) {
    kind = tree::PrimitiveType::Kind::FLOAT_32;
  } else if (testAndGo<Keyword>(Keyword::FLOAT_64)) {
    kind = tree::PrimitiveType::Kind::FLOAT_64;
  } else if (testAndGo<Keyword>(Keyword::CHARACTER)) {
    kind = tree::PrimitiveType::Kind::CHARACTER;
  } else if (testAndGo<Keyword>(Keyword::STRING)) {
    kind = tree::PrimitiveType::Kind::STRING;
  } else if (testAndGo<Keyword>(Keyword::BOOLEAN)) {
    kind = tree::PrimitiveType::Kind::BOOLEAN;
  } else {
    throw fail("primitive type");
  }

  return std::make_unique<tree::PrimitiveType>(kind, true, position());
}

std::unique_ptr<tree::RangeLoop> Analyzer::rangeLoop() {
  saveStartPosition();
  match<Keyword>(Keyword::RANGE_LOOP);
  auto fieldDescription = typeField();
  match<Keyword>(Keyword::RANGE_LOOP_OPERATOR);
  auto range = expression(false);
  auto body = compound();
  return std::make_unique<tree::RangeLoop>(std::move(fieldDescription),
                                           std::move(range), std::move(body),
                                           position());
}

std::unique_ptr<tree::ReturnStatement> Analyzer::returnStatement() {
  saveStartPosition();
  match<Keyword>(Keyword::RETURN);

  std::unique_ptr<tree::Operand> operand = nullptr;

  if (test<Operation>(Operation::MULTIPLICATION) ||
      test<Operation>(Operation::CONJUNCTION) ||
      test<Operation>(Operation::ADDITION) ||
      test<Operation>(Operation::NEGATION) ||
      test<Keyword>(Keyword::BOOLEAN_FALSE) ||
      test<Keyword>(Keyword::BOOLEAN_TRUE) || test<CharacterLiteral>() ||
      test<Keyword>(Keyword::FLOAT_INF) ||
      test<Keyword>(Keyword::NULL_POINTER) || test<FloatLiteral>() ||
      test<Keyword>(Keyword::FLOAT_NAN) || test<Identifier>() ||
      test<IntegerLiteral>() ||
      test<Punctuation>(Punctuation::LEFT_ROUND_BRACKET) ||
      test<Punctuation>(Punctuation::LEFT_SQUARE_BRACKET) ||
      test<StringLiteral>() || test<Operation>(Operation::SUBTRACTION)) {
    operand = expression();
  }

  return std::make_unique<tree::ReturnStatement>(std::move(operand),
                                                 position());
}

std::unique_ptr<tree::Selection> Analyzer::selection() {
  saveStartPosition();
  match<Keyword>(Keyword::SELECTION);
  auto operand = expression(false);
  match<Punctuation>(Punctuation::LEFT_CURLY_BRACKET);
  auto branches = selectionBranchList();
  match<Punctuation>(Punctuation::RIGHT_CURLY_BRACKET);
  return std::make_unique<tree::Selection>(std::move(operand),
                                           std::move(branches), position());
}

std::unique_ptr<tree::SelectionBranch> Analyzer::selectionBranch() {
  saveStartPosition();
  std::unique_ptr<tree::Literal> matchLiteral = nullptr;

  if (test<Keyword>(Keyword::BOOLEAN_TRUE) ||
      test<Keyword>(Keyword::BOOLEAN_FALSE) ||
      test<Keyword>(Keyword::FLOAT_NAN) || test<Keyword>(Keyword::FLOAT_INF) ||
      test<Keyword>(Keyword::NULL_POINTER) || test<FloatLiteral>() ||
      test<IntegerLiteral>() || test<CharacterLiteral>() ||
      test<StringLiteral>()) {
    matchLiteral = literal();
  } else if (testAndGo<Keyword>(Keyword::CONDITIONAL_ALTERNATIVE)) {
  } else {
    throw fail("selection branch");
  }

  match<Punctuation>(Punctuation::SELECTION);

  std::unique_ptr<tree::Statement> statement = nullptr;

  if (test<Operation>(Operation::MULTIPLICATION) ||
      test<Operation>(Operation::CONJUNCTION) ||
      test<Operation>(Operation::ADDITION) ||
      test<Operation>(Operation::NEGATION) ||
      test<Keyword>(Keyword::BOOLEAN_FALSE) ||
      test<Keyword>(Keyword::BOOLEAN_TRUE) || test<CharacterLiteral>() ||
      test<Keyword>(Keyword::FLOAT_INF) ||
      test<Keyword>(Keyword::NULL_POINTER) || test<FloatLiteral>() ||
      test<Keyword>(Keyword::FLOAT_NAN) || test<Identifier>() ||
      test<IntegerLiteral>() ||
      test<Punctuation>(Punctuation::LEFT_ROUND_BRACKET) ||
      test<Punctuation>(Punctuation::LEFT_SQUARE_BRACKET) ||
      test<StringLiteral>() || test<Operation>(Operation::SUBTRACTION) ||
      test<Keyword>(Keyword::RETURN) || test<Keyword>(Keyword::CONTINUE) ||
      test<Keyword>(Keyword::BREAK) ||
      test<Keyword>(Keyword::MUTABLE_VARIABLE) ||
      test<Keyword>(Keyword::IMMUTABLE_VARIABLE) ||
      test<Keyword>(Keyword::READ) || test<Keyword>(Keyword::WRITE)) {
    statement = incompleteStatement();
  } else if (test<Punctuation>(Punctuation::LEFT_CURLY_BRACKET)) {
    statement = compound();
  } else {
    throw fail("selection branch body");
  }

  return std::make_unique<tree::SelectionBranch>(
      std::move(matchLiteral), std::move(statement), position());
}

std::list<std::unique_ptr<tree::SelectionBranch>>
Analyzer::selectionBranchList() {
  std::list<std::unique_ptr<tree::SelectionBranch>> branches;

  while (test<Keyword>(Keyword::BOOLEAN_TRUE) ||
         test<Keyword>(Keyword::BOOLEAN_FALSE) ||
         test<Keyword>(Keyword::FLOAT_NAN) ||
         test<Keyword>(Keyword::FLOAT_INF) ||
         test<Keyword>(Keyword::NULL_POINTER) || test<FloatLiteral>() ||
         test<IntegerLiteral>() || test<CharacterLiteral>() ||
         test<StringLiteral>() ||
         test<Keyword>(Keyword::CONDITIONAL_ALTERNATIVE)) {
    branches.push_back(selectionBranch());

    if (testAndGo<Punctuation>(Punctuation::COMMA)) {
    } else {
      break;
    }
  }

  return branches;
}

std::unique_ptr<tree::Statement> Analyzer::statement() {
  if (test<Operation>(Operation::MULTIPLICATION) ||
      test<Operation>(Operation::CONJUNCTION) ||
      test<Operation>(Operation::ADDITION) ||
      test<Operation>(Operation::NEGATION) ||
      test<Keyword>(Keyword::BOOLEAN_FALSE) ||
      test<Keyword>(Keyword::BOOLEAN_TRUE) || test<CharacterLiteral>() ||
      test<Keyword>(Keyword::FLOAT_INF) ||
      test<Keyword>(Keyword::NULL_POINTER) || test<FloatLiteral>() ||
      test<Keyword>(Keyword::FLOAT_NAN) || test<Identifier>() ||
      test<IntegerLiteral>() ||
      test<Punctuation>(Punctuation::LEFT_ROUND_BRACKET) ||
      test<Punctuation>(Punctuation::LEFT_SQUARE_BRACKET) ||
      test<StringLiteral>() || test<Operation>(Operation::SUBTRACTION) ||
      test<Keyword>(Keyword::RETURN) || test<Keyword>(Keyword::CONTINUE) ||
      test<Keyword>(Keyword::BREAK) ||
      test<Keyword>(Keyword::MUTABLE_VARIABLE) ||
      test<Keyword>(Keyword::IMMUTABLE_VARIABLE) ||
      test<Keyword>(Keyword::READ) || test<Keyword>(Keyword::WRITE)) {
    auto result = incompleteStatement();
    match<Punctuation>(Punctuation::SEMICOLON);
    return result;
  } else if (test<Keyword>(Keyword::CONDITIONAL_MAIN)) {
    return conditional();
  } else if (test<Keyword>(Keyword::SELECTION)) {
    return selection();
  } else if (test<Keyword>(Keyword::UNCONDITIONAL_LOOP)) {
    return unconditionalLoop();
  } else if (test<Keyword>(Keyword::CONDITIONAL_LOOP)) {
    return conditionalLoop();
  } else if (test<Keyword>(Keyword::RANGE_LOOP)) {
    return rangeLoop();
  } else {
    throw fail("statement");
  }
}

std::list<std::unique_ptr<tree::Statement>> Analyzer::statementList() {
  std::list<std::unique_ptr<tree::Statement>> result;

  while (test<Operation>(Operation::MULTIPLICATION) ||
         test<Operation>(Operation::CONJUNCTION) ||
         test<Operation>(Operation::ADDITION) ||
         test<Operation>(Operation::NEGATION) ||
         test<Keyword>(Keyword::BOOLEAN_FALSE) ||
         test<Keyword>(Keyword::BOOLEAN_TRUE) || test<CharacterLiteral>() ||
         test<Keyword>(Keyword::FLOAT_INF) ||
         test<Keyword>(Keyword::NULL_POINTER) || test<FloatLiteral>() ||
         test<Keyword>(Keyword::FLOAT_NAN) || test<Identifier>() ||
         test<IntegerLiteral>() ||
         test<Punctuation>(Punctuation::LEFT_ROUND_BRACKET) ||
         test<Punctuation>(Punctuation::LEFT_SQUARE_BRACKET) ||
         test<StringLiteral>() || test<Operation>(Operation::SUBTRACTION) ||
         test<Keyword>(Keyword::RETURN) || test<Keyword>(Keyword::CONTINUE) ||
         test<Keyword>(Keyword::BREAK) ||
         test<Keyword>(Keyword::MUTABLE_VARIABLE) ||
         test<Keyword>(Keyword::IMMUTABLE_VARIABLE) ||
         test<Keyword>(Keyword::CONDITIONAL_MAIN) ||
         test<Keyword>(Keyword::SELECTION) ||
         test<Keyword>(Keyword::UNCONDITIONAL_LOOP) ||
         test<Keyword>(Keyword::CONDITIONAL_LOOP) ||
         test<Keyword>(Keyword::RANGE_LOOP) || test<Keyword>(Keyword::READ) ||
         test<Keyword>(Keyword::WRITE)) {
    result.push_back(statement());
  }

  return result;
}

std::unique_ptr<tree::Structure> Analyzer::structure() {
  saveStartPosition();
  match<Keyword>(Keyword::STRUCTURE);
  auto identifier = extractIdentifier();
  match<Punctuation>(Punctuation::LEFT_CURLY_BRACKET);
  auto fields = typeFieldList();
  match<Punctuation>(Punctuation::RIGHT_CURLY_BRACKET);
  return std::make_unique<tree::Structure>(
      toTypeIdentifier(std::move(identifier)), std::move(fields), position());
}

std::unique_ptr<tree::Operand> Analyzer::structureExpression() {
  saveStartPosition();

  auto identifier = toTypeIdentifier(extractIdentifier());

  if (test<Punctuation>(Punctuation::LEFT_CURLY_BRACKET)) {
    match<Punctuation>(Punctuation::LEFT_CURLY_BRACKET);
    auto list = operandFieldList();
    match<Punctuation>(Punctuation::RIGHT_CURLY_BRACKET);
    return std::make_unique<tree::StructureExpression>(
        std::move(identifier), std::move(list), position());
  } else {
    position();
    return toOperandIdentifier(std::move(identifier));
  }
}

std::unique_ptr<tree::Type> Analyzer::type() {
  saveStartPosition();
  auto kind = tree::ReferenceType::Kind::REFERENCE;
  auto mutability = false;
  bool reference = false;
  std::unique_ptr<tree::Type> type = nullptr;

  if (testAndGo<Operation>(Operation::MULTIPLICATION)) {
    reference = true;
    kind = tree::ReferenceType::Kind::POINTER;
  } else if (testAndGo<Operation>(Operation::CONJUNCTION)) {
    reference = true;
    kind = tree::ReferenceType::Kind::REFERENCE;
  }

  if (reference && test<Keyword>(Keyword::MUTABLE_VARIABLE)) {
    nextToken();
    mutability = true;
  }

  if (test<Identifier>()) {
    type = toTypeIdentifier(extractIdentifier());
  } else if (test<Keyword>(Keyword::SIGNED_INT_8) ||
             test<Keyword>(Keyword::UNSIGNED_INT_8) ||
             test<Keyword>(Keyword::SIGNED_INT_16) ||
             test<Keyword>(Keyword::UNSIGNED_INT_16) ||
             test<Keyword>(Keyword::SIGNED_INT_32) ||
             test<Keyword>(Keyword::UNSIGNED_INT_32) ||
             test<Keyword>(Keyword::SIGNED_INT_64) ||
             test<Keyword>(Keyword::UNSIGNED_INT_64) ||
             test<Keyword>(Keyword::SIGNED_INT_SIZE) ||
             test<Keyword>(Keyword::UNSIGNED_INT_SIZE) ||
             test<Keyword>(Keyword::FLOAT_32) ||
             test<Keyword>(Keyword::FLOAT_64) ||
             test<Keyword>(Keyword::CHARACTER) ||
             test<Keyword>(Keyword::STRING) ||
             test<Keyword>(Keyword::BOOLEAN)) {
    type = primitiveType();
  } else if (test<Punctuation>(Punctuation::LEFT_ROUND_BRACKET)) {
    type = tupleType();
  } else if (test<Punctuation>(Punctuation::LEFT_SQUARE_BRACKET)) {
    type = arrayType();
  }

  if (reference) {
    return std::make_unique<tree::ReferenceType>(kind, std::move(type),
                                                 mutability, true, position());
  } else {
    position();
    return type;
  }
}

std::list<std::unique_ptr<tree::Type>> Analyzer::typeList() {
  std::list<std::unique_ptr<tree::Type>> result;

  while (test<Identifier>() || test<Keyword>(Keyword::BOOLEAN) ||
         test<Keyword>(Keyword::CHARACTER) ||
         test<Keyword>(Keyword::FLOAT_32) || test<Keyword>(Keyword::FLOAT_64) ||
         test<Keyword>(Keyword::SIGNED_INT_16) ||
         test<Keyword>(Keyword::SIGNED_INT_32) ||
         test<Keyword>(Keyword::SIGNED_INT_64) ||
         test<Keyword>(Keyword::SIGNED_INT_8) ||
         test<Keyword>(Keyword::SIGNED_INT_SIZE) ||
         test<Keyword>(Keyword::STRING) ||
         test<Keyword>(Keyword::UNSIGNED_INT_16) ||
         test<Keyword>(Keyword::UNSIGNED_INT_32) ||
         test<Keyword>(Keyword::UNSIGNED_INT_64) ||
         test<Keyword>(Keyword::UNSIGNED_INT_8) ||
         test<Keyword>(Keyword::UNSIGNED_INT_SIZE) ||
         test<Operation>(Operation::CONJUNCTION) ||
         test<Operation>(Operation::MULTIPLICATION) ||
         test<Punctuation>(Punctuation::LEFT_ROUND_BRACKET) ||
         test<Punctuation>(Punctuation::LEFT_SQUARE_BRACKET)) {
    result.push_back(type());

    if (testAndGo<Punctuation>(Punctuation::COMMA)) {
    } else {
      break;
    }
  }

  return result;
}

std::unique_ptr<tree::TypeField> Analyzer::typeField() {
  saveStartPosition();
  auto identifier = extractIdentifier();
  match<Punctuation>(Punctuation::COLON);
  auto fieldType = type();
  return std::make_unique<tree::TypeField>(std::move(identifier),
                                           std::move(fieldType), position());
}

std::list<std::unique_ptr<tree::TypeField>> Analyzer::typeFieldList() {
  std::list<std::unique_ptr<tree::TypeField>> result;

  while (test<Identifier>()) {
    result.push_back(typeField());

    if (testAndGo<Punctuation>(Punctuation::COMMA)) {
    } else {
      break;
    }
  }

  return result;
}

std::unique_ptr<tree::Operand> Analyzer::tupleExpression() {
  saveStartPosition();
  std::list<std::unique_ptr<tree::Operand>> result;
  bool tuple = false;

  match<Punctuation>(Punctuation::LEFT_ROUND_BRACKET);

  while (test<Operation>(Operation::MULTIPLICATION) ||
         test<Operation>(Operation::CONJUNCTION) ||
         test<Operation>(Operation::ADDITION) ||
         test<Operation>(Operation::NEGATION) ||
         test<Keyword>(Keyword::BOOLEAN_FALSE) ||
         test<Keyword>(Keyword::BOOLEAN_TRUE) || test<CharacterLiteral>() ||
         test<Keyword>(Keyword::FLOAT_INF) ||
         test<Keyword>(Keyword::NULL_POINTER) || test<FloatLiteral>() ||
         test<Keyword>(Keyword::FLOAT_NAN) || test<Identifier>() ||
         test<IntegerLiteral>() ||
         test<Punctuation>(Punctuation::LEFT_ROUND_BRACKET) ||
         test<Punctuation>(Punctuation::LEFT_SQUARE_BRACKET) ||
         test<StringLiteral>() || test<Operation>(Operation::SUBTRACTION)) {
    result.push_back(expression());

    if (testAndGo<Punctuation>(Punctuation::COMMA)) {
      tuple = true;
    } else {
      break;
    }
  }

  match<Punctuation>(Punctuation::RIGHT_ROUND_BRACKET);

  if (tuple || result.empty()) {
    return std::make_unique<tree::TupleExpression>(std::move(result),
                                                   position());
  } else {
    position();
    return std::move(result.front());
  }
}

std::unique_ptr<tree::TupleType> Analyzer::tupleType() {
  saveStartPosition();
  match<Punctuation>(Punctuation::LEFT_ROUND_BRACKET);
  auto list = typeList();
  match<Punctuation>(Punctuation::RIGHT_ROUND_BRACKET);
  return std::make_unique<tree::TupleType>(std::move(list), true, position());
}

std::unique_ptr<tree::UnaryOperation> Analyzer::unaryOperation() {
  saveStartPosition();

  if (testAndGo<Operation>(Operation::CONJUNCTION)) {

    if (testAndGo<Keyword>(Keyword::MUTABLE_VARIABLE)) {
      return std::make_unique<tree::UnaryOperation>(
          tree::UnaryOperation::Kind::MUTABLE_REFERENCE, position());
    } else {
      return std::make_unique<tree::UnaryOperation>(
          tree::UnaryOperation::Kind::REFERENCE, position());
    }
  } else if (testAndGo<Operation>(Operation::MULTIPLICATION)) {
    return std::make_unique<tree::UnaryOperation>(
        tree::UnaryOperation::Kind::DEREFERENCE, position());
  } else if (testAndGo<Operation>(Operation::ADDITION)) {
    return std::make_unique<tree::UnaryOperation>(
        tree::UnaryOperation::Kind::POSITIVE, position());
  } else if (testAndGo<Operation>(Operation::SUBTRACTION)) {
    return std::make_unique<tree::UnaryOperation>(
        tree::UnaryOperation::Kind::NEGATIVE, position());
  } else if (testAndGo<Operation>(Operation::NEGATION)) {
    return std::make_unique<tree::UnaryOperation>(
        tree::UnaryOperation::Kind::NEGATION, position());
  } else {
    throw fail("unary operation");
  }
}

std::unique_ptr<tree::UnconditionalLoop> Analyzer::unconditionalLoop() {
  saveStartPosition();
  match<Keyword>(Keyword::UNCONDITIONAL_LOOP);
  auto body = compound();
  return std::make_unique<tree::UnconditionalLoop>(std::move(body), position());
}

std::unique_ptr<tree::Variable> Analyzer::variable() {
  saveStartPosition();
  bool mutability = false;
  bool constness = false;

  if (testAndGo<Keyword>(Keyword::MUTABLE_VARIABLE)) {
    mutability = true;
  } else if (testAndGo<Keyword>(Keyword::IMMUTABLE_VARIABLE)) {
  } else {
    throw fail("variable declaration");
  }

  auto fieldDescription = typeField();
  fieldDescription->type()->setMutable(mutability);
  match<Operation>(Operation::ASSIGNMENT);
  auto initializer = expression();
  return std::make_unique<tree::Variable>(std::move(fieldDescription),
                                          constness, std::move(initializer),
                                          position());
}
} // namespace smile::syntax
