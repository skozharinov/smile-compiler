#ifndef SMILECOMPILER_SYNTAX_INCLUDE_SYNTAX_ANALYZER_HPP
#define SMILECOMPILER_SYNTAX_INCLUDE_SYNTAX_ANALYZER_HPP

#include <common/Exception.hpp>
#include <common/Position.hpp>

#include <forward_list>
#include <list>
#include <memory>
#include <stack>
#include <type_traits>

namespace smile {
namespace token {
class Token;
}

namespace tree {
class Tree;

class Expression;
class ArrayExpression;
class OperationExpression;
class StructureExpression;
class TupleExpression;

class Literal;
class BooleanLiteral;
class CharacterLiteral;
class FloatLiteral;
class IntegerLiteral;
class StringLiteral;

class Node;
class Function;
class Implementation;
class Import;
class Structure;

class Operand;

class Operation;
class BinaryOperation;
class Call;
class Cast;
class StructureAccess;
class TupleAccess;
class UnaryOperation;

class Statement;
class Compound;
class Conditional;
class ConditionalLoop;
class Io;
class LoopBreaker;
class RangeLoop;
class ReturnStatement;
class Selection;
class SelectionBranch;
class UnconditionalLoop;
class Variable;

class Token;

class Type;
class ArrayType;
class FunctionType;
class PrimitiveType;
class RangeType;
class ReferenceType;
class TupleType;

class TypeField;
class OperandField;

class Identifier;
class TypeIdentifier;
class OperandIdentifier;
} // namespace tree

namespace syntax {
class Analyzer {
public:
  using TokenPointer = std::unique_ptr<token::Token>;

  explicit Analyzer(std::forward_list<TokenPointer> &&);

  tree::Tree process();

protected:
  const TokenPointer &token() const;
  void nextToken();
  bool reachedEnd() const;

  template <class T> inline bool test() const noexcept;
  template <class T> inline bool test(typename T::Kind) const noexcept;

  template <class T> inline void match(typename T::Kind);

  template <class T> inline bool testAndGo() noexcept;
  template <class T> inline bool testAndGo(typename T::Kind) noexcept;

  void saveStartPosition();
  Position currentPosition() const;
  Position position();

  Exception fail(const std::string &) const;

  tree::Tree program();

  std::unique_ptr<tree::Identifier> extractIdentifier();
  static std::unique_ptr<tree::TypeIdentifier>
  toTypeIdentifier(std::unique_ptr<tree::Identifier> &&);
  static std::unique_ptr<tree::OperandIdentifier>
  toOperandIdentifier(std::unique_ptr<tree::Identifier> &&);
  std::unique_ptr<tree::IntegerLiteral> extractIntLiteral();
  std::unique_ptr<tree::FloatLiteral> extractFloatLiteral();
  std::unique_ptr<tree::BooleanLiteral> extractBooleanLiteral();
  std::unique_ptr<tree::CharacterLiteral> extractCharacterLiteral();
  std::unique_ptr<tree::StringLiteral> extractStringLiteral();

  std::unique_ptr<tree::Operand> arrayExpression();
  std::unique_ptr<tree::ArrayType> arrayType();
  std::unique_ptr<tree::BinaryOperation> binaryOperation();
  std::unique_ptr<tree::Call> call();
  std::unique_ptr<tree::Cast> cast();
  std::unique_ptr<tree::Compound> compound();
  std::unique_ptr<tree::Conditional> conditional();
  std::unique_ptr<tree::ConditionalLoop> conditionalLoop();
  std::list<std::unique_ptr<tree::Token>> expressionHelper(bool = true);
  std::unique_ptr<tree::Operand> expression(bool = true);
  std::list<std::unique_ptr<tree::Operand>> expressionList();
  std::list<std::unique_ptr<tree::Token>> expressionPrime(bool = true);
  std::unique_ptr<tree::Function> function();
  std::list<std::unique_ptr<tree::Function>> functionList();
  std::unique_ptr<tree::Implementation> implementation();
  std::unique_ptr<tree::Import> import();
  std::unique_ptr<tree::Statement> incompleteStatement();
  std::unique_ptr<tree::Io> io();
  std::unique_ptr<tree::Literal> literal();
  std::unique_ptr<tree::LoopBreaker> loopBreaker();
  std::unique_ptr<tree::OperandField> operandField();
  std::list<std::unique_ptr<tree::OperandField>> operandFieldList();
  std::unique_ptr<tree::PrimitiveType> primitiveType();
  std::unique_ptr<tree::RangeLoop> rangeLoop();
  std::unique_ptr<tree::ReturnStatement> returnStatement();
  std::unique_ptr<tree::Selection> selection();
  std::unique_ptr<tree::SelectionBranch> selectionBranch();
  std::list<std::unique_ptr<tree::SelectionBranch>> selectionBranchList();
  std::unique_ptr<tree::Statement> statement();
  std::list<std::unique_ptr<tree::Statement>> statementList();
  std::unique_ptr<tree::Structure> structure();
  std::unique_ptr<tree::Operand> structureExpression();
  std::unique_ptr<tree::Type> type();
  std::list<std::unique_ptr<tree::Type>> typeList();
  std::unique_ptr<tree::TypeField> typeField();
  std::list<std::unique_ptr<tree::TypeField>> typeFieldList();
  std::unique_ptr<tree::Operand> tupleExpression();
  std::unique_ptr<tree::TupleType> tupleType();
  std::unique_ptr<tree::UnaryOperation> unaryOperation();
  std::unique_ptr<tree::UnconditionalLoop> unconditionalLoop();
  std::unique_ptr<tree::Variable> variable();

private:
  TokenPointer m_currentToken;
  Position m_currentPosition;
  std::stack<Position> m_savedPosition;
  std::forward_list<TokenPointer> m_tokens;
  std::forward_list<TokenPointer>::iterator m_tokensIterator;
};

template <class T> bool Analyzer::test() const noexcept {
  static_assert(std::is_base_of<token::Token, T>());
  return dynamic_cast<const T *>(token().get()) != nullptr;
}

template <class T> bool Analyzer::test(typename T::Kind kind) const noexcept {
  static_assert(std::is_base_of<token::Token, T>());
  static_assert(std::is_enum<typename T::Kind>());
  auto testToken = dynamic_cast<const T *>(token().get());
  return testToken != nullptr && testToken->kind() == kind;
}

template <class T> void Analyzer::match(typename T::Kind kind) {
  static_assert(std::is_base_of<token::Token, T>());
  static_assert(std::is_enum<typename T::Kind>());

  if (!testAndGo<T>(kind)) {
    throw fail(std::make_unique<T>(kind, Position())->codeString());
  }
}

template <class T> bool Analyzer::testAndGo() noexcept {
  static_assert(std::is_base_of<token::Token, T>());

  if (test<T>()) {
    nextToken();
    return true;
  } else {
    return false;
  }
}

template <class T> bool Analyzer::testAndGo(typename T::Kind kind) noexcept {
  static_assert(std::is_base_of<token::Token, T>());
  static_assert(std::is_enum<typename T::Kind>());

  if (test<T>(kind)) {
    nextToken();
    return true;
  } else {
    return false;
  }
}
} // namespace syntax
} // namespace smile

#endif // SMILECOMPILER_SYNTAX_INCLUDE_SYNTAX_ANALYZER_HPP
