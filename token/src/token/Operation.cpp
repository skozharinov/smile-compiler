#include <token/Operation.hpp>

namespace smile::token {
const std::unordered_map<Operation::Kind, std::tuple<std::string, std::string>>
    Operation::DATA = {
        {NEGATION, {"NEGATION", "!"}},
        {ADDITION, {"ADDITION", "+"}},
        {SUBTRACTION, {"SUBTRACTION", "-"}},
        {MULTIPLICATION, {"MULTIPLICATION", "*"}},
        {DIVISION, {"DIVISION", "/"}},
        {REMAINDER, {"REMAINDER", "%"}},
        {CONJUNCTION, {"CONJUNCTION", "&"}},
        {DISJUNCTION, {"DISJUNCTION", "|"}},
        {XOR, {"XOR", "^"}},
        {LEFT_SHIFT, {"LEFT_SHIFT", "<<"}},
        {RIGHT_SHIFT, {"RIGHT_SHIFT", ">>"}},
        {EQUALITY, {"EQUALITY", "=="}},
        {INEQUALITY, {"INEQUALITY", "!="}},
        {LESS, {"LESS", "<"}},
        {LESS_OR_EQUALS, {"LESS_OR_EQUALS", "<="}},
        {GREATER, {"GREATER", ">"}},
        {GREATER_OR_EQUALS, {"GREATER_OR_EQUALS", ">="}},
        {ASSIGNMENT, {"ASSIGNMENT", "="}},
        {ADDITION_ASSIGNMENT, {"ADDITION_ASSIGNMENT", "+="}},
        {SUBTRACTION_ASSIGNMENT, {"SUBTRACTION_ASSIGNMENT", "-="}},
        {MULTIPLICATION_ASSIGNMENT, {"MULTIPLICATION_ASSIGNMENT", "*="}},
        {DIVISION_ASSIGNMENT, {"DIVISION_ASSIGNMENT", "/="}},
        {REMAINDER_ASSIGNMENT, {"REMAINDER_ASSIGNMENT", "%="}},
        {CONJUNCTION_ASSIGNMENT, {"CONJUNCTION_ASSIGNMENT", "&="}},
        {DISJUNCTION_ASSIGNMENT, {"DISJUNCTION_ASSIGNMENT", "|="}},
        {XOR_ASSIGNMENT, {"XOR_ASSIGNMENT", "^="}},
        {LEFT_SHIFT_ASSIGNMENT, {"LEFT_SHIFT_ASSIGNMENT", "<<="}},
        {RIGHT_SHIFT_ASSIGNMENT, {"RIGHT_SHIFT_ASSIGNMENT", ">>="}},
        {MEMBER_ACCESS, {"MEMBER_ACCESS", "."}},
        {SCOPE_RESOLUTION, {"SCOPE_RESOLUTION", "::"}},
        {LAZY_CONJUNCTION, {"LAZY_CONJUNCTION", "&&"}},
        {LAZY_DISJUNCTION, {"LAZY_DISJUNCTION", "||"}},
};

Operation::Operation(Operation::Kind kind, Position position)
    : Token(position), m_kind(kind) {}

Operation::Kind Operation::kind() const { return m_kind; }

std::string Operation::codeString() const {
  return std::get<1>(DATA.at(kind()));
}

std::string Operation::classString() const { return "OPERATION"; }

std::string Operation::typeString() const {
  return std::get<0>(DATA.at(kind()));
}
} // namespace smile::token