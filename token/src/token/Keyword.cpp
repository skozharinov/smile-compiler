#include <token/Keyword.hpp>

namespace smile::token {
const std::unordered_map<Keyword::Kind, std::tuple<std::string, std::string>>
    Keyword::DATA = {
        {READ, {"READ", "read"}},
        {WRITE, {"WRITE", "write"}},
        {SIGNED_INT_8, {"SIGNED_INT_8", "i8"}},
        {UNSIGNED_INT_8, {"UNSIGNED_INT_8", "u8"}},
        {SIGNED_INT_16, {"SIGNED_INT_16", "i16"}},
        {UNSIGNED_INT_16, {"UNSIGNED_INT_16", "u16"}},
        {SIGNED_INT_32, {"SIGNED_INT_32", "i32"}},
        {UNSIGNED_INT_32, {"UNSIGNED_INT_32", "u32"}},
        {SIGNED_INT_64, {"SIGNED_INT_64", "i64"}},
        {UNSIGNED_INT_64, {"UNSIGNED_INT_64", "u64"}},
        {SIGNED_INT_SIZE, {"SIGNED_INT_SIZE", "isize"}},
        {UNSIGNED_INT_SIZE, {"UNSIGNED_INT_SIZE", "usize"}},
        {FLOAT_32, {"FLOAT_32", "f32"}},
        {FLOAT_64, {"FLOAT_64", "f64"}},
        {FLOAT_NAN, {"FLOAT_NAN", "nan"}},
        {FLOAT_INF, {"FLOAT_INF", "inf"}},
        {CHARACTER, {"CHARACTER", "char"}},
        {STRING, {"STRING", "str"}},
        {BOOLEAN, {"BOOLEAN", "bool"}},
        {BOOLEAN_TRUE, {"BOOLEAN_TRUE", "true"}},
        {BOOLEAN_FALSE, {"BOOLEAN_FALSE", "false"}},
        {CAST_OPERATOR, {"CAST_OPERATOR", "as"}},
        {CONDITIONAL_MAIN, {"CONDITIONAL_MAIN", "if"}},
        {CONDITIONAL_ALTERNATIVE, {"CONDITIONAL_ALTERNATIVE", "else"}},
        {SELECTION, {"SELECTION", "match"}},
        {UNCONDITIONAL_LOOP, {"UNCONDITIONAL_LOOP", "loop"}},
        {CONDITIONAL_LOOP, {"CONDITIONAL_LOOP", "while"}},
        {RANGE_LOOP, {"RANGE_LOOP", "for"}},
        {RANGE_LOOP_OPERATOR, {"RANGE_LOOP_OPERATOR", "in"}},
        {RANGE_OPERATOR, {"RANGE_OPERATOR", "to"}},
        {MUTABLE_VARIABLE, {"MUTABLE_VARIABLE", "mut"}},
        {IMMUTABLE_VARIABLE, {"IMMUTABLE_VARIABLE", "let"}},
        {FUNCTION, {"FUNCTION", "fun"}},
        {STRUCTURE, {"STRUCTURE", "struct"}},
        {RETURN, {"RETURN", "return"}},
        {CONTINUE, {"CONTINUE", "continue"}},
        {BREAK, {"BREAK", "break"}},
        {NULL_POINTER, {"NULL_POINTER", "null"}},
        {IMPORT, {"IMPORT", "import"}},
        {IMPLEMENTATION, {"IMPLEMENTATION", "impl"}},
};

Keyword::Keyword(Keyword::Kind kind, Position position)
    : Token(position), m_kind(kind) {}

Keyword::Kind Keyword::kind() const { return m_kind; }

std::string Keyword::codeString() const { return std::get<1>(DATA.at(kind())); }

std::string Keyword::classString() const { return "KEYWORD"; }

std::string Keyword::typeString() const { return std::get<0>(DATA.at(kind())); }
} // namespace smile::token
