#include <token/Punctuation.hpp>

namespace smile::token {
const std::unordered_map<Punctuation::Kind,
                         std::tuple<std::string, std::string>>
    Punctuation::DATA = {
        {SEMICOLON, {"SEMICOLON", ";"}},
        {LEFT_ROUND_BRACKET, {"LEFT_ROUND_BRACKET", "("}},
        {RIGHT_ROUND_BRACKET, {"RIGHT_ROUND_BRACKET", ")"}},
        {LEFT_SQUARE_BRACKET, {"LEFT_SQUARE_BRACKET", "["}},
        {RIGHT_SQUARE_BRACKET, {"RIGHT_SQUARE_BRACKET", "]"}},
        {LEFT_CURLY_BRACKET, {"LEFT_CURLY_BRACKET", "{"}},
        {RIGHT_CURLY_BRACKET, {"RIGHT_CURLY_BRACKET", "}"}},
        {COMMA, {"COMMA", ","}},
        {COLON, {"COLON", ":"}},
        {SELECTION, {"SELECTION", "->"}},
};

Punctuation::Punctuation(Punctuation::Kind kind, Position position)
    : Token(position), m_kind(kind) {}

Punctuation::Kind Punctuation::kind() const { return m_kind; }

std::string Punctuation::codeString() const {
  return std::get<1>(DATA.at(kind()));
}

std::string Punctuation::classString() const { return "PUNCTUATION"; }

std::string Punctuation::typeString() const {
  return std::get<0>(DATA.at(kind()));
}
} // namespace smile::token
