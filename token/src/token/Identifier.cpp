#include <token/Identifier.hpp>

namespace smile::token {
Identifier::Identifier(std::string contents, Position position)
    : Token(position), m_contents(std::move(contents)) {}

std::string Identifier::codeString() const {
  if (m_contents.length() > 0) {
    return m_contents;
  } else {
    return typeString();
  }
}

std::string Identifier::classString() const { return "IDENTIFIER"; }

std::string Identifier::typeString() const { return "IDENTIFIER"; }
} // namespace smile::token
