#include <token/literal/Literal.hpp>

#include <utility>

namespace smile::token {
Literal::Literal(std::string contents, Position position)
    : Token(position), m_contents(std::move(contents)) {}

std::string Literal::codeString() const {
  if (m_contents.length() > 0) {
    return m_contents;
  } else {
    return typeString();
  }
}

std::string Literal::classString() const { return "LITERAL"; }
} // namespace smile::token
