#include <token/literal/FloatLiteral.hpp>

#include <utility>

namespace smile::token {
FloatLiteral::FloatLiteral(std::string contents, Position position)
    : Literal(std::move(contents), position) {}

std::string FloatLiteral::typeString() const { return "FLOAT"; }

double FloatLiteral::toFloat64() const {
  std::string result = codeString();
  return std::stod(result);
}
} // namespace smile::token
