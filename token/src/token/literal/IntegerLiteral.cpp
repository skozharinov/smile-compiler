#include <token/literal/IntegerLiteral.hpp>

#include <utility>

namespace smile::token {
IntegerLiteral::IntegerLiteral(std::string contents, Position position)
    : Literal(std::move(contents), position) {}

std::string IntegerLiteral::typeString() const { return "INT"; }

std::uint64_t IntegerLiteral::toUnsigned64() const {
  std::string result = codeString();
  return std::stoll(result);
}
} // namespace smile::token
