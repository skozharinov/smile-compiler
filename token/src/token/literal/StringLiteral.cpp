#include <token/literal/StringLiteral.hpp>

#include <common/escapes.hpp>
#include <utility>

namespace smile::token {
StringLiteral::StringLiteral(std::string contents, Position position)
    : Literal(std::move(contents), position) {}

std::string StringLiteral::typeString() const { return "STRING"; }

std::string StringLiteral::toString() const {
  std::string result = codeString();
  escapes::interpretEscaped(result);
  return result.substr(1, result.size() - 2);
}
} // namespace smile::token
