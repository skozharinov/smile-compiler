#include <token/literal/CharacterLiteral.hpp>

#include <common/escapes.hpp>
#include <utility>

namespace smile::token {
CharacterLiteral::CharacterLiteral(std::string contents, Position position)
    : Literal(std::move(contents), position) {}

std::string CharacterLiteral::typeString() const { return "CHARACTER"; }

char CharacterLiteral::toCharacter() const {
  std::string result = codeString();
  escapes::interpretEscaped(result);
  return result[1];
}
} // namespace smile::token
