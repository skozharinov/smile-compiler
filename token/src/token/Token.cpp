#include <token/Token.hpp>

namespace smile::token {
Position Token::position() const { return m_position; }

Token::Token(Position position) : m_position(position) {}
} // namespace smile::token
