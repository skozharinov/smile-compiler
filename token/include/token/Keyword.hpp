#ifndef SMILECOMPILER_TOKEN_INCLUDE_TOKEN_KEYWORD_HPP
#define SMILECOMPILER_TOKEN_INCLUDE_TOKEN_KEYWORD_HPP

#include <token/Token.hpp>

#include <unordered_map>

namespace smile::token {
class Keyword : public Token {
public:
  enum Kind {
    READ,
    WRITE,
    SIGNED_INT_8,
    UNSIGNED_INT_8,
    SIGNED_INT_16,
    UNSIGNED_INT_16,
    SIGNED_INT_32,
    UNSIGNED_INT_32,
    SIGNED_INT_64,
    UNSIGNED_INT_64,
    SIGNED_INT_SIZE,
    UNSIGNED_INT_SIZE,
    FLOAT_32,
    FLOAT_64,
    FLOAT_NAN,
    FLOAT_INF,
    CHARACTER,
    STRING,
    BOOLEAN,
    BOOLEAN_TRUE,
    BOOLEAN_FALSE,
    CAST_OPERATOR,
    CONDITIONAL_MAIN,
    CONDITIONAL_ALTERNATIVE,
    SELECTION,
    UNCONDITIONAL_LOOP,
    CONDITIONAL_LOOP,
    RANGE_LOOP,
    RANGE_LOOP_OPERATOR,
    RANGE_OPERATOR,
    MUTABLE_VARIABLE,
    IMMUTABLE_VARIABLE,
    FUNCTION,
    STRUCTURE,
    RETURN,
    CONTINUE,
    BREAK,
    NULL_POINTER,
    IMPORT,
    IMPLEMENTATION,
  };

  static const std::unordered_map<Kind, std::tuple<std::string, std::string>>
      DATA;

  Keyword(Kind, Position);

  std::string codeString() const override;
  std::string classString() const override;
  std::string typeString() const override;

  Kind kind() const;

private:
  Kind m_kind;
};
} // namespace smile::token

#endif // SMILECOMPILER_TOKEN_INCLUDE_TOKEN_KEYWORD_HPP
