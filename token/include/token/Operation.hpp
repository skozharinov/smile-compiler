#ifndef SMILECOMPILER_TOKEN_INCLUDE_TOKEN_OPERATION_HPP
#define SMILECOMPILER_TOKEN_INCLUDE_TOKEN_OPERATION_HPP

#include <token/Token.hpp>

#include <unordered_map>

namespace smile::token {
class Operation : public Token {
public:
  enum Kind {
    NEGATION,
    ADDITION,
    SUBTRACTION,
    MULTIPLICATION,
    DIVISION,
    REMAINDER,
    CONJUNCTION,
    DISJUNCTION,
    XOR,
    LEFT_SHIFT,
    RIGHT_SHIFT,
    EQUALITY,
    INEQUALITY,
    LESS,
    LESS_OR_EQUALS,
    GREATER,
    GREATER_OR_EQUALS,
    ASSIGNMENT,
    ADDITION_ASSIGNMENT,
    SUBTRACTION_ASSIGNMENT,
    MULTIPLICATION_ASSIGNMENT,
    DIVISION_ASSIGNMENT,
    REMAINDER_ASSIGNMENT,
    CONJUNCTION_ASSIGNMENT,
    DISJUNCTION_ASSIGNMENT,
    XOR_ASSIGNMENT,
    LEFT_SHIFT_ASSIGNMENT,
    RIGHT_SHIFT_ASSIGNMENT,
    MEMBER_ACCESS,
    SCOPE_RESOLUTION,
    LAZY_CONJUNCTION,
    LAZY_DISJUNCTION,
  };

  static const std::unordered_map<Kind, std::tuple<std::string, std::string>>
      DATA;

  Operation(Kind, Position);

  std::string codeString() const override;
  std::string classString() const override;
  std::string typeString() const override;

  Kind kind() const;

private:
  Kind m_kind;
};
} // namespace smile::token

#endif // SMILECOMPILER_TOKEN_INCLUDE_TOKEN_OPERATION_HPP
