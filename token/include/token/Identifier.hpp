#ifndef SMILECOMPILER_TOKEN_INCLUDE_TOKEN_IDENTIFIER_HPP
#define SMILECOMPILER_TOKEN_INCLUDE_TOKEN_IDENTIFIER_HPP

#include <string>
#include <token/Token.hpp>

namespace smile::token {
class Identifier : public Token {
public:
  Identifier(std::string, Position);

  std::string codeString() const override;
  std::string classString() const override;
  std::string typeString() const override;

private:
  std::string m_contents;
};
} // namespace smile::token

#endif // SMILECOMPILER_TOKEN_INCLUDE_TOKEN_IDENTIFIER_HPP
