#ifndef SMILECOMPILER_TOKEN_INCLUDE_TOKEN_PUNCTUATION_HPP
#define SMILECOMPILER_TOKEN_INCLUDE_TOKEN_PUNCTUATION_HPP

#include <token/Token.hpp>

#include <unordered_map>

namespace smile::token {
class Punctuation : public Token {
public:
  enum Kind {
    SEMICOLON,
    LEFT_ROUND_BRACKET,
    RIGHT_ROUND_BRACKET,
    LEFT_SQUARE_BRACKET,
    RIGHT_SQUARE_BRACKET,
    LEFT_CURLY_BRACKET,
    RIGHT_CURLY_BRACKET,
    COMMA,
    COLON,
    SELECTION,
  };

  static const std::unordered_map<Kind, std::tuple<std::string, std::string>>
      DATA;

  Punctuation(Kind, Position);

  std::string codeString() const override;
  std::string classString() const override;
  std::string typeString() const override;

  Kind kind() const;

private:
  Kind m_kind;
};
} // namespace smile::token

#endif // SMILECOMPILER_TOKEN_INCLUDE_TOKEN_PUNCTUATION_HPP
