#ifndef SMILECOMPILER_TOKEN_INCLUDE_TOKEN_LITERAL_STRINGLITERAL_HPP
#define SMILECOMPILER_TOKEN_INCLUDE_TOKEN_LITERAL_STRINGLITERAL_HPP

#include <token/literal/Literal.hpp>

namespace smile::token {
class StringLiteral : public Literal {
public:
  StringLiteral(std::string, Position);

  std::string typeString() const override;

  std::string toString() const;
};
} // namespace smile::token

#endif
