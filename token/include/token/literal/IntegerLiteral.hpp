#ifndef SMILECOMPILER_TOKEN_INCLUDE_TOKEN_LITERAL_INTEGERLITERAL_HPP
#define SMILECOMPILER_TOKEN_INCLUDE_TOKEN_LITERAL_INTEGERLITERAL_HPP

#include <token/literal/Literal.hpp>

namespace smile::token {
class IntegerLiteral : public Literal {
public:
  IntegerLiteral(std::string, Position);

  std::string typeString() const override;

  std::uint64_t toUnsigned64() const;
};
} // namespace smile::token

#endif // SMILECOMPILER_TOKEN_INCLUDE_TOKEN_LITERAL_INTEGERLITERAL_HPP
