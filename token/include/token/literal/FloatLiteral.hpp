#ifndef SMILECOMPILER_TOKEN_INCLUDE_TOKEN_LITERAL_FLOATLITERAL_HPP
#define SMILECOMPILER_TOKEN_INCLUDE_TOKEN_LITERAL_FLOATLITERAL_HPP

#include <token/literal/Literal.hpp>

namespace smile::token {
class FloatLiteral : public Literal {
public:
  FloatLiteral(std::string, Position);

  std::string typeString() const override;

  double toFloat64() const;
};
} // namespace smile::token

#endif
