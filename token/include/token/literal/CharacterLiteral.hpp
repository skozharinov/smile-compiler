#ifndef SMILECOMPILER_TOKEN_INCLUDE_TOKEN_LITERAL_CHARACTERLITERAL_HPP
#define SMILECOMPILER_TOKEN_INCLUDE_TOKEN_LITERAL_CHARACTERLITERAL_HPP

#include <token/literal/Literal.hpp>

namespace smile::token {
class CharacterLiteral : public Literal {
public:
  CharacterLiteral(std::string, Position);

  std::string typeString() const override;

  char toCharacter() const;
};
} // namespace smile::token

#endif // SMILECOMPILER_TOKEN_INCLUDE_TOKEN_LITERAL_CHARACTERLITERAL_HPP
