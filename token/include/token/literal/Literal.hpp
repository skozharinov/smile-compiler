#ifndef SMILECOMPILER_TOKEN_INCLUDE_TOKEN_LITERAL_LITERAL_HPP
#define SMILECOMPILER_TOKEN_INCLUDE_TOKEN_LITERAL_LITERAL_HPP

#include <string>
#include <token/Token.hpp>

namespace smile::token {
class Literal : public Token {
public:
  std::string codeString() const final;
  std::string classString() const final;

protected:
  Literal(std::string, Position);

private:
  std::string m_contents;
};
} // namespace smile::token

#endif // SMILECOMPILER_TOKEN_INCLUDE_TOKEN_LITERAL_LITERAL_HPP
