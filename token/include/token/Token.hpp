#ifndef SMILECOMPILER_TOKEN_INCLUDE_TOKEN_TOKEN_HPP
#define SMILECOMPILER_TOKEN_INCLUDE_TOKEN_TOKEN_HPP

#include <common/Position.hpp>

#include <string>

namespace smile::token {
class Token {
public:
  Position position() const;

  virtual ~Token() = default;

  virtual std::string codeString() const = 0;
  virtual std::string classString() const = 0;
  virtual std::string typeString() const = 0;

protected:
  explicit Token(Position);

private:
  const Position m_position;
};
} // namespace smile::token

#endif // SMILECOMPILER_TOKEN_INCLUDE_TOKEN_TOKEN_HPP
